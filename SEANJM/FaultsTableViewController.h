//
//  FaultsTableViewController.h
//  SEANJM
//
//  Created by Randa Al-Sadek on 5/20/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FaultsTableViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) NSMutableArray *allFaults;


@end
