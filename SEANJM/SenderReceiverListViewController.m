//
//  SenderReceiverListViewController.m
//  SEANJM
//
//  Created by Abdul Kareem on 9/16/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "SenderReceiverListViewController.h"
#import "TwoTitleCell.h"
#import "FooterViewCell.h"
#import "Macros.h"
#import "LanguageController.h"
#import "AFNetworking.h"
#import "Utility.h"
#import "UserInformation.h"
#import "MBProgressHUD.h"
#import "AddUsersViewController.h"
#import "AddBusesViewController.h"
#import "AFNetworking.h"
#import "ConfigurationManager.h"
#import "BTSData.h"
#import "BusDetailViewController.h"

#define BTSGetAllRouteBuses @"http://213.236.53.172/snbts/restserviceimpl.svc/getAllRoutesBuses"
#define BTSRemoveUserFromRoute @"http://213.236.53.172/snbts/restserviceimpl.svc/removeBusesFromSpecificRoute"

@interface SenderReceiverListViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    ConfigurationManager *config;
    NSOperationQueue *operationqueue;
    
    
}
@property (nonatomic,weak)IBOutlet UITableView *srTableView;
@property (nonatomic)NSString       *busCellIdentifier;
@property (nonatomic)NSString       *srFooterIdentifier;
@property (nonatomic)NSMutableArray *busesArray;
@property (nonatomic)NSDictionary   *userSelectedRole;
@property (nonatomic)NSDictionary   *NewUserRoute;
@property (nonatomic)MBProgressHUD  *hudview;
@property (nonatomic)NSInteger      selectedIndexToEdit;
@property(nonatomic,strong) NSMutableArray *sendBusArray;
@property(nonatomic) NSMutableArray *removingArray;

@end

@implementation SenderReceiverListViewController

-(id)initWithSelectedBusesOfRoute :(NSArray *)busesForRoute
{
    self =[super init];
    _busesArray = [NSMutableArray arrayWithArray:busesForRoute];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _sendBusArray   = [NSMutableArray array];
    _removingArray  = [NSMutableArray array];
    _busesArray     = [NSMutableArray array];
    
    config = [[ConfigurationManager alloc]init];
    operationqueue =[[NSOperationQueue alloc]init];
    
    _busCellIdentifier = @"TwoTitleCell";
    _srFooterIdentifier = @"FooterViewCell";
    
    [_srTableView registerNib:[UINib nibWithNibName:_busCellIdentifier bundle:[NSBundle mainBundle]]forCellReuseIdentifier:_busCellIdentifier];
    
    [_srTableView registerNib:[UINib nibWithNibName:_srFooterIdentifier bundle:[NSBundle mainBundle]]forCellReuseIdentifier:_srFooterIdentifier];
    
    [self performSelector:@selector(loadAllBussesWithRouteAssignedByCoordinator) withObject:nil afterDelay:0.00005];
    
    AddObserver(self, @selector(loadAllBussesWithRouteAssignedByCoordinator), @"loadAllBussesNotification", nil);
}



-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.topItem.title = [[UserInformation getUserSelectedRoute] objectForKey:@"name"];
    self.navigationController.navigationBar.backItem.title = LocalizedString(@"back");
    _NewUserRoute = [NSDictionary dictionary];
    
    UIBarButtonItem *btnRefresh =[[UIBarButtonItem alloc] initWithTitle:LocalizedString(@"refresh") style:UIBarButtonItemStylePlain target:self action:@selector(loadAllBussesWithRouteAssignedByCoordinator)];
    [self.navigationItem setRightBarButtonItem:btnRefresh];
    
    NSIndexPath *tableSelection = [_srTableView indexPathForSelectedRow];
    [_srTableView deselectRowAtIndexPath:tableSelection animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    if(![self.navigationController.viewControllers containsObject:self])
        RemoveObserver(@"loadAllBussesNotification");
}

#pragma mark - Coordinator
//Manipulate Array for Coordinator Routes
-(void)loadAllBussesWithRouteAssignedByCoordinator
{
    if(![Utility checkInternetConnection])
        return;
    
    if(!_hudview)
    {
    _hudview = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _hudview.mode = MBProgressHUDModeIndeterminate;
    _hudview.labelText = @"Loading...";
    [_hudview setDimBackground:YES];
    }
    NSMutableDictionary *parameters =[NSMutableDictionary dictionary];
    [parameters setObject:[UserInformation getUserId] forKey:@"userId"];
    [parameters setObject:[UserInformation getGroupID] forKey:@"groupId"];
    [parameters setObject:[UserInformation getUserSelectedRoleId] forKey:@"roleId"];
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@",BTSGetAllRouteBuses];
    
    NSLog(@"Request Buses %@ %@",serviceURL,parameters);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager POST:serviceURL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"Assigned Buses List %@", responseObject);
        if(![responseObject  isKindOfClass:[NSNull class]])
        {
            _busesArray = [NSMutableArray arrayWithArray:[responseObject objectForKey:@"Result"]];
            
            [UserDefaults setObject:_busesArray forKey:kBTS_SR_Assignedbuses];
            [UserDefaults synchronize];

            [_srTableView reloadData];
            
            if(_busesArray.count ==0)
            {
                [Utility showAlertViewWithTitle:@"" message:LocalizedString(@"nobusesareassigned") cancelButtonTile:LocalizedString(@"ok")];
            }
        }
        [_hudview hide:YES];
        _hudview = nil;
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"BTS Error: %@", error);
        [_hudview setHidden:YES];
        _hudview = nil;
    }];
}

-(void)loadAllTasks
{
    /// Load from webservice ///
    
    if([UserDefaults objectForKey:kBTS_SR_Assignedbuses] && _busesArray.count ==0)
        _busesArray = (NSMutableArray *)[BTSData getAssignedBusesByCoordinatorForRoute];
    
    //Update Task Info
    if([[UserDefaults objectForKey:kUpdateBus] count] >0)
    {
        _NewUserRoute = [UserDefaults objectForKey:kUpdateBus];
        [_busesArray replaceObjectAtIndex:_selectedIndexToEdit withObject:_NewUserRoute];
        [UserDefaults removeObjectForKey:kUpdateBus];
        [UserDefaults synchronize];
    }
    
    //Add New Task
    else if([[UserDefaults objectForKey:kAddnewBus] count] >0)
    {
        _NewUserRoute = [UserDefaults objectForKey:kAddnewBus];
        [_sendBusArray addObject:_NewUserRoute];
        [_busesArray addObject:_NewUserRoute];
        [UserDefaults removeObjectForKey:kAddnewBus];
        [UserDefaults synchronize];
        [_srTableView reloadData];
        
    }
}

#pragma mark -
#pragma mark IBActions

-(void)changeSavedStatusAfterUploadingSuccessfully
{
    NSMutableArray *finalArray =[NSMutableArray array];
    NSMutableDictionary *mutableDictionary;
    
    for (NSMutableDictionary *castDic in _busesArray) {
        mutableDictionary = [castDic mutableCopy];
        if( [castDic objectForKey:@"addItem"])
        {
            [mutableDictionary removeObjectForKey:@"addItem"];
        }
        if( [castDic objectForKey:@"deleteItem"])
        {
            [mutableDictionary removeObjectForKey:@"deleteItem"];
        }
        if(mutableDictionary.count >0)
            [finalArray addObject:mutableDictionary];
    }
    //[finalArray removeObjectsInArray:_removingArray];
    //[finalArray arrayByAddingObjectsFromArray:_sendBusArray];
    _busesArray = finalArray;
    
    [_srTableView reloadData];
}

#pragma mark -
#pragma mark UITableView DataSource


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _busesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TwoTitleCell *taskCell =[tableView dequeueReusableCellWithIdentifier:_busCellIdentifier forIndexPath:indexPath];
    
    taskCell.lblRightTitle.text = [NSString stringWithFormat:@"%@: ",[BTSData getRouteNameWithRouteId:[[_busesArray objectAtIndex:indexPath.row] objectForKey:@"routeId"]]];

    NSString *stringForLeftLabel =[NSString stringWithFormat:@"%@ : %@",[[_busesArray objectAtIndex:indexPath.row] objectForKey:@"companyName"],[[_busesArray objectAtIndex:indexPath.row] objectForKey:@"plateNumber"]];
    
    taskCell.lblLeftTitle.text = stringForLeftLabel;
    [taskCell.btnImage setContentMode:UIViewContentModeScaleAspectFit];
    [taskCell.btnImage setBackgroundImage:[UIImage imageNamed:@"ic_info"] forState:UIControlStateNormal];
    [taskCell.btnImage.superview setTag:indexPath.row];
    [taskCell.btnImage addTarget:self action:@selector(leftButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    //[taskCell.btnImage setHidden:YES];
    
    [taskCell.btnImage setUserInteractionEnabled:YES];
    [taskCell setBackgroundColor:[UIColor whiteColor]];
    
    if(_removingArray.count >0)
    {
        if([[_busesArray objectAtIndex:indexPath.row] objectForKey:@"deleteItem"])
            [taskCell setBackgroundColor:LightRedColor];
    }
    if(_NewUserRoute.count >0){
        if([[_busesArray objectAtIndex:indexPath.row] objectForKey:@"addItem"])
        {
            [taskCell setBackgroundColor:LightYellowColor];
        }
    }
    return taskCell;
}

#pragma mark - TableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section;
//{
//    return 44.0;
//}

#pragma mark TABELVIEW EDITING
/*
-(NSString *)tableView:(UITableView *)tableView titleForSwipeAccessoryButtonForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return @"Undo";
}

-(void)tableView:(UITableView *)tableView swipeAccessoryButtonPushedForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    [_removingArray removeObject:[_busesArray objectAtIndex:indexPath.row]];
    [[tableView cellForRowAtIndexPath:indexPath] setBackgroundColor:[UIColor whiteColor]];
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
        
        [tableView beginUpdates];
        if(![_removingArray containsObject:[_busesArray objectAtIndex:indexPath.row]])
            [_removingArray addObject:[_busesArray objectAtIndex:indexPath.row]];
        
        [[tableView cellForRowAtIndexPath:indexPath] setBackgroundColor:LightRedColor];
        NSMutableDictionary *deleteDic = [NSMutableDictionary dictionaryWithDictionary:[_busesArray objectAtIndex:indexPath.row]];
        [deleteDic setObject:@"delete" forKey:@"deleteItem"];
        [_busesArray replaceObjectAtIndex:indexPath.row withObject:deleteDic];
        
        [tableView endUpdates];
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}
 */

#pragma mark -
#pragma mark - IBActions
-(IBAction)leftButtonPressed:(UIButton *)sender
{
    _selectedIndexToEdit = sender.superview.tag;
    BusDetailViewController *objBusDetail = [[BusDetailViewController alloc]initWithSelectedBus:[_busesArray objectAtIndex:_selectedIndexToEdit]];
    [self.navigationController pushViewController:objBusDetail animated:YES];
}

//Add new task
-(IBAction)leftFooterButtonTapped:(UIButton *)sender
{
    AddBusesViewController *objAddbus = [[AddBusesViewController alloc]initWithBusArray:_busesArray];
    [self.navigationController pushViewController:objAddbus animated:YES];
}

@end
