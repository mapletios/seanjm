//
//  ViewController.m
//  SEANJM
//
//  Created by Randa Al-Sadek on 5/12/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "MainViewController.h"
#import "FaultsTableViewController.h"
#import "LanguageController.h"

@interface MainViewController () {
    
    __weak IBOutlet UILabel *addReportLbl;
}

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    addReportLbl.text = LocalizedString(@"addReport");
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)addReportButtonPressed:(id)sender {
    FaultsTableViewController *faultsTableView = [[FaultsTableViewController alloc]init];
    [self.navigationController pushViewController:faultsTableView animated:YES];
}
@end
