//
//  RolesViewController.m
//  SEANJM
//
//  Created by Abdul Kareem on 9/2/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "RolesViewController.h"
#import "SingleTitleCell.h"
#import "Macros.h"
#import "LanguageController.h"
#import "HeaderView.h"
#import "RoutesViewController.h"
#import "AFNetworking.h"
#import "Utility.h"
#import "UserInformation.h"
#import "MBProgressHUD.h"
#import "SenderReceiverListViewController.h"
#import "AdminsOptionsViewController.h"

#define BTSConfigurationService @"http://213.236.53.172/snbts/restserviceimpl.svc/getBTSConfiguration"
#define BTSCoordinatorConfigurationService @"http://213.236.53.172/snbts/restserviceimpl.svc/getCoordinatorBTSConfiguration"

@interface RolesViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSString *serviceURLBTS;
    NSString *serviceURLBTSCoordinator;
}
@property (nonatomic,weak)IBOutlet UITableView *roleTableView;
@property (nonatomic)NSString *roleCellIdentifier;
@property (nonatomic)NSMutableArray *rolesArray;
@property (nonatomic)MBProgressHUD *hudview;
@end

@implementation RolesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _rolesArray = [NSMutableArray array];
    
    //Everytime we need to load
    //if(![UserDefaults boolForKey:kBTSCoordinatorConfigLoaded])
    {
        serviceURLBTSCoordinator = [NSString stringWithFormat:@"%@/%@",BTSCoordinatorConfigurationService,[UserDefaults objectForKey:@"userGroupId"]];
        [self performSelector:@selector(loadRoles:) withObject:serviceURLBTSCoordinator afterDelay:0.0005];
    }
    
    if(![UserDefaults boolForKey:kBTSConfigLoaded])
    {
        serviceURLBTS = [NSString stringWithFormat:@"%@/%@",BTSConfigurationService,[UserDefaults objectForKey:@"userGroupId"]];
        [self performSelector:@selector(loadRoles:) withObject:serviceURLBTS afterDelay:0.0005];
    }
    else
        [self checkRole];
    
    
    _roleCellIdentifier = @"SingleTitleCell";
    [_roleTableView registerNib:[UINib nibWithNibName:_roleCellIdentifier bundle:[NSBundle mainBundle]]forCellReuseIdentifier:_roleCellIdentifier];
    [_roleTableView registerNib:[UINib nibWithNibName:@"HeaderView" bundle:[NSBundle mainBundle]]forCellReuseIdentifier:@"HeaderCell"];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    [Utility setNavigationBarProperties:self.navigationController];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    if(![self.navigationController.viewControllers containsObject:self])
    {
        [_hudview hide:YES];
        _hudview = nil;
    }
}

-(void)loadRoles :(NSString *)service_URL
{
    if(![Utility checkInternetConnection])
        return;
    
    if(!_hudview)
    {
        _hudview = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        _hudview.mode = MBProgressHUDModeIndeterminate;
        _hudview.labelText = @"Loading...";
        [_hudview setDimBackground:YES];
    }

   // dispatch_async(dispatch_get_main_queue(), ^{
         
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    //[manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager GET:service_URL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {

        //NSLog(@"URL %@", operation.request.URL.relativeString);

        if(![responseObject  isKindOfClass:[NSNull class]])
        {
            if([operation.request.URL.relativeString isEqualToString: serviceURLBTS])
            {
                [UserDefaults setObject:responseObject forKey:kBTSCONFIGURATION];
                [UserDefaults setBool:YES forKey:kBTSConfigLoaded];
                [self checkRole];
            }
            else if([operation.request.URL.relativeString isEqualToString:serviceURLBTSCoordinator])
            {
                [UserDefaults setObject:responseObject forKey:kBTSCOORDINATORCONFIGURATION];
                [UserDefaults setBool:YES forKey:kBTSCoordinatorConfigLoaded];
            }
            [UserDefaults synchronize];
        }
        
        else {
            [Utility showAlertViewWithTitle:LocalizedString(@"error")  message:LocalizedString(@"failLoadConfig") cancelButtonTile:LocalizedString(@"ok")];
        }
        [_hudview hide:YES];
        _hudview = nil;
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"Error: %@", error);
        //[activityIndicator stopAnimating];
        [_hudview hide:YES];
        _hudview = nil;
    }];
         
    // });
}

-(void)checkRole
{
    //NSLog(@"######BTS CONFIGURATION ######## %@",[[UserDefaults objectForKey:kBTSCONFIGURATION] objectForKey:kbtsroles]);
    NSArray *rolesList = [NSArray arrayWithArray:[[UserDefaults objectForKey:kBTSCONFIGURATION] objectForKey:kbtsroles]];
    NSArray *userRoles = [UserInformation getUserRoles];

    //Manager
    if([UserInformation getIsUserGM])
        [_rolesArray addObject:[rolesList objectAtIndex:2]];
    
    @autoreleasepool {
        for (NSDictionary *userrole in userRoles) {
            
            for (NSDictionary *selectedRole in rolesList) {
                
                
                if([[[selectedRole objectForKey:@"id"] stringValue] isEqualToString:[[userrole objectForKey:@"roleId"] stringValue]])
                {
                    if(![_rolesArray containsObject:selectedRole])
                        [_rolesArray addObject:selectedRole];
                }
                else if(([[[selectedRole objectForKey:@"id"] stringValue] isEqualToString:@"2"]) && ([UserInformation getIsUserGM]))
                {
                    if(![_rolesArray containsObject:selectedRole])
                        [_rolesArray addObject:selectedRole];
                }
            }
        }
        
    }
    
    //NSLog(@"my roles %@",_rolesArray);
    if(_rolesArray.count ==0)
    {
        [Utility showAlertViewWithTitle:LocalizedString(@"info") message:LocalizedString(@"Youarenotassignedanyrole") cancelButtonTile:@"ok"];
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    [_roleTableView reloadData];
    [_hudview hide:YES];
    _hudview = nil;
}

#pragma mark -
#pragma mark UITableView DataSource

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    HeaderView *headerView = [tableView dequeueReusableCellWithIdentifier:@"HeaderCell"];
    [headerView setTitle:LocalizedString(@"roles")];
    
    return headerView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(_rolesArray.count >0)
    return 1;

    return 0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _rolesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    SingleTitleCell *roleCell =[tableView dequeueReusableCellWithIdentifier:_roleCellIdentifier];
    [roleCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    roleCell.lblTitle.text = LocalizedString([[_rolesArray objectAtIndex:indexPath.row] objectForKey:@"name"]);
    [roleCell.btnImage setBackgroundImage:[UIImage imageNamed:@"ic_role"] forState:UIControlStateNormal];
    [roleCell.btnImage setTag:indexPath.row];
    [roleCell.btnImage addTarget:self action:@selector(leftButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    return roleCell;
}

#pragma mark - TableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}

#pragma mark -
#pragma mark - IBActions
-(IBAction)leftButtonTapped:(UIButton *)sender
{
    if(![Utility checkInternetConnection])
        return;
    
    NSString *selectedRole = [[[_rolesArray objectAtIndex:sender.tag] objectForKey:@"id"] stringValue];
    if([selectedRole isEqualToString: kBTSSenderID] || [selectedRole isEqualToString: kBTSReceiverID])
    {
        SenderReceiverListViewController *objSR =[[SenderReceiverListViewController alloc]init];
        [self.navigationController pushViewController:objSR animated:YES];
    }
    else if([selectedRole isEqualToString:kBTSManagerID])
    {
        AdminsOptionsViewController *objAdminOptions =[[AdminsOptionsViewController alloc]initWithAuthorityIsSuperAdmin:NO];
        [self.navigationController pushViewController:objAdminOptions animated:YES];
    }
    else if([selectedRole isEqualToString:kBTSCoordinatorID])
    {
        RoutesViewController *objRoute =[[RoutesViewController alloc]initWithRole:[_rolesArray objectAtIndex:sender.tag]];
        [self.navigationController pushViewController:objRoute animated:YES];
    }

    
    [UserDefaults setObject:[_rolesArray objectAtIndex:sender.tag] forKey:kUserSelectedRole];
    [UserDefaults synchronize];
}

@end
