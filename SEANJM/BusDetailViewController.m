//
//  BusDetailViewController.m
//  SEANJM
//
//  Created by Abdul Kareem on 9/16/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "BusDetailViewController.h"
#import "HeaderView.h"
#import "TwoTitleCell.h"
#import "Utility.h"
#import "BTSData.h"
#import "UserInformation.h"
#import "SenderReceiverActionCell.h"
#import "ConfirmationCell.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "Macros.h"
#import "LanguageController.h"

#define BTSuserActionSend @"http://213.236.53.172/snbts/restserviceimpl.svc/userActionService"

@interface BusDetailViewController ()
{
    ConfirmationCell *footerView;
    BOOL oneTimeMEssage;
}
@property (nonatomic)NSMutableDictionary        *busDetailsDic;
@property (nonatomic,weak)IBOutlet UITableView  *routeTableView;
@property (nonatomic)NSString                   *routeCellIdentifier;
@property (nonatomic)NSMutableArray             *rowPlaceHolder;
@property (nonatomic)NSString                   *addUserFooterIdentifier;
@property (nonatomic)MBProgressHUD              *hudview;
@property (nonatomic)NSMutableDictionary        *actionsParameters;
@property (nonatomic)NSMutableArray             *radioButtonArray;
@property (nonatomic)NSString                   *currentRoundKey;
@property (nonatomic)int currentRoundNo;
@end

@implementation BusDetailViewController

-(id)initWithSelectedBus :(NSDictionary *)bus_detail
{
    self =[super init];
    _busDetailsDic =[NSMutableDictionary dictionaryWithDictionary:bus_detail];
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    _radioButtonArray  = [NSMutableArray array];
    _actionsParameters = [NSMutableDictionary dictionary];
    [_actionsParameters setObject:@"0" forKey:@"actionId"];
    
    _routeCellIdentifier = @"TwoTitleCell";
    _addUserFooterIdentifier =@"ConfirmationCell";
    [_routeTableView registerNib:[UINib nibWithNibName:_routeCellIdentifier bundle:[NSBundle mainBundle]]forCellReuseIdentifier:_routeCellIdentifier];
    
    [_routeTableView registerNib:[UINib nibWithNibName:@"HeaderView" bundle:[NSBundle mainBundle]]forCellReuseIdentifier:@"HeaderCell"];
    [_routeTableView registerNib:[UINib nibWithNibName:@"SenderReceiverActionCell" bundle:[NSBundle mainBundle]]forCellReuseIdentifier:@"SenderReceiverActionCell"];
    
    [_routeTableView registerNib:[UINib nibWithNibName:_addUserFooterIdentifier bundle:[NSBundle mainBundle]]forCellReuseIdentifier:_addUserFooterIdentifier];

    if([[UserInformation getUserSelectedRoleId] isEqualToString:@"0"])
        _currentRoundKey = @"senderCurrentRoundsCount";
    else
        _currentRoundKey = @"receiverCurrentRoundsCount";
        
    
    [self addmoreInformtionIntoBusDic];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.backItem.title = LocalizedString(@"back");
    self.navigationController.navigationBar.topItem.title = LocalizedString(@"busInfo");
}

-(void)addmoreInformtionIntoBusDic
{
    NSString *locationID;
   //NSDictionary *busLocationInfo;
    
    //busLocationInfo = [BTSData getBusInfoFromBusId:[_busDetailsDic objectForKey:@"busId"]];

    
    if([[UserInformation getUserSelectedRoleId] isEqualToString:@"0"])
        locationID = [_busDetailsDic objectForKey:@"senderLocationId"];
    else
        locationID = [_busDetailsDic objectForKey:@"receiverLocationId"];
    

    [_busDetailsDic setObject:[BTSData getRouteNameWithRouteId:[_busDetailsDic objectForKey:@"routeId"]] forKey:@"routeName"];
    [_busDetailsDic setObject:[BTSData getLocationNameWithLocationId:locationID] forKey:@"locationName"];

    _rowPlaceHolder = [NSMutableArray arrayWithObjects:@"routeName",@"companyName",@"plateNumber",@"locationName",@"driverName",@"model",@"busCapacity",@"guiderName",@"guiderMobile",@"roundsNumber", nil];
    
    _currentRoundNo = [[_busDetailsDic objectForKey:_currentRoundKey] intValue];
}
#pragma mark -
#pragma mark UITableView DataSource
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    HeaderView *headerView = [tableView dequeueReusableCellWithIdentifier:@"HeaderCell"];
    if(section == 0)
        [headerView setTitle:LocalizedString(@"busDetails")];
    else
        [headerView setTitle:LocalizedString(@"busAction")];
    
    return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section;
{
    if(section ==1)
    {
        if(!footerView)
        {
    footerView = [tableView dequeueReusableCellWithIdentifier:_addUserFooterIdentifier];
    [footerView.btnAdd addTarget:self action:@selector(sendButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [footerView.btnCancel addTarget:self action:@selector(cancelButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [footerView.btnAdd setTitle:LocalizedString(@"send") forState:UIControlStateNormal];
    [footerView.btnCancel setTitle:LocalizedString(@"cancel") forState:UIControlStateNormal];
    [footerView.btnAdd.layer setCornerRadius:4.0];
    [footerView.btnAdd setClipsToBounds:YES];
    [footerView.btnCancel.layer setCornerRadius:4.0];
    [footerView.btnCancel setClipsToBounds:YES];
        [footerView setOpaque:YES];
        

        [footerView.btnAdd setUserInteractionEnabled:NO];
        [footerView.btnAdd setHighlighted:YES];

        }
    return footerView;
    }
    
    else
       return  [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section;
{
    if(section ==1)
        return 80.0;
    else
        return 0.0;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section ==0)
        return _rowPlaceHolder.count;
    else
        return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if(indexPath.section == 0 )
    {
    TwoTitleCell *routeCell =[tableView dequeueReusableCellWithIdentifier:_routeCellIdentifier];
        [routeCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    routeCell.lblLeftTitle.text = [NSString stringWithFormat:@"%@",[_busDetailsDic objectForKey:[_rowPlaceHolder objectAtIndex:indexPath.row]]];
        [routeCell.lblLeftTitle setFont:[UIFont fontWithName:HELVETICA_BOLD size:14.0]];
    routeCell.lblRightTitle.text = [NSString stringWithFormat:@"%@: ",LocalizedString([_rowPlaceHolder objectAtIndex:indexPath.row])];
    
    [routeCell.btnImage setHidden:YES];
    
    return routeCell;
    }
    else
    {
         SenderReceiverActionCell *actionView = [tableView dequeueReusableCellWithIdentifier:@"SenderReceiverActionCell"];
        [actionView.btnSend.layer setBorderWidth:1.5];
        [actionView.btnSend.layer setBorderColor:[UIColor blackColor].CGColor];
        [actionView.btnSend.layer setCornerRadius:13.5];
        
        [actionView.lblsubTitle setText:[NSString stringWithFormat:@"* %@ :%@ ",LocalizedString(@"currentRoundNo"),[_busDetailsDic objectForKey:_currentRoundKey]]];
        [actionView.btnSend addTarget:self action:@selector(actionButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [actionView.btnSend setClipsToBounds:YES];
        [actionView.btnSend setTag:indexPath.row];
        
        if(![_radioButtonArray containsObject:actionView.btnSend])
            [_radioButtonArray addObject:actionView.btnSend];
        
        if(indexPath.row ==0)
            [actionView.lblTitle setText:LocalizedString(@"addActionMessage")];
        else
            [actionView.lblTitle setText:LocalizedString(@"undoActionMessage")];
        
        
        return actionView;
    }
}

#pragma mark - TableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
}

-(BOOL)checkInputValuesValidation
{
    return YES;
}

#pragma mark -
#pragma mark - IBActions
-(IBAction)addButtonTapped:(id)sender
{
    
}

-(IBAction)sendButtonTapped:(id)sender
{
    if(![Utility checkInternetConnection])
        return;
    if(!_hudview)
    {
    _hudview = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _hudview.mode = MBProgressHUDModeIndeterminate;
    _hudview.labelText = @"Loading...";
    [_hudview setDimBackground:YES];
    }
    [_actionsParameters setObject:[UserInformation getUserId] forKey:@"userId"];
    [_actionsParameters setObject:[_busDetailsDic objectForKey:@"routeId"] forKey:@"routeId"];
    [_actionsParameters setObject:[_busDetailsDic objectForKey:@"groupId"] forKey:@"groupId"];
    [_actionsParameters setObject:[_busDetailsDic objectForKey:@"guiderId"] forKey:@"guiderId"];
    [_actionsParameters setObject:[_busDetailsDic objectForKey:@"busId"] forKey:@"busId"];
    [_actionsParameters setObject:@"50" forKey:@"capacity"];
    [_actionsParameters setObject:[UserInformation getUserSelectedRoleId] forKey:@"roleId"];
    
    if([[UserInformation getUserSelectedRoleId] isEqualToString:@"0"])
        [_actionsParameters setObject:[_busDetailsDic objectForKey:@"senderLocationId"] forKey:@"locationId"];
    else
        [_actionsParameters setObject:[_busDetailsDic objectForKey:@"receiverLocationId"] forKey:@"locationId"];
    
    
    NSDictionary *sendingDictionary = @{@"userAction": _actionsParameters};
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@",BTSuserActionSend];
    
    NSLog(@"Request Buses %@ %@",serviceURL,_actionsParameters);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager POST:serviceURL parameters:sendingDictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"Bus Action %@", responseObject);
        if(![responseObject  isKindOfClass:[NSNull class]])
        {
            if([[[responseObject objectForKey:@"Result"] objectForKey:@"result"] intValue] == 1)
                
                [Utility showAlertViewWithTitle:LocalizedString(@"success")  message:LocalizedString(@"submitedsuccessfully") cancelButtonTile:LocalizedString(@"ok")];
            
                _currentRoundNo =[[[responseObject objectForKey:@"Result"] objectForKey:@"RoundNumberCurrent"] intValue];
                [_busDetailsDic setObject:[NSString stringWithFormat:@"%d",_currentRoundNo] forKey:_currentRoundKey];
            
                [self changeRadioButtonStatus];
                [self disableSendButton];
                [_routeTableView reloadData];
            
            DispatchEvent(@"loadAllBussesNotification", nil);
        }
        [_hudview setHidden:YES];
        _hudview = nil;
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"BTS Error: %@", error);
        [_hudview setHidden:YES];
        _hudview = nil;
        [Utility showAlertViewWithTitle:LocalizedString(@"error")  message:LocalizedString(@"error") cancelButtonTile:LocalizedString(@"ok")];
    }];
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(buttonIndex !=0){
        // do something
        [self sendButtonTapped:nil];
    }
}

-(void)changeRadioButtonStatus
{
    for (UIButton *radioBtn in _radioButtonArray) {
        [radioBtn setSelected:NO];
        [radioBtn setBackgroundColor:[UIColor whiteColor]];
    }
    
}

//ActionId 0 mean Add 1 mean delete
-(IBAction)actionButtonTapped:(UIButton *)sender
{
    if(sender.selected)
    {
        [sender setSelected:NO];
        [self disableSendButton];
        [sender setBackgroundColor:[UIColor whiteColor]];
    }
    else
    {
        if([self iscurrentRoundNoValid:sender.tag])
        {
            [self changeRadioButtonStatus];
            [sender setBackgroundColor:[UIColor colorWithRed:0/255.0 green:133.0/255.0 blue:0/255.0 alpha:1.0]];
            [sender setSelected:YES];

            if(sender.tag ==1)
            {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:LocalizedString(@"confirmMessage") delegate:self cancelButtonTitle:LocalizedString(@"cancel") otherButtonTitles:LocalizedString(@"confirm"), nil];
            oneTimeMEssage = YES;
            [alert show];
            }
            [self enableSendButton];
        }
        else
        {
            [sender setSelected:NO];
            [self disableSendButton];
        }
    }
    [_routeTableView reloadData];
}

-(IBAction)cancelButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)disableSendButton
{
    [footerView.btnAdd setUserInteractionEnabled:NO];
    [footerView.btnAdd setHighlighted:YES];
}
-(void)enableSendButton
{
    [footerView.btnAdd setUserInteractionEnabled:YES];
    [footerView.btnAdd setHighlighted:NO];
}

-(BOOL)iscurrentRoundNoValid :(NSInteger )buttomIndex
{
    /*If current round > Max Round so we can add
     if current round > 0 and current round <= max round so we can undo
     buttonIndex =0 means Add button
     buttonIndex =1 means Undo button
     */
    
    _currentRoundNo = [[_busDetailsDic objectForKey:_currentRoundKey] intValue];
    int maxRoundsNo = [[_busDetailsDic objectForKey:@"roundsNumber"] intValue];
    
    //To Add
    if(buttomIndex ==0)
    {
        if(_currentRoundNo < maxRoundsNo)
        {
            [_actionsParameters setObject:@"0" forKey:@"actionId"];
            return YES;
        }
        else
        {
            [self changeRadioButtonStatus];
            [Utility showAlertViewWithTitle:@"" message:LocalizedString(@"MaximumnumberofBusroundisfinished") cancelButtonTile:LocalizedString(@"ok")];
                return NO;
        }
    }
    //Undo
    else
    {
        if((_currentRoundNo >0 )&& (_currentRoundNo <= maxRoundsNo))
        {
            [_actionsParameters setObject:@"1" forKey:@"actionId"];
            return YES;
        }
        else
        {
            [self changeRadioButtonStatus];
            [Utility showAlertViewWithTitle:@"" message:LocalizedString(@"thereisnoroundtoundo⁠⁠⁠⁠") cancelButtonTile:LocalizedString(@"ok")];
            return NO;
        }
    }
}
@end
