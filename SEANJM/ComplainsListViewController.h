//
//  ComplainsListViewController.h
//  SEANJM
//
//  Created by Randa Al-Sadek on 9/8/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ComplainsListViewController : UIViewController

@property (nonatomic, strong) NSArray *complainsArray;
@property (nonatomic) int complainLayer;

@end
