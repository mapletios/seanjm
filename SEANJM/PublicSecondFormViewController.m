//
//  PublicSecondFormViewController.m
//  SEANJM
//
//  Created by Randa Al-Sadek on 7/23/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "PublicSecondFormViewController.h"
#import "MapCommViewController.h"
#import "ConfigurationManager.h"
#import "ActionSheetStringPicker.h"
#import "Utility.h"
#import "LanguageController.h"
#import "Macros.h"
#import "PNTToolbar.h"

@interface PublicSecondFormViewController ()<UITextFieldDelegate, UITextViewDelegate,UITextViewDelegate> {
    
    __weak IBOutlet UIScrollView *scrollView;
    __weak IBOutlet UILabel *poinLatLbl;
    __weak IBOutlet UILabel *pointLonLbl;
    __weak IBOutlet UITextField *poinLatTxt;
    __weak IBOutlet UITextField *pointLonTxt;
    __weak IBOutlet UILabel *personNumLbl;
    __weak IBOutlet UITextField *personNumTxt;
    __weak IBOutlet UILabel *passportLbl;
    __weak IBOutlet UITextField *passportTxt;
    __weak IBOutlet UILabel *genderLbl;
    __weak IBOutlet UITextField *genderTxt;
    __weak IBOutlet UILabel *ageLbl;
    __weak IBOutlet UITextField *ageTxt;
    __weak IBOutlet UILabel *personStatusLbl;
    __weak IBOutlet UITextView *personStatusTxt;
    __weak IBOutlet UITextField *personStatusHos;
    __weak IBOutlet UILabel *commentsLbl;
    __weak IBOutlet UITextView *commentsTxt;
    __weak IBOutlet UIButton *sendButton;
    __weak IBOutlet UIButton *mapButton;
    __weak IBOutlet UIActivityIndicatorView *activityIndicator;
    
    NSMutableArray  *inputAccessoryArray;
    
    ConfigurationManager *config;
    NSMutableArray *mapFeature;
}

@end

@implementation PublicSecondFormViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    NSString *title;
    switch (self.formType) {
        case 1:
            title = LocalizedString(@"ambulanceComplain");
            break;
        case 2:
            title = LocalizedString(@"hospitalsComplain");
            break;
        case 4:
            title = LocalizedString(@"guideComplain");
            break;
        default:
            break;
    }

    [self.navigationItem setTitle:title];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    tap.cancelsTouchesInView = NO;
    
    [self.view addGestureRecognizer:tap];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(returnMapPoint:) name:@"ReturnMapPoint" object:nil];
    
    [Utility setFormsWithPointLat:poinLatLbl pointLon:pointLonLbl mapButton:mapButton sendButton:sendButton username:nil userPhone:nil];
    [Utility setTextViewBorder:commentsTxt];
    [Utility setTextViewBorder:personStatusTxt];
    
    config = [[ConfigurationManager alloc]init];
    
    [Utility setScrollViewSize:scrollView];
    [Utility setWhiteShadowToAllLabels:scrollView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    inputAccessoryArray = [NSMutableArray array];
    for (id subview in scrollView.subviews) {
        if([subview isKindOfClass:[UITextField class]] || [subview isKindOfClass:[UITextView class]])
        {
            [inputAccessoryArray addObject:subview];
        }
    }
    
    PNTToolbar *toolbar = [PNTToolbar defaultToolbar];
    toolbar.navigationButtonsTintColor = [UIColor blueColor];
    toolbar.mainScrollView = scrollView;
    toolbar.inputFields = inputAccessoryArray;
    
        
    
}
-(void)viewDidDisappear:(BOOL)animated
{
    if(![self.navigationController.viewControllers containsObject:self])
        DispatchEvent(kStopLocation, nil);

}


-(void)viewWillAppear:(BOOL)animated {
    
    DispatchEvent(kStartLocation, nil);
    
    if (self.formType == 1 || self.formType == 4) {
        [personStatusHos removeFromSuperview];
        [inputAccessoryArray removeObject:personStatusHos];
        [personStatusTxt setHidden:NO];
    }
    else {
        [personStatusHos setHidden:NO];
        [personStatusTxt removeFromSuperview];
        [inputAccessoryArray removeObject:personStatusTxt];
    }
    
    [Utility applyBlueRoundedButton:sendButton];
}

#pragma mark -
#pragma mark - Keyboard Delegate


-(void)keyboardWillHide:(NSNotification *)notification
{
    [scrollView setContentOffset:CGPointZero animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma UITextFieldDelegate

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if(textField == personNumTxt)
    {
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        
        return newLength <= 10 || returnKey;
    }
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
     if (textField == genderTxt) {
        [self.view endEditing:YES];
        
        NSArray *gender = [[NSArray alloc]initWithObjects:LocalizedString(@"male"), LocalizedString(@"female"), nil];
        
        [ActionSheetStringPicker showPickerWithTitle:@"" rows:gender initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
            
            textField.text = selectedValue;
            
        } cancelBlock:nil origin:textField];

    }
    else if (textField == personStatusHos && self.formType == 2) {
        [self.view endEditing:YES];
        
        NSArray *status = [[NSArray alloc]initWithObjects:LocalizedString(@"admittance"), LocalizedString(@"improvement"), LocalizedString(@"death"), nil];
        
        [ActionSheetStringPicker showPickerWithTitle:@"" rows:status initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
            
            textField.text = selectedValue;
            
        } cancelBlock:nil origin:textField];
    }
    else {
        [scrollView setContentOffset:CGPointMake(0, CGRectGetMinY(textField.frame)-5) animated:YES];
        return YES;
    }
    
    return NO;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [scrollView setContentOffset:CGPointMake(0, CGRectGetMinY(textView.frame)-5) animated:YES];
}

//-(void)textFieldDidEndEditing:(UITextField *)textField
//{
//    if(textField  == personNumTxt)
//    {
//        personNumTxt.text = [personNumTxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//        if(((personNumTxt.text.length < 10) && (personNumTxt.text.length >0)) || ([personNumTxt.text isEqualToString:@""]))
//            [self showPilgrimIdAlert];
//    }
//}

-(void)showPilgrimIdAlert
{
    [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"pilgrimIDError") cancelButtonTile:LocalizedString(@"ok")];
}
- (IBAction)sendButtonPressed:(id)sender {
    
    if(![Utility checkInternetConnection])
        return;
    
    else if(![Utility isCurrentLocationAvailable])
    {
        [Utility showAlertViewWithTitle:LocalizedString(@"locationservice") message:LocalizedString(@"checklocationservice") cancelButtonTile:LocalizedString(@"ok")];
        return;
    }
    personNumTxt.text = [personNumTxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if(((personNumTxt.text.length < 10) && (personNumTxt.text.length >0)) || ([personNumTxt.text isEqual:nil]))
    {
        [self showPilgrimIdAlert];
        return;
    }
   else if ([genderTxt.text length] == 0 || [ageTxt.text length] == 0 || ([personStatusHos.text length] == 0 && [personStatusTxt.text length] == 0) ) {
        [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"emptyFields") cancelButtonTile:LocalizedString(@"ok")];
    }
    else if ([poinLatTxt.text length] == 0 || [pointLonTxt.text length] == 0) {
        [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"emptyMapPoint") cancelButtonTile:LocalizedString(@"ok")];
    }
    else {

        NSString *personStatus;
        
        if (self.formType == 1 || self.formType == 4) {
            personStatus = personStatusTxt.text;
        }
        else if (self.formType == 2) {
            personStatus = personStatusHos.text;
        }
        
        NSArray *data = [[NSArray alloc]initWithObjects:[NSNumber numberWithInt:self.formType], genderTxt.text, ageTxt.text, personStatus,  poinLatTxt.text, pointLonTxt.text, nil];
        NSArray *dataKeys = [[NSArray alloc]initWithObjects:@"mainTypeId", @"gender", @"age", @"healthStatus", @"xPosition", @"yPosition", nil];
        NSMutableDictionary *complaintDic = [[NSMutableDictionary alloc]initWithObjects:data forKeys:dataKeys];
        

        
        
        if ([personNumTxt.text length] > 0)
            [complaintDic setObject:personNumTxt.text forKey:@"pilgrimID"];
        else
            [complaintDic setObject:@"0" forKey:@"pilgrimID"];
        
        if ([passportTxt.text length] > 0)
            [complaintDic setObject:passportTxt.text forKey:@"passportID"];
        if ([commentsTxt.text length] > 0)
            [complaintDic setObject:commentsTxt.text forKey:@"comments"];
       
        [activityIndicator setHidden:NO];
        [activityIndicator startAnimating];
        
        [config sendPublicAffairsComplaint:complaintDic completion:^{
            [self resetView];
            [Utility showAlertViewWithTitle:LocalizedString(@"success") message:LocalizedString(@"successMessage") cancelButtonTile:LocalizedString(@"ok")];
            [activityIndicator stopAnimating];
            [self.navigationController popViewControllerAnimated:YES];

        }failure:^{
            [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"reportNotSent") cancelButtonTile:LocalizedString(@"ok")];
            [activityIndicator stopAnimating];
        }];
    }
}

- (IBAction)mapButtonPressed:(id)sender {
    
    if(![Utility checkInternetConnection])
        return;
    
    MapCommViewController *mapView = [[MapCommViewController alloc]init];
    mapView.mapType = 1;
    mapView.showReport = NO;
    [self.navigationController pushViewController:mapView animated:YES];
}

- (void)returnMapPoint:(NSNotification *)notification {
    mapFeature = notification.object;
    poinLatTxt.text = [mapFeature objectAtIndex:0];
    pointLonTxt.text = [mapFeature objectAtIndex:1];
}

- (void)resetView {
    personNumTxt.text = @"";
    passportTxt.text = @"";
    genderTxt.text = @"";
    ageTxt.text = @"";
    personStatusHos.text = @"";
    personStatusTxt.text = @"";
    commentsTxt.text = @"";
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

@end
