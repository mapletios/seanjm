//
//  PublicAffairsOptionsViewController.m
//  SEANJM
//
//  Created by Randa Al-Sadek on 7/20/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "PublicAffairsOptionsViewController.h"
#import "PublicFirstFormViewController.h"
#import "PublicSecondFormViewController.h"
#import "LanguageController.h"
#import "Utility.h"
#import "MapCommViewController.h"

@interface PublicAffairsOptionsViewController ()

@end

@implementation PublicAffairsOptionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithTitle:LocalizedString(@"back")
                                                                 style:UIBarButtonItemStylePlain
                                                                target:nil
                                                                action:nil];
    
    [self.navigationItem setBackBarButtonItem:backItem];
    
    [self.navigationItem setTitle:LocalizedString(@"healthComplain")];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)firstButtonPressed:(id)sender {
    PublicSecondFormViewController *view = [[PublicSecondFormViewController alloc]init];
    view.formType = 1;
    [self.navigationController pushViewController:view animated:YES];
}

- (IBAction)secondButtonPressed:(id)sender {
    PublicSecondFormViewController *view = [[PublicSecondFormViewController alloc]init];
    view.formType = 2;
    [self.navigationController pushViewController:view animated:YES];
}

- (IBAction)thirdButtonPressed:(id)sender {
    PublicFirstFormViewController *view = [[PublicFirstFormViewController alloc]init];
    [self.navigationController pushViewController:view animated:YES];
}

- (IBAction)fourthButtonPressed:(id)sender {
    PublicSecondFormViewController *view = [[PublicSecondFormViewController alloc]init];
    view.formType = 4;
    [self.navigationController pushViewController:view animated:YES];

}

- (IBAction)reportsButtonPressed:(id)sender {
    if(![Utility checkInternetConnection])
        return;
    
    MapCommViewController *mapView = [[MapCommViewController alloc]init];
    mapView.mapType = 5;
    mapView.showReport = YES;
    [self.navigationController pushViewController:mapView animated:YES];
}

@end
