//
//  ComplainStatusViewController.h
//  SEANJM
//
//  Created by Randa Al-Sadek on 9/10/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ComplainStatusViewController : UIViewController

@property (nonatomic, strong)NSDictionary *complain;
@end
