//
//  AddBusesViewController.h
//  SEANJM
//
//  Created by Abdul Kareem on 9/9/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddBusesViewController : UIViewController
-(id)initWithNumberOfRoundsView;
-(id)initWithBusArray :(NSMutableArray *)bus_array;
-(id)initWithSelectedTask :(NSDictionary *)taskdictionary;
-(id)initWithinfo :(NSDictionary *)infodictionary;
@end
