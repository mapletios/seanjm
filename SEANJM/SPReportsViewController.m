//
//  SPReportsViewController.m
//  SEANJM
//
//  Created by Abdul Kareem on 9/18/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "SPReportsViewController.h"
#import "MBProgressHUD.h"
#import "Utility.h"
#import "Macros.h"
#import "BTSData.h"
#import "ConfigurationManager.h"
#import "HeaderView.h"
#import "TwoTitleCell.h"
#import "LanguageController.h"
#import "SAGroupViewController.h"
#import "AFNetworking.h"
#import "UserInformation.h"
#import "ActionSheetStringPicker.h"

#define BTSSAgetAllGroupsForSpesificRoute @"http://213.236.53.172/snbts/restserviceimpl.svc/getAllGroupsForSpesificRoute"

#define BTSSAgetReportAccordingGroupOnSpesificRoute @"http://213.236.53.172/snbts/restserviceimpl.svc/getReportAccordingGroupOnSpesificRoute"

@interface SPReportsViewController ()
{
    ConfigurationManager *config;
    BOOL isFromRouteOption;
}
@property (nonatomic) MBProgressHUD *hudview;
@property (nonatomic)NSMutableArray *reportKeysArray;
@property (nonatomic,weak)IBOutlet UITableView *reportTableView;
@property (nonatomic)NSString *reportCellIdentifier;
@property (nonatomic)NSMutableDictionary *inheritedSelectedRoute;
@property (nonatomic)NSArray *groupsInfoKeys;
@property (nonatomic)NSMutableArray *groupArray;

@end

@implementation SPReportsViewController
-(id)initWithSelectedRoute :(NSDictionary *)selected_route isFromRoute :(BOOL)isfrom_route
{
    self =[super init];

    if(isfrom_route)
    {
        _inheritedSelectedRoute = [NSMutableDictionary dictionaryWithDictionary:selected_route];
    isFromRouteOption = isfrom_route;
    
    //For these keys we need to show Detail information
    _groupsInfoKeys =[NSArray arrayWithObjects:@"activeGroupsCount",
                                               @"finishedGroupsCount",
                                               @"notStartedGroupsCount",
                                               @"groupsTotalCountsNumber",nil];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    config =[[ConfigurationManager alloc]init];
    _groupArray =[NSMutableArray array];
    
    if(isFromRouteOption)
        _reportKeysArray = [NSMutableArray arrayWithArray:[[_inheritedSelectedRoute objectForKey:@"btsroutegroups"] allKeys]];
    else
    {
        _reportKeysArray = [NSMutableArray array];
        [self openPickerViewToLoadGroups];
    }
    
    _reportCellIdentifier = @"TwoTitleCell";
    [_reportTableView registerNib:[UINib nibWithNibName:_reportCellIdentifier bundle:[NSBundle mainBundle]]forCellReuseIdentifier:_reportCellIdentifier];
    
    [_reportTableView registerNib:[UINib nibWithNibName:@"HeaderView" bundle:[NSBundle mainBundle]]forCellReuseIdentifier:@"HeaderCell"];
   
    
    [_reportTableView reloadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.topItem.title = [BTSData getRouteNameWithRouteId:[_inheritedSelectedRoute objectForKey:krouteId]];
    self.navigationController.navigationBar.backItem.title = LocalizedString(@"back");
    
    if(!isFromRouteOption)
    {
        UIBarButtonItem *btnNextButton =[[UIBarButtonItem alloc] initWithTitle:LocalizedString(@"ChangeGroup") style:UIBarButtonItemStylePlain target:self action:@selector(openPickerViewToLoadGroups)];
        [self.navigationItem setRightBarButtonItem:btnNextButton];
    }
}

-(void)openPickerViewToLoadGroups
{
    if(_groupArray.count ==0 )
    {
            _hudview = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            _hudview.mode = MBProgressHUDModeIndeterminate;
            _hudview.labelText = @"Loading...";
            [_hudview setDimBackground:YES];
    
        NSString *serviceURl =[NSString stringWithFormat:@"%@/%@",BTSSAgetAllGroupsForSpesificRoute,[[UserInformation getUserSelectedRoute] objectForKey:@"id"]];
        [config LoadGroupsForSpecifRoute:serviceURl completion:^{
            _groupArray = (NSMutableArray *)[BTSData getAllGroups];
            [self shoPickerView];
            [_hudview setHidden:YES];
        } failure:^{
            [_hudview setHidden:YES];
        }];
    }
    else
        [self shoPickerView];
}

-(void)shoPickerView
{
    if(_groupArray.count >0)
    {
        [ActionSheetStringPicker showPickerWithTitle:LocalizedString(@"selectgroup") rows:_groupArray initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
            
            [self loadSuperAdminReportsForGroup:selectedValue];
        } cancelBlock:^(ActionSheetStringPicker *picker) {
            
        } origin:self.view];
    }
    else
    {
        [Utility showAlertViewWithTitle:@"" message:LocalizedString(@"thereisnogroup") cancelButtonTile:LocalizedString(@"ok")];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)loadSuperAdminReportsForGroup :(NSString *)groupId
{
    if(![Utility checkInternetConnection])
        return;
    
    if(!_hudview)
    {
    _hudview = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _hudview.mode = MBProgressHUDModeIndeterminate;
    _hudview.labelText = @"Loading...";
    [_hudview setDimBackground:YES];
    }
    NSString *serviceURl =[NSString stringWithFormat:@"%@",BTSSAgetReportAccordingGroupOnSpesificRoute];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSMutableDictionary *parameters =[NSMutableDictionary dictionaryWithObject:groupId forKey:kgroupId];
    [parameters setObject:[[UserInformation getUserSelectedRoute] objectForKey:@"id"] forKey:krouteId];
    
    
    [manager POST:serviceURl parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //NSLog(@"Group INfo List %@", responseObject);
        if(![responseObject  isKindOfClass:[NSNull class]])
        {
            if([responseObject objectForKey:@"Result"])
            {
                _inheritedSelectedRoute = [responseObject objectForKey:@"Result"];
                _reportKeysArray = [NSMutableArray arrayWithArray:[_inheritedSelectedRoute allKeys]];
                [_reportTableView reloadData];
            }
            [_hudview setHidden:YES];
            _hudview = nil;
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"BTS Error: %@", error);
        [_hudview setHidden:YES];
        _hudview = nil;
    }];
}

#pragma mark -
#pragma mark UITableView DataSource

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    HeaderView *headerView = [tableView dequeueReusableCellWithIdentifier:@"HeaderCell"];
    [headerView setTitle:LocalizedString(@"reportsBTS")];
    
    return headerView;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _reportKeysArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TwoTitleCell *reportCell =[tableView dequeueReusableCellWithIdentifier:_reportCellIdentifier forIndexPath:indexPath];
    
    if(isFromRouteOption)
    {
    reportCell.lblLeftTitle.text = [NSString stringWithFormat:@"%@",
                                    [[_inheritedSelectedRoute objectForKey:@"btsroutegroups"]
                                     objectForKey:[_reportKeysArray objectAtIndex:indexPath.row]]];
    
    }
    else
    {
        reportCell.lblLeftTitle.text = [NSString stringWithFormat:@"%@",
                                        [_inheritedSelectedRoute objectForKey:[_reportKeysArray objectAtIndex:indexPath.row]]];
    }
    reportCell.lblRightTitle.text = [NSString stringWithFormat:@"  %@ :",LocalizedString([_reportKeysArray objectAtIndex:indexPath.row])];
    
    
    
    if(![_groupsInfoKeys containsObject:[_reportKeysArray objectAtIndex:indexPath.row]])
    {
        [reportCell.btnImage setHidden:YES];
        [reportCell.btnImage setUserInteractionEnabled:NO];
    }
    else
    {
        [reportCell.btnImage setContentMode:UIViewContentModeScaleAspectFit];
        [reportCell.btnImage setBackgroundImage:[UIImage imageNamed:@"ic_info"] forState:UIControlStateNormal];
        [reportCell.btnImage.superview setTag:indexPath.row];
        [reportCell.btnImage addTarget:self action:@selector(leftButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    [reportCell setBackgroundColor:[UIColor whiteColor]];
    
    
    return reportCell;
}

#pragma mark - TableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

-(IBAction)leftButtonPressed:(UIButton *)sender
{
    if([[[_inheritedSelectedRoute objectForKey:@"btsroutegroups"] objectForKey:[_reportKeysArray objectAtIndex:sender.superview.tag]] intValue] >0)
    {
        NSString *groupInfokey = [_reportKeysArray objectAtIndex:sender.superview.tag];
        
        SAGroupViewController *objSAGroup =[[SAGroupViewController alloc]initWithSelectedOption:groupInfokey];
        [self.navigationController pushViewController:objSAGroup animated:YES];
    }
    else
        [Utility showAlertViewWithTitle:@"" message:LocalizedString(@"thereisNoGroup") cancelButtonTile:LocalizedString(@"ok")];
}


@end
