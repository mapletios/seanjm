//
//  Utility.h
//  SEANJM
//
//  Created by Randa Al-Sadek on 7/23/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Utility : NSObject

+ (void)setTextViewBorder: (UITextView *)textView;

+ (void)showAlertViewWithTitle: (NSString *)title message:(NSString *)message cancelButtonTile:(NSString *)cancelButtonTitle;

+ (void)setFormsWithPointLat: (UILabel *)pointLat pointLon:(UILabel *)pointLon mapButton:(UIButton *)mapButton sendButton:(UIButton *)sendButton username:(UITextField *)username userPhone:(UITextField *)userPhone;

+ (NSNumber *)setButtonChecked:(NSNumber *)option button:(UIButton *)button;

+ (NSNumber *)setButtonUnChecked:(NSNumber *)option button:(UIButton *)button;

+(void)applyBlueRoundedButton :(id)button;

+(void)setScrollViewSize :(UIScrollView *)scrollview;

+(void)setNavigationBarProperties :(UINavigationController *)navigation;

+(void)setWhiteShadowToAllLabels :(id)view;

+(void)setBlackShadowToAllLabels :(id)view;

+(void)setBlackShadow :(UILabel *)label;

+(BOOL)checkInternetConnection;

+(void)LoadVisualEffectView :(UIView *)view;

+(BOOL)isValueNilNullOrEmpty :(id)value;

+(BOOL)isCurrentLocationAvailable;

+(NSString *)getcurrentDateOnly;
+ (NSString *) replaceArabicNumbers:(NSString *) str;
@end
