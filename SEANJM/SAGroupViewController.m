//
//  SAGroupViewController.m
//  SEANJM
//
//  Created by Abdul Kareem on 9/18/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "SAGroupViewController.h"

#import "MBProgressHUD.h"
#import "Utility.h"
#import "Macros.h"
#import "BTSData.h"
#import "ConfigurationManager.h"
#import "HeaderView.h"
#import "InfoDetailCell.h"
#import "LanguageController.h"
#import "AFNetworking.h"
#import "UserInformation.h"
#define BTSSAgetAllGroupsAccordingSpesificRouteWithTypes @"http://213.236.53.172/snbts/restserviceimpl.svc/getAllGroupsAccordingSpesificRouteWithTypes"


@interface SAGroupViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic)NSString       *selectedKey;
@property (nonatomic)MBProgressHUD  *hudview;
@property (nonatomic)NSMutableArray *groupInformationArray;
@property (nonatomic)NSString       *groupTypeId;
@property (nonatomic)IBOutlet UITableView *groupInfoTableView;
@property (nonatomic)NSString *infoCellIdentifier;
@property (nonatomic)NSArray *keysArray;
@end

@implementation SAGroupViewController
-(id)initWithSelectedOption :(NSString *)option_key
{
    self =[super init];
    
    _selectedKey =option_key;
    if([option_key isEqualToString:@"groupsTotalCountsNumber"])
        _groupTypeId = @"0";
    else if([option_key isEqualToString:@"activeGroupsCount"])
        _groupTypeId = @"1";
    else if([option_key isEqualToString:@"finishedGroupsCount"])
        _groupTypeId = @"2";
    else  if([option_key isEqualToString:@"notStartedGroupsCount"])
        _groupTypeId = @"3";
    
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    _infoCellIdentifier = @"InfoDetailCell";
    [_groupInfoTableView registerNib:[UINib nibWithNibName:_infoCellIdentifier bundle:[NSBundle mainBundle]]forCellReuseIdentifier:_infoCellIdentifier];
    
    [_groupInfoTableView registerNib:[UINib nibWithNibName:@"HeaderView" bundle:[NSBundle mainBundle]]forCellReuseIdentifier:@"HeaderCell"];
    
    _keysArray = [NSArray arrayWithObjects:@"groupManagerFullName",@"groupManagerMobileNumber", nil];
    [self loadGroupInformation];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.topItem.title = LocalizedString(_selectedKey);
    self.navigationController.navigationBar.backItem.title = LocalizedString(@"back");
}

-(void)loadGroupInformation
{
    if(![Utility checkInternetConnection])
        return;
    
    if(!_hudview)
    {
    _hudview = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _hudview.mode = MBProgressHUDModeIndeterminate;
    _hudview.labelText = @"Loading...";
    [_hudview setDimBackground:YES];
    }
    NSString *serviceURl =[NSString stringWithFormat:@"%@/%@",BTSSAgetAllGroupsAccordingSpesificRouteWithTypes,[[[UserInformation getUserSelectedRoute] objectForKey:@"id"] stringValue]];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager GET:serviceURl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //NSLog(@"Group INfo List %@", responseObject);
        if(![responseObject  isKindOfClass:[NSNull class]])
        {
            if([responseObject objectForKey:@"Result"])
            {
                NSArray *allgroupInfo = [NSArray arrayWithArray:[responseObject objectForKey:@"Result"]];
            
                NSArray *filteredarray = [allgroupInfo filteredArrayUsingPredicate:
                                      [NSPredicate predicateWithFormat:@"groupTypeId == %d",_groupTypeId.intValue]];
                _groupInformationArray = [NSMutableArray arrayWithArray:[[filteredarray firstObject] objectForKey:@"groups"]];
                [_groupInfoTableView reloadData];
            }
                [_hudview setHidden:YES];
            _hudview =nil;
            
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"BTS Error: %@", error);
        [_hudview setHidden:YES];
        _hudview =nil;
    }];
}

#pragma mark -
#pragma mark UITableView DataSource

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    HeaderView *headerView = [tableView dequeueReusableCellWithIdentifier:@"HeaderCell"];
    [headerView setTitle:[NSString stringWithFormat:@"%@: %@",LocalizedString(@"GroupID"),[[_groupInformationArray objectAtIndex:section]objectForKey:@"groupId"]]];
    
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _keysArray.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _groupInformationArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    InfoDetailCell *reportCell =[tableView dequeueReusableCellWithIdentifier:_infoCellIdentifier forIndexPath:indexPath];

        [reportCell setSelectionStyle:UITableViewCellSelectionStyleNone];
        int indexRow = indexPath.row % 3;
        reportCell.lblRightTitle.text = [NSString stringWithFormat:@"%@ :",LocalizedString([_keysArray objectAtIndex:indexRow])];
        reportCell.txtView.text = [NSString stringWithFormat:@"   %@   ",[[_groupInformationArray objectAtIndex:indexPath.section] objectForKey:[_keysArray objectAtIndex:indexRow]]];
    
    return reportCell;
}

#pragma mark - TableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}


-(IBAction)leftButtonPressed:(id)sender
{
    
}

@end
