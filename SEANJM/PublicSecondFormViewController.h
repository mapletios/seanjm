//
//  PublicSecondFormViewController.h
//  SEANJM
//
//  Created by Randa Al-Sadek on 7/23/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PublicSecondFormViewController : UIViewController

@property (nonatomic) int formType;

@end
