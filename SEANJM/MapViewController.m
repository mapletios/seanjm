//
//  MapViewController.m
//  SEANJM
//
//  Created by Randa Al-Sadek on 6/3/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "MapViewController.h"
#import "AFNetworking.h"
#import <ArcGIS/ArcGIS.h>
#import "Macros.h"
#import "LanguageController.h"
#import "CustomCalloutViewController.h"
#import "Utility.h"

#define tentsService @"http://213.236.53.172/arcgis/rest/services/NJM/MinaTentsService/MapServer/0"
#define officesService @"http://213.236.53.172/arcgis/rest/services/NJM/MinaTentsService/MapServer/1"
#define checkOperationsService @"http://213.236.53.172/arcgis/rest/services/NJM/TCSCheckOperationsService/MapServer/0"

@interface MapViewController () <AGSLayerDelegate, AGSCalloutDelegate, AGSQueryTaskDelegate,AGSMapViewLayerDelegate> {
    
    __weak IBOutlet AGSMapView *mapView;
    NSString *tentId;
    double XCoordinate;
    double YCoordinate;
    
    AGSFeatureLayer *checkOperationsFeatureLayer;
    AGSFeatureLayer *tentsFeatureLayer;
    AGSFeatureLayer *officeFeatureLayer;
    AGSLayerDefinition *layerDef;
    AGSLayerDefinition *layerDef2;

    AGSQueryTask *queryTask;
    AGSQuery *query;
    
    AGSEnvelope *fullExtent;
    
//    CustomCalloutViewController *customCallout;
}

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(zoomToFullExtent:) name:AGSFeatureLayerDidLoadFeaturesNotification object:nil];

    [mapView setAllowCallout:YES];
    
    [mapView.callout setDelegate:self];
    [mapView setLayerDelegate:self];
    [mapView setMaxScale:16.0];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]initWithTitle:LocalizedString(@"done") style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonPressed)];
    [self.navigationItem setRightBarButtonItem:doneButton];
 
    AGSOpenStreetMapLayer* tiledLayer = [[AGSOpenStreetMapLayer alloc] init];
    [tiledLayer setDelegate:self];
    [mapView insertMapLayer:tiledLayer withName:@"Basemap" atIndex:0];
   
    int layerID = 0 ;
    NSString *defString;
    
    if ([[UserDefaults objectForKey:@"officeNumber"] isEqual:@"-1"]) {
        defString = [NSString stringWithFormat:@"1=1"];
    }
    else {
        defString = [NSString stringWithFormat:@"OFFICE_ID = '%@'", [UserDefaults objectForKey:@"officeNumber"]];
    }
    
    layerDef = [AGSLayerDefinition  layerDefinitionWithLayerId:layerID definition:defString];
    
    officeFeatureLayer = [[AGSFeatureLayer alloc] initWithURL:[NSURL URLWithString:officesService] mode:AGSFeatureLayerModeOnDemand];
    [officeFeatureLayer setDelegate:self];
    [officeFeatureLayer setDefinitionExpression:layerDef.definition];
    [officeFeatureLayer setOutFields:@[@"*"]];
    [mapView insertMapLayer:officeFeatureLayer withName:@"Office Layer" atIndex:1];
    
    tentsFeatureLayer = [[AGSFeatureLayer alloc] initWithURL:[NSURL URLWithString:tentsService] mode:AGSFeatureLayerModeOnDemand];
    [tentsFeatureLayer setDelegate:self];
    [tentsFeatureLayer setDefinitionExpression:layerDef.definition];
    [tentsFeatureLayer setOutFields:@[@"*"]];
    [mapView insertMapLayer:tentsFeatureLayer withName:@"Tents Layer" atIndex:2];
    
    NSString *stringDef = [self createUsersQuery];
    layerDef2 = [AGSLayerDefinition layerDefinitionWithLayerId:layerID definition:stringDef];
    
    checkOperationsFeatureLayer = [[AGSFeatureLayer alloc] initWithURL:[NSURL URLWithString:checkOperationsService] mode:AGSFeatureLayerModeOnDemand];
    [checkOperationsFeatureLayer setDelegate:self];
    [checkOperationsFeatureLayer setDefinitionExpression:layerDef2.definition];
    [checkOperationsFeatureLayer setOutFields:@[@"*"]];
    [mapView insertMapLayer:checkOperationsFeatureLayer withName:@"Check Operations Layer" atIndex:3];

    queryTask = [[AGSQueryTask alloc] initWithURL:[NSURL URLWithString:officesService]];
    [queryTask setDelegate:self];
    
    query = [AGSQuery query];
    [query setWhereClause:defString];
    [query setOutFields:@[@"*"]];
    [query setReturnGeometry:YES];
    
    [queryTask executeWithQuery:query];
}

#pragma mark - AGSLayerDelegate
- (void) layerDidLoad: (AGSLayer*) layer{
    NSLog(@"%s %@", __PRETTY_FUNCTION__, layer.name);
}

- (void) layer : (AGSLayer*) layer didFailToLoadwithError:(NSError*) error {
    NSLog(@"%s Error: %@", __PRETTY_FUNCTION__, error);
}

#pragma mark - AGSCalloutDelegate
-(BOOL)callout:(AGSCallout *)callout willShowForFeature:(id<AGSFeature>)feature layer:(AGSLayer<AGSHitTestable> *)layer mapPoint:(AGSPoint *)mapPoint {
    
    AGSGraphic *graphic = (AGSGraphic *)feature;
    NSDictionary *attributes = graphic.allAttributes;
    
    //Specify the callout's contents
    if ([attributes objectForKey:@"TENT_ID"]) {
        tentId = (NSString *)[attributes objectForKey:@"TENT_ID"];
        NSString *zoneName = (NSString *)[attributes objectForKey:@"Zone_Name"];
        NSString *category = (NSString *) [attributes objectForKey:@"CATEGORY"];
        
        CustomCalloutViewController *customCallout = [[CustomCalloutViewController alloc]init];
        customCallout.calloutType = @"TCS";
        customCallout.txt1= tentId;
        customCallout.txt2 = zoneName;
        customCallout.txt3 = category;
        mapView.callout.customView = customCallout.view;
    }
    else {
        return NO;
    }
    if ([attributes objectForKey:@"X"]) {
        XCoordinate = [[attributes objectForKey:@"X"]doubleValue];
    }
    if ([attributes objectForKey:@"Y"]) {
        YCoordinate = [[attributes objectForKey:@"Y"]doubleValue];
    }
    NSLog(@"tent id %@, count%lu",tentId,(unsigned long)tentId.length);
    return YES;
}

#pragma mark - AGSQueryTask
- (void)queryTask:(AGSQueryTask *)queryTask operation:(NSOperation *)op didExecuteWithFeatureSetResult:(AGSFeatureSet *)featureSet {

    AGSGraphic *graphic = featureSet.features[0];
//    NSDictionary *attributes = graphic.allAttributes;

    if (![[UserDefaults objectForKey:@"officeNumber"] isEqual:@"-1"]) {
        
        [mapView zoomToGeometry:graphic.geometry withPadding:3.0f animated:YES];
    }
}

-(void)queryTask:(AGSQueryTask *)queryTask operation:(NSOperation *)op didFailWithError:(NSError *)error {
    
    NSLog(@"%s Error: %@", __PRETTY_FUNCTION__, error);
}

- (void) doneButtonPressed {
    
    if(tentId)
        tentId = [tentId stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (![Utility isValueNilNullOrEmpty:tentId])
    {
        NSArray *featureDetails = [[NSArray alloc]initWithObjects:tentId, [NSDecimalNumber numberWithDouble:XCoordinate], [NSDecimalNumber numberWithDouble:YCoordinate], nil];
        DispatchEvent(@"returnTentId", featureDetails);
        //[[NSNotificationCenter defaultCenter] postNotificationName:@"returnTentId" object:featureDetails];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        [Utility showAlertViewWithTitle:LocalizedString(@"error")  message:LocalizedString(@"mapTentError") cancelButtonTile:LocalizedString(@"ok")];
    }
}

-(NSString *) createUsersQuery {
    NSArray *allUsers = [UserDefaults objectForKey:@"userIDs"];
    
    NSMutableString *whereQuery = [[NSMutableString alloc]init];
    
    if ([[UserDefaults objectForKey:@"officeNumber"] isEqual:@"-1"]) {
        [whereQuery appendString:@"1=1"];
    }
    else {
        for (int i = 1; i <= [allUsers count]; i++) {
            if (i == 1) {
                [whereQuery appendFormat:@"(user_id = '%@' or ",[allUsers objectAtIndex:i-1]];
            }
            else if (i != [allUsers count]) {
                [whereQuery appendFormat:@"user_id = '%@' or ",[allUsers objectAtIndex:i-1]];
            }
            else {
                [whereQuery appendFormat:@"user_id = '%@') and status!=3",[allUsers objectAtIndex:i-1]];
                
            }
        }
    }

    return whereQuery;
}

-(void) zoomToFullExtent: (NSNotification *) sender {
    AGSLayer *layer = sender.object;
    
    if ([[UserDefaults objectForKey:@"officeNumber"] isEqual:@"-1"] && [layer.name isEqualToString:@"Office Layer"]) {
        fullExtent = (AGSEnvelope *)[layer fullEnvelope];
        [mapView zoomToEnvelope:fullExtent animated:YES];
        
        [[NSNotificationCenter defaultCenter]removeObserver:self name:AGSFeatureLayerDidLoadFeaturesNotification object:nil];
    }
    
    
}

@end
