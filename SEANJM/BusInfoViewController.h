//
//  BusInfoViewController.h
//  SEANJM
//
//  Created by Abdul Kareem on 9/19/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BusInfoViewController : UIViewController
-(id)initWithBusDetail :(NSDictionary *)bus_info;
@end
