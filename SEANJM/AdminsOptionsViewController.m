//
//  AdminsOptionsViewController.m
//  SEANJM
//
//  Created by Abdul Kareem on 9/18/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "AdminsOptionsViewController.h"
#import "Utility.h"
#import "Macros.h"
#import "AdminsRoutesViewController.h"
#import "ActionSheetStringPicker.h"
#import "LanguageController.h"
#import "ConfigurationManager.h"
#import "MBProgressHUD.h"
#import "BTSData.h"
#import "GeneralReportViewController.h"
#import "RoutesViewController.h"
#import "UserInformation.h"

@interface AdminsOptionsViewController ()
{
    __weak IBOutlet UIView *superAdminOptions;
    __weak IBOutlet UIView *AdminOptions;
    __weak IBOutlet UIImageView *imgLogo;
    ConfigurationManager *config;
}
@property (nonatomic)MBProgressHUD *hudview;
@property (nonatomic)BOOL isSuperAdmin;
@end

@implementation AdminsOptionsViewController

-(id)initWithAuthorityIsSuperAdmin :(BOOL)is_SAdmin
{
    self =[super init];
    _isSuperAdmin = is_SAdmin;
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    config =[[ConfigurationManager alloc]init];
}

-(void)viewDidAppear:(BOOL)animated
{
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    
    [Utility setNavigationBarProperties:self.navigationController];
    self.navigationController.navigationBar.topItem.title = @"قائمة الخيارات";
    self.navigationController.navigationBar.backItem.title =LocalizedString(@"back");
    [self loadViewEitherForSuperAdminOrAdmin];
}

-(void)loadViewEitherForSuperAdminOrAdmin
{
    if(_isSuperAdmin)
    {
        [self.view addSubview:superAdminOptions];
        [superAdminOptions setBackgroundColor:[UIColor clearColor]];
        [superAdminOptions setCenter:self.view.center];

    }
    else
    {
        [self.view addSubview:AdminOptions];
        [AdminOptions setBackgroundColor:[UIColor clearColor]];
        [AdminOptions setCenter:self.view.center];
    }
}

-(IBAction)superAdminOptionsTapped:(UIButton *)sender
{
    if(sender.tag ==0)
    {
        AdminsRoutesViewController *objRoutes =[[AdminsRoutesViewController alloc]initWithOption:0];
        [self.navigationController pushViewController:objRoutes animated:YES];
    }
    else if(sender.tag ==1)
    {
        AdminsRoutesViewController *objRoutes =[[AdminsRoutesViewController alloc]initWithOption:1];
        [self.navigationController pushViewController:objRoutes animated:YES];
    }
    else
    {
        GeneralReportViewController *objGeneral =[[GeneralReportViewController alloc]init];
        [self.navigationController pushViewController:objGeneral animated:YES];
    }
}

-(IBAction)AdminOptionsTapped:(UIButton *)sender
{
    if(sender.tag ==0)
    {
        [UserDefaults setBool:NO forKey:kisManagerInReports];
        [UserDefaults synchronize];
    }

    else
    {
        [UserDefaults setBool:YES forKey:kisManagerInReports];
        [UserDefaults synchronize];
    }
    
    RoutesViewController *objRoute =[[RoutesViewController alloc]initWithRole:[UserInformation getUserSelectedRole]];
    [self.navigationController pushViewController:objRoute animated:YES];
}

@end
