//
//  Language.h
//  ACTVET
//
//  Created by Burhanuddin Sunelwala on 12/27/14.
//  Copyright (c) 2014 adec. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define SharedLanguage          [LanguageController sharedController]
#define LocalizedString(k)      [SharedLanguage localizedStringForKey:k]
#define LocalizedNumber(n,d)    [SharedLanguage localizedNumberForNumber:n decimalDigits:d]

typedef NS_ENUM(int, Language){
 
    English,
    Arabic
};

@interface LanguageController : NSObject

@property (nonatomic,assign) Language language;

+ (instancetype)sharedController;
- (NSString *)localizedStringForKey:(NSString *)key;
- (NSString *)localizedNumberForNumber:(NSNumber *)number decimalDigits:(NSUInteger)digits;
- (NSString *)localizedKey:(NSString *)key;
- (UIImage *)localizedImageNamed:(NSString *)imageName;

@end
