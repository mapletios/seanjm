//
//  PublicFirstFormViewController.m
//  SEANJM
//
//  Created by Randa Al-Sadek on 6/20/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "PublicFirstFormViewController.h"
#import "ActionSheetStringPicker.h"
#import "MapCommViewController.h"
#import "ConfigurationManager.h"
#import "Utility.h"
#import "LanguageController.h"
#import "Macros.h"
#import "PNTToolbar.h"

@interface PublicFirstFormViewController ()<UITextFieldDelegate, UITextViewDelegate,UITextViewDelegate> {
   
    __weak IBOutlet UILabel *pointLatLbl;
    __weak IBOutlet UILabel *pointLonLbl;
    __weak IBOutlet UITextField *pointLatTxt;
    __weak IBOutlet UITextField *pointLonTxt;
    __weak IBOutlet UIScrollView *scrollView1;
    
    __weak IBOutlet UILabel *areaLbl;
    __weak IBOutlet UITextField *areaTxt;
    __weak IBOutlet UILabel *pilgrimLbl;
    __weak IBOutlet UITextField *pilgrimIDTxt;
    __weak IBOutlet UILabel *permissionLbl;
    __weak IBOutlet UIButton *permissionCheck;

    __weak IBOutlet UILabel *permissionNumLbl;
    __weak IBOutlet UITextField *permissionNumTxt;
    __weak IBOutlet UILabel *passportLbl;
    __weak IBOutlet UIButton *passportCheck;

    __weak IBOutlet UILabel *passportDataLbl;
    __weak IBOutlet UIButton *passportDataCheck;

    __weak IBOutlet UILabel *ticketNumLbl;
    __weak IBOutlet UITextField *ticketNumTxt;
    __weak IBOutlet UILabel *stampLbl;
    __weak IBOutlet UIButton *stampCheck;

    __weak IBOutlet UILabel *processLbl;
    __weak IBOutlet UIButton *processCheck;

    __weak IBOutlet UILabel *processNumLbl;
    __weak IBOutlet UITextField *processNumTxt;
    __weak IBOutlet UILabel *passportAvailLbl;
    __weak IBOutlet UIButton *passportAvailCheck;

    __weak IBOutlet UILabel *commentsLbl;
    __weak IBOutlet UITextView *commentsTxtView;
    __weak IBOutlet UIButton *sendButton;
    __weak IBOutlet UIButton *mapButton;
    __weak IBOutlet UIActivityIndicatorView *activityIndicator;
    
    ConfigurationManager *config;
    NSMutableArray *mapFeature;
    NSNumber *isPermit;
    NSNumber *isStamped;
    NSNumber *isPassportCopies;
    NSNumber *isTicketExisted;
    NSNumber *isDelegated;
    NSNumber *isOrginalPassport;
    
    NSMutableArray  *inputAccessoryArray;
}

@end

@implementation PublicFirstFormViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.navigationItem setTitle:LocalizedString(@"deathComplain")];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    tap.cancelsTouchesInView = NO;
    
    [self.view addGestureRecognizer:tap];
   
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(returnMapPoint:) name:@"ReturnMapPoint" object:nil];
    
    [Utility setFormsWithPointLat:pointLatLbl pointLon:pointLonLbl mapButton:mapButton sendButton:sendButton username:nil userPhone:nil];

    [Utility setTextViewBorder:commentsTxtView];
    
    config = [[ConfigurationManager alloc]init];
    
    [Utility setScrollViewSize:scrollView1];
    
    isPermit = @0;
    isStamped = @0;
    isPassportCopies = @0;
    isTicketExisted = @0;
    isDelegated = @0;
    isOrginalPassport = @0;
    
    [Utility setWhiteShadowToAllLabels:scrollView1];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

    //ToolBar
    inputAccessoryArray = [NSMutableArray array];
    for (id subview in scrollView1.subviews) {
        if([subview isKindOfClass:[UITextField class]] || [subview isKindOfClass:[UITextView class]])
        {
            [inputAccessoryArray addObject:subview];
        }
    }
    
    PNTToolbar *toolbar = [PNTToolbar defaultToolbar];
    toolbar.navigationButtonsTintColor = [UIColor blueColor];
    toolbar.mainScrollView = scrollView1;
    toolbar.inputFields = inputAccessoryArray;
    
    
}
-(void)viewDidDisappear:(BOOL)animated
{
    if(![self.navigationController.viewControllers containsObject:self])
        DispatchEvent(kStopLocation, nil);
    
}

-(void)viewWillAppear:(BOOL)animated
{
    DispatchEvent(kStartLocation, nil);
    [Utility applyBlueRoundedButton:sendButton];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma mark - Keyboard Delegate


-(void)keyboardWillHide:(NSNotification *)notification
{
    [scrollView1 setContentOffset:CGPointZero animated:YES];
}


#pragma UITextFieldDelegate

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if(textField == pilgrimIDTxt)
    {
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        
        return newLength <= 10 || returnKey;
    }
    return YES;
}


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField == areaTxt) {
        [self.view endEditing:YES];
        
        NSArray *areas = [[NSArray alloc]initWithObjects:LocalizedString(@"mecca"), LocalizedString(@"mina"), LocalizedString(@"arafat"), LocalizedString(@"muzdalefa"), nil];
        
        [ActionSheetStringPicker showPickerWithTitle:@"" rows:areas initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
            
            textField.text = selectedValue;
            
        } cancelBlock:nil origin:textField];

    }
    else {
        
        [scrollView1 setContentOffset:CGPointMake(0, CGRectGetMinY(textField.frame)-5) animated:YES];
        return YES;
    }
    return NO;
}


-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [scrollView1 setContentOffset:CGPointMake(0, CGRectGetMinY(textView.frame)-5) animated:YES];
}

//-(void)textFieldDidEndEditing:(UITextField *)textField
//{
//    if(textField  == pilgrimIDTxt)
//    {
//        pilgrimIDTxt.text = [pilgrimIDTxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//        if(((pilgrimIDTxt.text.length < 10) && (pilgrimIDTxt.text.length >0)) || ([pilgrimIDTxt.text isEqualToString:@""]))
//            [self showPilgrimIdAlert];
//    }
//}

-(void)showPilgrimIdAlert
{
    [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"pilgrimIDError") cancelButtonTile:LocalizedString(@"ok")];
}

- (IBAction)sendButtonPressed:(id)sender {
    
    if(![Utility checkInternetConnection])
        return;
    
    else if(![Utility isCurrentLocationAvailable])
    {
        [Utility showAlertViewWithTitle:LocalizedString(@"locationservice") message:LocalizedString(@"checklocationservice") cancelButtonTile:LocalizedString(@"ok")];
        return;
    }
    
    pilgrimIDTxt.text = [pilgrimIDTxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if(((pilgrimIDTxt.text.length < 10) && (pilgrimIDTxt.text.length >0)) || ([pilgrimIDTxt.text isEqual:nil]))
    {
        [self showPilgrimIdAlert];
        return;
    }
    else if ([areaTxt.text length] == 0 || [permissionNumTxt.text length] == 0 || [processNumTxt.text length] == 0) {
        [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"emptyFields") cancelButtonTile:LocalizedString(@"ok")];
    }
    else if ([pointLatTxt.text length] == 0 || [pointLonTxt.text length] == 0) {
        [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"emptyMapPoint") cancelButtonTile:LocalizedString(@"ok")];
    }
    else {
        NSArray *data = [[NSArray alloc]initWithObjects:@3, areaTxt.text, permissionNumTxt.text, processNumTxt.text, isPermit, isStamped, isPassportCopies, isTicketExisted, isDelegated, isOrginalPassport, pointLatTxt.text, pointLonTxt.text, nil];
        
        NSArray *dataKeys = [[NSArray alloc]initWithObjects:@"mainTypeId", @"address", @"permitID", @"delegateID", @"isPermit", @"isStamped", @"isPassportCopies", @"isTicketExisted", @"isDelegated", @"isOriginalPassport", @"xPosition", @"yPosition", nil];
        
        NSMutableDictionary *complaintDic = [[NSMutableDictionary alloc]initWithObjects:data forKeys:dataKeys];
        
        
        if ([ticketNumTxt.text length] > 0)
            [complaintDic setObject:ticketNumTxt.text forKey:@"ticketID"];
        if ([commentsTxtView.text length] > 0)
            [complaintDic setObject:commentsTxtView.text forKey:@"comment"];
        if ([pilgrimIDTxt.text length] > 0)
            [complaintDic setObject:pilgrimIDTxt.text forKey:@"pilgrimID"];
        else
            [complaintDic setObject:@"0" forKey:@"pilgrimID"];
        
        [activityIndicator setHidden:NO];
        [activityIndicator startAnimating];
        
        [config sendDeathComplaint:complaintDic completion:^{
            [self resetView];
            [Utility showAlertViewWithTitle:LocalizedString(@"success") message:LocalizedString(@"successMessage") cancelButtonTile:LocalizedString(@"ok")];
            [activityIndicator stopAnimating];
            [self.navigationController popViewControllerAnimated:YES];

        }failure:^{
            [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"reportNotSent") cancelButtonTile:LocalizedString(@"ok")];
            [activityIndicator stopAnimating];
        }];
    }
}

- (IBAction)permissionCheckPressed:(id)sender {
    if ([isPermit isEqualToNumber:@0]) {
        isPermit = [Utility setButtonChecked:isPermit button:(UIButton *)sender];
    }
    else {
        isPermit = [Utility setButtonUnChecked:isPermit button:(UIButton *)sender];
    }
}

- (IBAction)passportCheckPressed:(id)sender {
    if ([isPassportCopies isEqualToNumber:@0]) {
        isPassportCopies = [Utility setButtonChecked:isPassportCopies button:(UIButton *)sender];
    }
    else {
        isPassportCopies = [Utility setButtonUnChecked:isPassportCopies button:(UIButton *)sender];
    }
}

- (IBAction)passportDataCheckPressed:(id)sender {
    if ([isTicketExisted isEqualToNumber:@0]) {
        isTicketExisted = [Utility setButtonChecked:isTicketExisted button:(UIButton *)sender];
    }
    else {
        isTicketExisted = [Utility setButtonUnChecked:isTicketExisted button:(UIButton *)sender];
    }
}

- (IBAction)stampCheckPressed:(id)sender {
    if ([isStamped isEqualToNumber:@0]) {
        isStamped = [Utility setButtonChecked:isStamped button:(UIButton *)sender];
    }
    else {
        isStamped = [Utility setButtonUnChecked:isStamped button:(UIButton *)sender];
    }
}

- (IBAction)processCheckPressed:(id)sender {
    if ([isDelegated isEqualToNumber:@0]) {
        isDelegated = [Utility setButtonChecked:isDelegated button:(UIButton *)sender];
    }
    else {
        isDelegated = [Utility setButtonUnChecked:isDelegated button:(UIButton *)sender];
    }
}

- (IBAction)passportAvailCheckPressed:(id)sender {
    if ([isOrginalPassport isEqualToNumber:@0]) {
        isOrginalPassport = [Utility setButtonChecked:isOrginalPassport button:(UIButton *)sender];
    }
    else {
        isOrginalPassport = [Utility setButtonUnChecked:isOrginalPassport button:(UIButton *)sender];
    }
}

- (IBAction)mapButtonPressed:(id)sender {
    
    if(![Utility checkInternetConnection])
        return;
    
    MapCommViewController *mapView = [[MapCommViewController alloc]init];
    mapView.mapType = 2;
    mapView.showReport = NO;
    [self.navigationController pushViewController:mapView animated:YES];
}

- (void)returnMapPoint:(NSNotification *)notification {
    mapFeature = notification.object;
    pointLatTxt.text = [mapFeature objectAtIndex:0];
    pointLonTxt.text = [mapFeature objectAtIndex:1];
}

- (void)resetView {
    isPermit = @1;
    isStamped = @1;
    isPassportCopies = @1;
    isTicketExisted = @1;
    isDelegated = @1;
    isOrginalPassport = @1;
    
    [self permissionCheckPressed:nil];
    [self passportCheckPressed:nil];
    [self passportDataCheckPressed:nil];
    [self passportAvailCheckPressed:nil];
    [self stampCheckPressed:nil];
    [self processCheckPressed:nil];
    
    areaTxt.text = @"";
    pilgrimIDTxt.text = @"";
    permissionNumTxt.text = @"";
    ticketNumTxt.text = @"";
    processNumTxt.text = @"";
    commentsTxtView.text = @"";
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

@end
