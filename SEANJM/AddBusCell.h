//
//  AddBusCellTableViewCell.h
//  SEANJM
//
//  Created by Abdul Kareem on 9/10/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddBusCell: UITableViewCell
@property (nonatomic,weak)IBOutlet UILabel *lblInfo;
@property (nonatomic,weak)IBOutlet UITextField *txtUserDetail;
@end
