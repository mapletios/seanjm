//
//  AddUsersViewController.h
//  SEANJM
//
//  Created by Abdul Kareem on 9/5/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddUsersViewController : UIViewController
-(id)initWithSelectedTask :(NSDictionary *)taskdictionary;
-(id)initWithUserArray :(NSMutableArray *)user_array;
@end
