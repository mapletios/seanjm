//
//  GeneralReportViewController.m
//  SEANJM
//
//  Created by Abdul Kareem on 9/19/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "GeneralReportViewController.h"
#import "MBProgressHUD.h"
#import "Macros.h"
#import "Utility.h"
#import "AFNetworking.h"

#define BTSgetGeneralValues @"http://213.236.53.172/snbts/restserviceimpl.svc/getGeneralValues"
@interface GeneralReportViewController ()
{
    __weak IBOutlet UILabel *lblNoOFPilgrims;
    __weak IBOutlet UILabel *lblNoOFGroups;
}
@property (nonatomic)MBProgressHUD *hudview;
@property (nonatomic)NSDictionary *infoDictionary;
@end

@implementation GeneralReportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self performSelector:@selector(LoadGeneralValues) withObject:nil afterDelay:0.0005];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];

    self.navigationController.navigationBar.topItem.title = @"قائمة التقارير";
    [lblNoOFGroups.layer setCornerRadius:70.0];
    [lblNoOFPilgrims.layer setCornerRadius:70.0];
    [lblNoOFGroups setClipsToBounds:YES];
    [lblNoOFPilgrims setClipsToBounds:YES];
}

-(void)LoadGeneralValues
{
    if(![Utility checkInternetConnection])
        return;
    
    if(!_hudview)
    {
    _hudview = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _hudview.mode = MBProgressHUDModeIndeterminate;
    _hudview.labelText = @"Loading...";
    [_hudview setDimBackground:YES];
    }
    NSString *serviceURl =[NSString stringWithFormat:@"%@",BTSgetGeneralValues];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager GET:serviceURl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //NSLog(@"Group INfo List %@", responseObject);
        if(![responseObject  isKindOfClass:[NSNull class]])
        {
            if([responseObject objectForKey:@"Result"])
            {
                _infoDictionary = [NSDictionary dictionaryWithDictionary:[responseObject objectForKey:@"Result"]];
                
                [lblNoOFGroups setText:[[_infoDictionary objectForKey:@"groupsTotalCount"] stringValue]];
                [lblNoOFPilgrims setText:[[_infoDictionary objectForKey:@"pilgrimsTotalCount"] stringValue]];
            }
            
        }
        [_hudview setHidden:YES];
        _hudview = nil;
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"BTS Error: %@", error);
        [_hudview setHidden:YES];
        _hudview = nil;
    }];
}

@end
