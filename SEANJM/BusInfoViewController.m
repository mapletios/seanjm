//
//  BusInfoViewController.m
//  SEANJM
//
//  Created by Abdul Kareem on 9/19/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "BusInfoViewController.h"
#import "Macros.h"
#import "Utility.h"
#import "MBProgressHUD.h"
#import "Utility.h"
#import "Macros.h"
#import "BTSData.h"
#import "LanguageController.h"
#import "AFNetworking.h"
#import "UserInformation.h"
#import "TwoTitleCell.h"
#import "HeaderView.h"

@interface BusInfoViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic)NSDictionary *busInfoDic;
@property (nonatomic)NSArray *keysArray;
@property (nonatomic)IBOutlet UITableView *infoTableView;
@property (nonatomic)CGFloat headerHeight;
@property (nonatomic)MBProgressHUD *hudview;
@property (nonatomic)NSString *infoCellIdentifier;

@end

@implementation BusInfoViewController

-(id)initWithBusDetail :(NSDictionary *)bus_info
{
    self = [super init];
    _busInfoDic = bus_info;
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    _headerHeight = 45.0;
    _keysArray = [NSArray arrayWithArray:[_busInfoDic allKeys]];

    //[Utility LoadVisualEffectView:self.view];
    
    _infoCellIdentifier = @"TwoTitleCell";
    
    
    //[self createTableView];
    
    [_infoTableView registerNib:[UINib nibWithNibName:_infoCellIdentifier bundle:[NSBundle mainBundle]]forCellReuseIdentifier:_infoCellIdentifier];
    [_infoTableView registerNib:[UINib nibWithNibName:@"HeaderView" bundle:[NSBundle mainBundle]]forCellReuseIdentifier:@"HeaderCell"];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.topItem.title = LocalizedString(@"BusDetail");
    self.navigationController.navigationBar.backItem.title = LocalizedString(@"back");
}


-(IBAction)infoRemove:(id)sender
{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"BusInfo_close"object:self];
}

#pragma mark -TableView Initialization


#pragma mark -UITABLEVIEW DATASOURCE
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    HeaderView *headerView = [tableView dequeueReusableCellWithIdentifier:@"HeaderCell"];
    [headerView setTitle:[NSString stringWithFormat:@"%@ : %@",LocalizedString(@"plateNumber"),[_busInfoDic objectForKey:@"plateNumber"]]];
    
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _busInfoDic.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TwoTitleCell *userinfoCell =[tableView dequeueReusableCellWithIdentifier:_infoCellIdentifier forIndexPath:indexPath];
    
    
    if([[_busInfoDic objectForKey:[_keysArray objectAtIndex:indexPath.row]] isKindOfClass:[NSString class]])
    {
    [userinfoCell.lblLeftTitle setText:[NSString stringWithFormat:@"%@",
                                        [_busInfoDic objectForKey:[_keysArray objectAtIndex:indexPath.row]]]];
    }
    else
    {
        [userinfoCell.lblLeftTitle setText:[NSString stringWithFormat:@"%d",
                                            [[_busInfoDic objectForKey:[_keysArray objectAtIndex:indexPath.row]] intValue]]];
    }
    
    [userinfoCell.lblRightTitle setText:[NSString stringWithFormat:@"%@ : ",LocalizedString([_keysArray objectAtIndex:indexPath.row])]];
    
    return userinfoCell;
}

@end
