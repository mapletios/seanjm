//
//  FaultsTableViewController.m
//  SEANJM
//
//  Created by Randa Al-Sadek on 5/20/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "FaultsTableViewController.h"
#import "AddReportFormViewController.h"
#import "ConfigurationManager.h"
#import "MapViewController.h"
#import "Macros.h"
#import "LanguageController.h"
#import "Utility.h"
#import <ArcGIS/ArcGIS.h>

#define tentsService @"http://213.236.53.172/arcgis/rest/services/NJM/MinaTentsService/MapServer/0"

@interface FaultsTableViewController ()<UITextFieldDelegate, UIAlertViewDelegate, AGSQueryTaskDelegate> {
    
    __weak IBOutlet UILabel *tentNumberLbl;
    __weak IBOutlet UILabel *costLbl;
    __weak IBOutlet UITableView *faultsTableView;
    __weak IBOutlet UITextField *tentNumTxt;
    __weak IBOutlet UITextField *costTxt;
    __weak IBOutlet UIButton *mapButton;
    __weak IBOutlet UIButton *searchButton;
    __weak IBOutlet UIBarButtonItem *clearButton;
    __weak IBOutlet UIBarButtonItem *doneButton;
    __weak IBOutlet UIProgressView *progressView;
    __weak IBOutlet UILabel *progressLabel;
    __weak IBOutlet UIActivityIndicatorView *activityIndicator;
    __weak IBOutlet UIButton *btnSearch;
    
    
    NSInteger selectedIndex;
    NSArray *oldFaults;
    NSMutableArray *mapFeature;
    ConfigurationManager *config;
    
    NSDecimalNumber *XCoordinate;
    NSDecimalNumber *YCoordinate;
    int operationExist;
    int changesDone;        //To check if changes were done to reports
    
    AGSQueryTask *queryTaskCheck;
    AGSQuery *queryCheck;
    
    BOOL isSearchButtonTapped;
    BOOL isTentValid;
}
@property (nonatomic,retain)NSString *office_id;
@end

@implementation FaultsTableViewController

@synthesize allFaults;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    tentNumberLbl.text = LocalizedString(@"tentNumber");
    costLbl.text = LocalizedString(@"totalCost");
//    mapButton.titleLabel.text = LocalizedString(@"map");
    searchButton.titleLabel.text = LocalizedString(@"search");
    clearButton.title = LocalizedString(@"clearAll");
    doneButton.title = LocalizedString(@"send");
    
    _office_id = [UserDefaults objectForKey:@"officeNumber"];
    _office_id = [_office_id stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    config = [[ConfigurationManager alloc]init];
    
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc]
                                  initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addFaultButtonPressed)];
    self.navigationItem.rightBarButtonItem = addButton;
//    [self.navigationItem.rightBarButtonItem setEnabled:NO];
    
    [self.navigationItem setTitle: LocalizedString(@"reports")];
    
    allFaults  = [[NSMutableArray alloc]init];
    mapFeature = [[NSMutableArray alloc]init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addToFaults:) name:@"AddToFaults" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveToFaults:) name:@"SaveToFaults" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(returnTentId:) name:@"returnTentId" object:nil];
    
    faultsTableView.allowsMultipleSelectionDuringEditing = NO;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    tap.cancelsTouchesInView = NO;
    
    [self.view addGestureRecognizer:tap];
    
    costTxt.text = @"0";
    changesDone = 0;
    isTentValid = NO;
    
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    //[super viewWillDisappear:animated];
    if(![self.navigationController.viewControllers containsObject:self])
    {
        DispatchEvent(kStopLocation, nil);

        [[NSNotificationCenter defaultCenter]removeObserver:self name:@"returnTentId" object:nil];
        [[NSNotificationCenter defaultCenter]removeObserver:self name:@"SaveToFaults" object:nil];
        [[NSNotificationCenter defaultCenter]removeObserver:self name:@"AddToFaults" object:nil];
        [allFaults removeAllObjects];
    }
    
}

-(void)viewWillAppear:(BOOL)animated {

    DispatchEvent(kStartLocation, nil);
    [faultsTableView reloadData];
    [self updateProgress];
    [Utility applyBlueRoundedButton:btnSearch];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView Datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [allFaults count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellDetail"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"CellDetail"];
    }
    id fault = [[allFaults objectAtIndex:indexPath.row] valueForKey:@"deleted"];
    
    if (![fault isEqual:@1]) {
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell setUserInteractionEnabled:YES];
        [cell setTag:0];
    }
    else {
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    
    NSDictionary *currentFault = [allFaults objectAtIndex:indexPath.row];
    [cell.textLabel setTextAlignment:NSTextAlignmentRight];
    [cell.detailTextLabel setTextAlignment:NSTextAlignmentRight];
    [cell.textLabel setText:[currentFault objectForKey:@"name"]];
    [cell.detailTextLabel setText:[currentFault objectForKey:@"type"]];

    UIImageView *imv = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_SIZE.width-50, 12, 20, 20)];

    if ([[currentFault objectForKey:@"status"]integerValue] == 0) {
        imv.image=[UIImage imageNamed:@"status-red.png"];
    }
    else if ([[currentFault objectForKey:@"status"]integerValue] == 1){
        imv.image=[UIImage imageNamed:@"status-green.png"];
    }
    else {
        imv.image=[UIImage imageNamed:@"status-blue.png"];
    }
   
    [cell addSubview:imv];

    return cell;
}

#pragma mark - TableView Delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedIndex = indexPath.row;
    
    NSDictionary *selectedFault = [allFaults objectAtIndex:selectedIndex];
    
    if (![[selectedFault valueForKey:@"deleted"] isEqual:@1]) {
        AddReportFormViewController *view = [[AddReportFormViewController alloc]init];
        view.selectedFault = [[NSDictionary alloc]initWithDictionary:selectedFault];
        view.editMode = TRUE;
        
        [self.navigationController pushViewController:view animated:YES];
    }
    
}

#pragma mark - TextField Delegate
-(void)textFieldDidEndEditing:(UITextField *)textField {
    isTentValid = NO;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if (textField == tentNumTxt) {
        [textField resignFirstResponder];
        [self.view endEditing:NO];
        return YES;
    }
    
    return NO;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *loginUser = [UserDefaults objectForKey:@"loginUser"];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];

    if ([loginUser isEqualToString:[[allFaults objectAtIndex:indexPath.row]objectForKey:@"username"]] && cell.tag != 1) {
        return YES;
    }
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *selectedFault = [allFaults objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [cell setBackgroundColor:[UIColor redColor]];
        [cell setUserInteractionEnabled:NO];
        [cell setTag:1];
        
        NSString *date = [config getCurrentDate];
        
        [selectedFault setValue:@1 forKey:@"deleted"];
        [selectedFault setValue:date forKey:@"reportDetailsDate_d"];
        [selectedFault setValue:[UserDefaults objectForKey:@"loginUser"] forKey:@"username_d"];
        [selectedFault setValue:@2 forKey:@"flag"];
        [selectedFault setValue:@3 forKey:@"status"];
        
        [faultsTableView reloadData];
        [self calculateCost];
        [self updateProgress];
        
        changesDone = 1;
        
    }
    else
        [cell setBackgroundColor:[UIColor clearColor]];
}

#pragma mark - UIAlertViewDelegate 
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {

    if (buttonIndex && alertView.tag == 1) {
    
        if (mapFeature.count > 0) {
            XCoordinate = [mapFeature objectAtIndex:1];
            YCoordinate = [mapFeature objectAtIndex:2];
        }
        if (changesDone == 1) {
            [activityIndicator setHidden:NO];
            [activityIndicator startAnimating];
            
            NSArray *newArray = [[NSArray alloc]initWithObjects:allFaults, tentNumTxt.text, XCoordinate, YCoordinate, [NSNumber numberWithInt:operationExist], nil];
            
            [config sendAllFaults:newArray completion:^{
                [allFaults removeAllObjects];
                [faultsTableView reloadData];
                
                tentNumTxt.text = @"";
                costTxt.text = @"";
                [self updateProgress];
                
                [activityIndicator stopAnimating];
                
                [Utility showAlertViewWithTitle:LocalizedString(@"success") message:LocalizedString(@"successMessage") cancelButtonTile:LocalizedString(@"ok")];
            }failure:^{
                [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"sendFaultsError") cancelButtonTile:LocalizedString(@"ok")];

                [activityIndicator stopAnimating];
            }];
        }
        else {
            [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"noChanges") cancelButtonTile:LocalizedString(@"ok")];
        }
    }
}

#pragma AGSQueryTaskDelegate
-(void)queryTask:(AGSQueryTask *)queryTask operation:(NSOperation *)op didExecuteWithFeatureSetResult:(AGSFeatureSet *)featureSet {

    [activityIndicator stopAnimating];
    
    if ([featureSet.features count] > 0) {
        AGSGraphic *graphic = featureSet.features[0];
        NSDictionary *attributes = graphic.allAttributes;
    
        if (queryTask == queryTaskCheck) {
            if ([_office_id isEqualToString:@"-1"] || [[attributes objectForKey:@"OFFICE_ID"] isEqual:_office_id])
            {
                isTentValid = YES;
                
                if (!XCoordinate || !YCoordinate) {
                XCoordinate = [attributes objectForKey:@"X"];
                YCoordinate = [attributes objectForKey:@"Y"];
                }
                
                if (isSearchButtonTapped) {
                    [self checkOldFaults];
                }
                else {
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:LocalizedString(@"confirmation") message:LocalizedString(@"sendReport") delegate:self cancelButtonTitle:LocalizedString(@"cancel") otherButtonTitles:LocalizedString(@"yes"), nil];
                    alert.tag = 1;
                    [alert show];
                }
            }
            else  {
                [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"wrongTent") cancelButtonTile:LocalizedString(@"ok")];
                isTentValid = NO;
            }
        }
    }
    else
    {
        [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"wrongTent") cancelButtonTile:LocalizedString(@"ok")];
    }
}

-(void)queryTask:(AGSQueryTask *)queryTask operation:(NSOperation *)op didFailWithError:(NSError *)error {
    [activityIndicator stopAnimating];
    NSLog(@"Failed!");
    
}

-(void)checkTentIDisValid
{
    
    queryTaskCheck = [[AGSQueryTask alloc]initWithURL:[NSURL URLWithString:tentsService]];
    [queryTaskCheck setDelegate:self];
    
    queryCheck = [AGSQuery query];

    if([_office_id  isEqualToString:@"-1"])
        [queryCheck setWhereClause:[NSString stringWithFormat:@"TENT_ID='%@'",tentNumTxt.text]];
    else
        [queryCheck setWhereClause:[NSString stringWithFormat:@"TENT_ID='%@' and OFFICE_ID='%@'",tentNumTxt.text,_office_id]];

    [queryCheck setOutFields:@[@"*"]];
    
    [queryTaskCheck executeWithQuery:queryCheck];
    
    [activityIndicator startAnimating];
}

#pragma mark - Actions and Buttons
- (void) addFaultButtonPressed {
    if ([tentNumTxt.text length] > 0 && isTentValid) {
        AddReportFormViewController *view = [[AddReportFormViewController alloc]init];
        [self.navigationController pushViewController:view animated:YES];
    }
    else {
        [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"wrongTent") cancelButtonTile:LocalizedString(@"ok")];
    }
}

- (IBAction) doneButtonPressed:(id)sender {
    
    if(![Utility checkInternetConnection])
        return;
    else if(![Utility isCurrentLocationAvailable])
    {
        [Utility showAlertViewWithTitle:LocalizedString(@"locationservice") message:LocalizedString(@"checklocationservice") cancelButtonTile:LocalizedString(@"ok")];
        return;
    }
    
    //Else
    NSString *tent = [tentNumTxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

    if (allFaults.count > 0) {
        
        isSearchButtonTapped = NO;
        [self checkTentIDisValid];

    }
    else {
        [Utility showAlertViewWithTitle:LocalizedString(@"error")  message:LocalizedString(@"noFaults") cancelButtonTile:LocalizedString(@"ok")];
    }
}

- (IBAction)clearButtonPressed:(id)sender {
    if ([allFaults count] > 0) {
        [allFaults removeAllObjects];
        [faultsTableView reloadData];
    }
    costTxt.text = @"0";
    tentNumTxt.text = @"";
}

- (IBAction)mapButtonPressed:(id)sender {
    
    if(![Utility checkInternetConnection])
        return;
    
    MapViewController *mapView = [[MapViewController alloc]init];
    [self.navigationController pushViewController:mapView animated:YES];
}

- (IBAction)searchButtonPressed:(id)sender {
    
    if(![Utility checkInternetConnection])
        return;
    
    isSearchButtonTapped = YES;
    [self checkTentIDisValid];
}

- (void) returnTentId:(NSNotification *)notification {
    mapFeature = notification.object;
    tentNumTxt.text = [mapFeature objectAtIndex:0];
    isTentValid = YES;
    
//    if (allFaults.count > 0) {
//        [allFaults removeAllObjects];
//    }
    [allFaults removeAllObjects];
    [self checkOldFaults];
}

- (void) addToFaults:(NSNotification *)notification {
    NSDictionary *fault = notification.object;
    
    if (![self checkIfFaultExist:fault editMode:NO]) {
        [allFaults addObject:fault];
        
        [self calculateCost];
        [self updateProgress];
        
        changesDone = 1;
    }
}

- (void) saveToFaults:(NSNotification *)notification {
    NSDictionary *fault = notification.object;
    
    if (![self checkIfFaultExist:fault editMode:YES]) {
        [allFaults replaceObjectAtIndex:selectedIndex withObject:fault];
        
        [self calculateCost];
        [self updateProgress];
        
        changesDone = 1;
    }
}

-(BOOL)checkIfFaultExist: (NSDictionary *)newFault editMode:(BOOL)editMode{
    BOOL faultExists = NO;
    for (NSInteger i = 0; i < [allFaults count]; i++) {
        NSDictionary *fault = [allFaults objectAtIndex:i];
       
        if (([[fault objectForKey:@"categoryId"] isEqual:[newFault objectForKey:@"categoryId"]]) &&
            ([[fault objectForKey:@"subCategoryId"] isEqual:[newFault objectForKey:@"subCategoryId"]])) {
            
            if((editMode && selectedIndex != i) || !editMode) {
                faultExists = YES;
                break;
            }
        }
    }
    
    if (faultExists) {
        [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"duplicatedFault") cancelButtonTile:LocalizedString(@"ok")];
    }
    return faultExists;
}

- (void) calculateCost {
    int totalCost = 0;
    
    for (NSDictionary *fault in allFaults) {
        if (![[fault valueForKey:@"deleted"] isEqual: @1]) {
            int x = [[fault objectForKey:@"cost"]intValue];
            int y = [[fault objectForKey:@"count"]intValue];
            totalCost = totalCost + (x*y);

        }
    }
    
    costTxt.text = [NSString stringWithFormat:@"%.01f", (double)totalCost];
}

- (void) checkOldFaults {
    //Check from service if tent is available
    [activityIndicator setHidden:NO];
    [activityIndicator startAnimating];
    
    [config loadFaultsForTent:tentNumTxt.text completion:^ {
        oldFaults = [config getOldFaults];
        
        if (oldFaults.count > 0) {
            [self loadOldFaultsToTable];
            operationExist = 1;

        }
        else {
            [faultsTableView reloadData];
            costTxt.text = @"0";
    
            operationExist = 0;
        }
        [activityIndicator stopAnimating];
        
    }failure:^{
        [Utility showAlertViewWithTitle:LocalizedString(@"error")  message:LocalizedString(@"getTentError") cancelButtonTile:LocalizedString(@"ok")];

        [activityIndicator stopAnimating];
    }];
}

- (void) loadOldFaultsToTable {
    for (NSDictionary *oldFault in oldFaults) {
        
        //Status 3 is for deleted items in server but we dont have to show in app
        if([[oldFault objectForKey:@"status"] intValue] != 3)
        {
            NSDictionary *categoryObject = [config getCategoryObjectById:[oldFault objectForKey:@"categoryId"]];
            NSDictionary *subCategoryObject = [config getSubCategoryObjectById:[oldFault objectForKey:@"subCategoryId"]];
        
            NSMutableDictionary *fault = [[NSMutableDictionary alloc]initWithDictionary:oldFault];
            [fault setObject:[categoryObject objectForKey:@"name"] forKey:@"name"];
            [fault setObject:[subCategoryObject objectForKey:@"name"] forKey:@"type"];
            [fault setObject:[subCategoryObject objectForKey:@"cost"] forKey:@"cost"];
            [fault setValue:[NSNumber numberWithBool:YES] forKey:@"oldFault"];
            [allFaults addObject:fault];
            
        }
    }
    [faultsTableView reloadData];
    [self calculateCost];
    [self updateProgress];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

-(void)updateProgress {
    int count = 0;
    NSMutableArray *progressArray = [[NSMutableArray alloc]init];
    
    if ([allFaults count] > 0) {
        for (NSDictionary *fault in allFaults) {
            if ([[fault objectForKey:@"status"]integerValue] == 1 && ![[fault valueForKey:@"deleted"] isEqual: @1]) {
                count++;
            }
            if (![[fault valueForKey:@"deleted"] isEqual: @1]) {
                [progressArray addObject:fault];
            }
        }
        
        progressLabel.text = [NSString stringWithFormat:@"%.1f%%", ((float)count/[allFaults count]) * 100];
        progressView.progress = (float)count/[allFaults count];

    }
    else {
        progressLabel.text = @"0%";
        progressView.progress = 0;
    }
}

-(void)dealloc
{
    
}

@end
