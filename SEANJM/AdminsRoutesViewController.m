//
//  AdminsRoutesViewController.m
//  SEANJM
//
//  Created by Abdul Kareem on 9/18/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "AdminsRoutesViewController.h"
#import "Macros.h"
#import "LanguageController.h"
#import "HeaderView.h"
#import "CenterLabelCell.h"
#import "BTSData.h"
#import "UserInformation.h"
#import "MBProgressHUD.h"
#import "Utility.h"
#import "ConfigurationManager.h"
#import "SPReportsViewController.h"
#import "AFNetworking.h"

#define BTSConfigurationService @"http://213.236.53.172/snbts/restserviceimpl.svc/getBTSConfiguration"
#define BTSSAgetReportRouteGroupsAllRoutes @"http://213.236.53.172/snbts/restserviceimpl.svc/getReportRouteGroupsAllRoutes"

@interface AdminsRoutesViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    int numberOfBusRounds;
    NSInteger selectedRouteIndexForNumberOfRounds;
    ConfigurationManager *config;
}
@property (nonatomic,weak)IBOutlet UITableView *routeTableView;
@property (nonatomic)NSString *routeCellIdentifier;
@property (nonatomic)NSMutableArray *assignedRouteArray;
@property (nonatomic)NSMutableArray *routeArray;
@property (nonatomic)NSMutableArray *busesArray;
@property (nonatomic)NSDictionary *userSelectedRole;
@property (nonatomic)MBProgressHUD *hudview;
@property (nonatomic)NSString *managerServiceURL;
@property (nonatomic)NSString *coordinatorServiceURL;
@property (nonatomic)NSUInteger optionIndex;

@end

@implementation AdminsRoutesViewController

-(id)initWithOption :(NSUInteger)selectedIndex
{
    self = [super init];
    _optionIndex = selectedIndex;
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _assignedRouteArray = [NSMutableArray array];
    _routeArray = [NSMutableArray array];
    _busesArray = [NSMutableArray array];
    numberOfBusRounds = 1;
    config =[[ConfigurationManager alloc]init];
    [self performSelector:@selector(loadCOnfiguration) withObject:nil afterDelay:0.0005];

    
    [_routeTableView registerNib:[UINib nibWithNibName:@"HeaderView" bundle:[NSBundle mainBundle]]forCellReuseIdentifier:@"HeaderCell"];
    [_routeTableView registerNib:[UINib nibWithNibName:@"CenterLabelCell" bundle:[NSBundle mainBundle]]forCellReuseIdentifier:@"CenterLabelCell"];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    
    NSIndexPath *tableSelection = [_routeTableView indexPathForSelectedRow];
    [_routeTableView deselectRowAtIndexPath:tableSelection animated:YES];
    self.navigationController.navigationBar.backItem.title = LocalizedString(@"back");
    
    
    UIBarButtonItem *btnRefresh =[[UIBarButtonItem alloc] initWithTitle:LocalizedString(@"refresh") style:UIBarButtonItemStylePlain target:self action:@selector(loadCOnfiguration)];
    [self.navigationItem setRightBarButtonItem:btnRefresh];
}

-(void)loadCOnfiguration
{
    _routeArray = (NSMutableArray *)[BTSData getRoutes];
    //if(_routeArray.count !=8)
    {
    if(![Utility checkInternetConnection])
        return;
    
    if(!_hudview)
    {
        _hudview = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        _hudview.mode = MBProgressHUDModeIndeterminate;
        _hudview.labelText = @"Loading...";
        [_hudview setDimBackground:YES];
    }
 
   NSString *serviceURLBTS = [NSString stringWithFormat:@"%@/%@",BTSConfigurationService,[UserDefaults objectForKey:@"userGroupId"]];
    
        
        ////////////
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        
        [manager GET:serviceURLBTS parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            //NSLog(@"JSON: %@", responseObject);
            [self performSelector:@selector(loadSuperAdminReports) withObject:nil afterDelay:0.0005];
            
            [UserDefaults setObject:responseObject forKey:kBTSCONFIGURATION];
            [UserDefaults setBool:YES forKey:kBTSConfigLoaded];
            [UserDefaults synchronize];
            
            _routeArray = (NSMutableArray *)[BTSData getRoutes];
            [_hudview setHidden:YES];
            [_routeTableView reloadData];
            _hudview = nil;
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [Utility showAlertViewWithTitle:LocalizedString(@"error")  message:LocalizedString(@"error") cancelButtonTile:LocalizedString(@"ok")];
            [_hudview setHidden:YES];
            _hudview =nil;
            
        }];
        ///////////////
//    [config LoadBTSConfiguration:serviceURLBTS completion:^{
//
//        
//    } failure:^{
//        [_hudview setHidden:YES];
//    }];
    }
}

//Loading In background to use In Next Controller for Routes
-(void)loadSuperAdminReports
{
    if(![Utility checkInternetConnection])
        return;
        
        NSString *serviceURLBTS = [NSString stringWithFormat:@"%@",BTSSAgetReportRouteGroupsAllRoutes];
        [config LoadBTSSAReportsForAllRoutes:serviceURLBTS completion:^{
           
            
        } failure:^{
            
            [Utility showAlertViewWithTitle:LocalizedString(@"error")  message:LocalizedString(@"error") cancelButtonTile:LocalizedString(@"ok")];
            [_hudview hide:YES];
            _hudview = nil;
            
        }];
}


#pragma mark -
#pragma mark UITableView DataSource
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _routeArray.count;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    HeaderView *headerView = [tableView dequeueReusableCellWithIdentifier:@"HeaderCell"];
    [headerView setTitle:LocalizedString(@"routes")];

    return headerView;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    CenterLabelCell *routeCell = [tableView dequeueReusableCellWithIdentifier:@"CenterLabelCell"];
    [routeCell setTitle:[[_routeArray objectAtIndex:indexPath.row] objectForKey:@"name"]];
    [routeCell setBackgroundColor:[UIColor clearColor]];
    return routeCell;
}

#pragma mark - TableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    //Super Admin Routes
    if(_optionIndex ==0)
    {
        SPReportsViewController  *objSpReports =[[SPReportsViewController alloc]initWithSelectedRoute:[BTSData getSuperAdminReportForRoute:[[[_routeArray objectAtIndex:indexPath.row] objectForKey:@"id"] intValue]] isFromRoute:YES];
        [self.navigationController pushViewController:objSpReports animated:YES];
        
        [UserDefaults setObject:[_routeArray objectAtIndex:indexPath.row] forKey:kUserSelectedRoute];
        [UserDefaults synchronize];
    }
    else
    {
        SPReportsViewController  *objSpReports =[[SPReportsViewController alloc]initWithSelectedRoute:[_routeArray objectAtIndex:indexPath.row] isFromRoute:NO];
        [self.navigationController pushViewController:objSpReports animated:YES];
        
        [UserDefaults setObject:[_routeArray objectAtIndex:indexPath.row] forKey:kUserSelectedRoute];
        [UserDefaults synchronize];
    }
}


@end
