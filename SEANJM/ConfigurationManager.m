//
//  ConfigurationManager.m
//  SEANJM
//
//  Created by Randa Al-Sadek on 5/24/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "ConfigurationManager.h"
#import "AFNetworking.h"
#import "Macros.h"

/*******BTS Configuration **************/

/*******TCS Configuration links*********/

#define getTCSConfiguration @"http://213.236.53.172/sntcs/restserviceimpl.svc/getTCSConfiguration/0"
#define sendCheckOperationService @"http://213.236.53.172/sntcs/RestServiceImpl.svc/sendCheckOperationDetails"
#define getReportsInTentService @"http://213.236.53.172/sntcs/RestServiceImpl.svc/getReportsInTent/%@"

/*******SRS Configuration links*********/

#define getSRSConfiguration @"http://213.236.53.172/snsrs/restserviceimpl.svc/getSRSConfiguration"
#define sendMaintenanceService @"http://213.236.53.172/snsrs/restserviceimpl.svc/sendMaintenanceComplaint"
#define sendTransportationService @"http://213.236.53.172/snsrs/restserviceimpl.svc/sendTransComplaint"
#define sendPublicAffairsService @"http://213.236.53.172/snsrs/restserviceimpl.svc/sendHealthComplaint"
#define sendDeathService @"http://213.236.53.172/snsrs/restserviceimpl.svc/sendDeadComplaint"
#define deleteComplaintService @"http://213.236.53.172/snsrs/restserviceimpl.svc/deleteComplaint"
#define updateComplaintService @"http://213.236.53.172/snsrs/restserviceimpl.svc/updateComplaint"


@implementation ConfigurationManager {
    NSArray *rootCategories;
    NSArray *rootSubCategories;
    NSArray *tentFaults;
    void (^callbackWithResponse)(id);
    void (^callback)(void);
    void (^callbackFail)(void);

    NSUInteger count;
    
    NSString *tent;
    NSNumber *tentLat;
    NSNumber *tentLon;
    NSString *cost;
    int operation;
    NSNumber *currentLocationLat;
    NSNumber *currentLocationLon;
    
    NSString *complainerName;
    NSString *complainerPhone;
    NSString *comments;
    NSString *driverName;
    NSString *guiderName;
    NSNumber *isHasPilgrims;
}
@synthesize currentLocationLat,currentLocationLon;

/*********************BTS Configuration Manager *****************************/
- (void)LoadGroupsForSpecifRoute: (NSString *)serviceURL completion:(void (^)(void)) callback_  failure:(void (^)(void))callbackFail_ {

    callback = callback_;
    callbackFail = callbackFail_;
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

    [manager GET:serviceURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //NSLog(@"All groups: %@", responseObject);
        if(![responseObject  isKindOfClass:[NSNull class]])
        {
            if([responseObject objectForKey:@"Result"])
            {
                [UserDefaults setObject:[responseObject objectForKey:@"Result"] forKey:kAllGroups];
                [UserDefaults synchronize];
            }
        }
        callback();
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"Error: %@", error);
        callbackFail();
    }];
    
}

- (void)LoadBTSSAReportsForAllRoutes: (NSString *)serviceURL completion:(void (^)(void)) callback_  failure:(void (^)(void))callbackFail_ {
    callback = callback_;
    callbackFail = callbackFail_;
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    [manager GET:serviceURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //NSLog(@"JSON: %@", responseObject);
        if(![responseObject  isKindOfClass:[NSNull class]])
        {
            if([responseObject objectForKey:@"Result"])
            {
                [UserDefaults setObject:[responseObject objectForKey:@"Result"] forKey:kSARAllRoutesReports];
                [UserDefaults synchronize];
            }
        }
        callback();
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"Error: %@", error);
        callbackFail();
    }];
    
}

- (void)LoadBTSConfiguration: (NSString *)serviceURL completion:(void (^)(void)) callback_  failure:(void (^)(void))callbackFail_ {
    callback = callback_;
    callbackFail = callbackFail_;

    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    [manager GET:serviceURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        ////NSLog(@"JSON: %@", responseObject);
        
        [UserDefaults setObject:responseObject forKey:kBTSCONFIGURATION];
        [UserDefaults setBool:YES forKey:kBTSConfigLoaded];
        [UserDefaults synchronize];
        callback();
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"Error: %@", error);
        callbackFail();
    }];
    
}

/********************** TCS ConfigurationManager ****************************/

//Load all categories and store threm
- (void) loadTCSConfiguration: (void (^)(void)) callback_ failure:(void (^)(void)) callbackFail_ {
    callback = callback_;
    callbackFail = callbackFail_;
    
    [[AFHTTPRequestOperationManager manager] GET:getTCSConfiguration parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *response = responseObject;
        rootCategories = [response objectForKey:@"categories"];
        rootSubCategories = [response objectForKey:@"types"];

        [UserDefaults setObject:rootCategories forKey:@"rootCategories"];
        [UserDefaults setObject:rootSubCategories forKey:@"rootSubCategories"];
        [UserDefaults synchronize];
        
        callback();
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"Fail to load TCS categories");
        
        callbackFail();
    }];
}

//return an array with all categories names
- (NSArray *) getCategoriesNames {
    [self getRootsFromUserDefaults];
    
    rootCategories = [UserDefaults objectForKey:@"rootCategories"];
    NSMutableArray *categoriesArray = [[NSMutableArray alloc]init];
    
    for (NSDictionary *category in rootCategories) {
        [categoriesArray addObject: [category objectForKey:@"name"]];
    }
    
    return categoriesArray;
}

//return an array with subcategories names for a specific categoryId
- (NSArray *) getSubCategoriesNames: (NSString *)categoryName {
    [self getRootsFromUserDefaults];
    
     NSMutableArray *subCategoriesArray = [[NSMutableArray alloc]init];
     int selectedCategoryId=0;
    
    for (NSDictionary *category in rootCategories) {
        if ([categoryName isEqualToString:[category objectForKey:@"name"]]) {
            selectedCategoryId = [[category objectForKey:@"id"] intValue];
            break;
        }
    }
    
    for (NSDictionary *subcategory in rootSubCategories) {
        if (selectedCategoryId == [[subcategory objectForKey:@"categoryId"] intValue]) {
            [subCategoriesArray addObject:[subcategory objectForKey:@"name"]];
        }
    }
    
    return  subCategoriesArray;
}

- (NSDictionary *)getCategoryObjectByName: (NSString *)categoryName {
    [self getRootsFromUserDefaults];
    
    NSDictionary *categoryObject = [[NSDictionary alloc]init];
    
    for (NSDictionary *category in rootCategories) {
        if ([categoryName isEqualToString:[category objectForKey:@"name"]]) {
            categoryObject = category;
            break;
        }
    }
    return categoryObject;
}

- (NSDictionary *)getCategoryObjectById: (NSNumber *)categoryId {
    [self getRootsFromUserDefaults];

    NSDictionary *categoryObject = [[NSDictionary alloc]init];
    
    for (NSDictionary *category in rootCategories) {
        if (categoryId == [category objectForKey:@"id"]) {
            categoryObject = category;
            break;
        }
    }
    return categoryObject;
}

- (NSDictionary *)getSubCategoryObjectByName: (NSString *)subCategoryName {
    [self getRootsFromUserDefaults];
    
    NSDictionary *subCategoryObject = [[NSDictionary alloc]init];
    
    for (NSDictionary *subCategory in rootSubCategories) {
        if ([subCategoryName isEqualToString:[subCategory objectForKey:@"name"]]) {
            subCategoryObject = subCategory;
            break;
        }
    }
    return subCategoryObject;
}

- (NSDictionary *)getSubCategoryObjectById: (NSNumber *)subCategoryId {
    [self getRootsFromUserDefaults];
    NSDictionary *subCategoryObject = [[NSDictionary alloc]init];
    
    for (NSDictionary *subCategory in rootSubCategories) {
//        if ([[subCategory objectForKey:@"categoryId"]  isEqual: @2]) {
//            //NSLog(@"category id is 2");
//        }
        if ([subCategoryId isEqual:[subCategory objectForKey:@"id"]]) {
            subCategoryObject = subCategory;
            break;
        }
    }
    return subCategoryObject;
}

- (void)sendAllFaults: (NSArray *)newArray completion:(void (^)(void)) callback_  failure:(void (^)(void))callbackFail_{

    callback = callback_;
    callbackFail = callbackFail_;
    
    NSMutableArray *faultsArray = [[NSMutableArray alloc]init];
    
    NSString *date = [self getCurrentDate];

    for (NSDictionary *fault in [newArray objectAtIndex:0]) {
    
        id flag;
        if ([fault valueForKey:@"flag"]) {
            flag = [fault valueForKey:@"flag"];
        }
        else {
           continue;
        }

        if ([[fault objectForKey:@"deleted"]isEqual:@1] && ([[fault objectForKey:@"oldFault"]isEqual:[NSNumber numberWithBool:NO]])) {
            continue;
        }
        
        NSDictionary *faultParams = @{@"categoryId": [fault objectForKey:@"categoryId"],
                                       @"subCategoryId": [fault objectForKey:@"subCategoryId"],
                                       @"count": [fault objectForKey:@"count"],
                                       @"comments": [fault objectForKey:@"comments"],
                                       @"reportDetailsDate": [fault objectForKey:@"reportDetailsDate"],
                                       @"username": [fault objectForKey:@"username"],
                                       @"status": [fault valueForKey:@"status"],
                                       @"flag": flag,
                                       @"reportDetailsDate_u": [fault objectForKey:@"reportDetailsDate_u"],
                                       @"reportDetailsDate_d": [fault objectForKey:@"reportDetailsDate_d"],
                                       @"username_u": [fault objectForKey:@"username_u"],
                                       @"username_d": [fault objectForKey:@"username_d"]};
        [faultsArray addObject:faultParams];
    }
        
    NSString *loginUser;
    operation = [[newArray objectAtIndex:4]intValue];
    
    if (operation == 0) {
        loginUser= [UserDefaults objectForKey:@"loginUser"];
    }
    else if (operation == 1) {
        loginUser = @"";
    }

    [self checkCurrentLocation];

    NSDictionary *params = @{@"itemId": [newArray objectAtIndex:1],
                             @"userName": loginUser,
                             @"operationDate": date,
                             @"phaseId": @2,
                             @"xPosition": [newArray objectAtIndex:2],
                             @"yPosition": [newArray objectAtIndex:3],
                             @"latitude": currentLocationLat,
                             @"longitude": currentLocationLon,
                             @"status": @0,
                             @"positionFlag": @0,
                             @"operation": [NSNumber numberWithInt:operation],
                             @"bivalues":faultsArray};
    
    NSDictionary *mainDictionary = @{@"cod": params};
   // //NSLog(@"%@", params);
   // //NSLog(@"%@", mainDictionary);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];


    [manager POST:sendCheckOperationService parameters:mainDictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [faultsArray removeAllObjects];
        //NSLog(@"JSON: %@", responseObject);
        
        callback();
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"Error: %@", error);
        callbackFail();
    }];

}

-(void)loadFaultsForTent: (NSString *) tentNum completion:(void (^)(void)) callback_ failure:(void (^) (void))callbackFail_{
    callback = callback_;
    callbackFail = callbackFail_;

    [[AFHTTPRequestOperationManager manager] GET:[NSString stringWithFormat:getReportsInTentService, tentNum] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        tentFaults = [[NSArray alloc]initWithArray:responseObject];
        callback();
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"Fail tent");
        
        callbackFail();
    }];
}

-(NSArray *)getOldFaults {
    return tentFaults;
}

-(void) getRootsFromUserDefaults {
    rootCategories = [UserDefaults objectForKey:@"rootCategories"];
    rootSubCategories = [UserDefaults objectForKey:@"rootSubCategories"];
}

//- (void) getTentAndCost:(NSNotification *)notification {
//    NSArray *tentAndCost = notification.object;
//    tent = [tentAndCost objectAtIndex:0];
//    tentLat = [tentAndCost objectAtIndex:1];
//    tentLon = [tentAndCost objectAtIndex:2];
//    cost = [tentAndCost objectAtIndex:3];
//    operation = [[tentAndCost objectAtIndex:4]intValue];
//}

- (NSString *) getCurrentDate {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    
    NSString *date = [dateFormatter stringFromDate:[NSDate date]];

    return date;
}

/********************** SRS ConfigurationManager ****************************/

- (void) loadSRSConfiguration: (void (^)(void)) callback_ failure:(void (^)(void)) callbackFail_ {

    callback = callback_;
    callbackFail = callbackFail_;
    
    [[AFHTTPRequestOperationManager manager] GET:getSRSConfiguration parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSArray *categories = responseObject;
        
        [UserDefaults setObject:categories forKey:@"SRSCategories"];
        
        NSArray *complaintTypes = [[UserDefaults objectForKey:@"SRSCategories"] objectForKey:@"ComplaintTypes"];
        NSArray *maintenanceTypes = [[UserDefaults objectForKey:@"SRSCategories"] objectForKey:@"MaintenanaceTypes"];
        NSArray *transportationTypes = [[UserDefaults objectForKey:@"SRSCategories"] objectForKey:@"TransportationTypes"];
        NSArray *healthTypes = [[UserDefaults objectForKey:@"SRSCategories"] objectForKey:@"HealthTypes"];
        
        [UserDefaults setObject:complaintTypes forKey:@"ComplaintTypes"];
        [UserDefaults setObject:maintenanceTypes forKey:@"MaintenanceTypes"];
        [UserDefaults setObject:transportationTypes forKey:@"TransportationTypes"];
        [UserDefaults setObject:healthTypes forKey:@"HealthTypes"];
        
        NSArray *keys = [[NSArray alloc]initWithObjects:@"0", @"1", @"2", @"-1", nil];
        NSArray *values = [[NSArray alloc]initWithObjects:@"غير منتهية", @"تحت التنفيذ", @"منتهية", @"محذوف", nil];
        NSDictionary *maintenanceStatus = [[NSDictionary alloc]initWithObjects:values forKeys:keys];
        
        [UserDefaults setObject:maintenanceStatus forKey:@"MaintenanceStatus"];
        [UserDefaults synchronize];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"Fail to load SRS categories");
        callbackFail();
    }];
    
}

- (void)sendMaintenanceComplaint: (NSDictionary *)complaintDic completion:(void (^)(void)) callback_  failure:(void (^)(void))callbackFail_{
    
    callback = callback_;
    callbackFail = callbackFail_;
    
    [self checkAvailDataToSend:complaintDic];
    
    NSString *date = [self getCurrentDate];
    NSString *loginUser= [UserDefaults objectForKey:@"loginUser"];
    
    [self checkCurrentLocation];
    
    NSDictionary *params = @{@"regionName": [complaintDic objectForKey:@"region"],
                             @"mainTypeId": [complaintDic objectForKey:@"categoryId"],
                             @"subTypeId": [complaintDic objectForKey:@"subCategoryId"],
                             @"complainerName": complainerName,
                             @"complainerMobile": complainerPhone,
                             @"status": @0,
                             @"reportDate": date,
                             @"userName": loginUser,
                             @"xPosition": [complaintDic objectForKey:@"xPosition"],
                             @"yPosition": [complaintDic objectForKey:@"yPosition"],
                             @"latitude": currentLocationLat,
                             @"longitude": currentLocationLon,
                             @"comment": comments};
    
    NSDictionary *mainDictionary = @{@"mc": params};
    //NSLog(@"%@", params);
    //NSLog(@"%@", mainDictionary);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    [manager POST:sendMaintenanceService parameters:mainDictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //NSLog(@"JSON: %@", responseObject);
        
        callback();
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"Error: %@", error);
        callbackFail();
    }];
    
}

- (void)sendTransportationComplaint: (NSDictionary *)complaintDic completion:(void (^)(void)) callback_  failure:(void (^)(void))callbackFail_ {
    callback = callback_;
    callbackFail = callbackFail_;
    
    NSString *date = [self getCurrentDate];
    NSString *loginUser= [UserDefaults objectForKey:@"loginUser"];
    
    [self checkCurrentLocation];
    [self checkAvailDataToSend:complaintDic];
    
    NSDictionary *params = @{@"mainTypeId": [complaintDic objectForKey:@"mainTypeId"],
                             @"subTypeId": [complaintDic objectForKey:@"subTypeId"],
                             @"busID": [complaintDic objectForKey:@"busID"],
                             @"complaintName": complainerName,
                             @"complaintMobile": complainerPhone,
                             @"driverName": driverName,
                             @"guiderName": guiderName,
                             @"isBusChanged": [complaintDic objectForKey:@"isBusChanged"],
                             @"isHasPilgrims": isHasPilgrims,
                             @"companyID": [complaintDic objectForKey:@"companyID"],
                             @"status": @0,
                             @"reportDate": date,
                             @"userName": loginUser,
                             @"xPosition": [complaintDic objectForKey:@"xPosition"],
                             @"yPosition": [complaintDic objectForKey:@"yPosition"],
                             @"latitude": currentLocationLat,
                             @"longitude": currentLocationLon,
                             @"comment": comments};
    
    NSDictionary *mainDictionary = @{@"tc": params};
    //NSLog(@"%@", params);
    //NSLog(@"%@", mainDictionary);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    [manager POST:sendTransportationService parameters:mainDictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //NSLog(@"JSON: %@", responseObject);
        
        callback();
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"Error: %@", error);
        callbackFail();
    }];
}

- (void)sendPublicAffairsComplaint: (NSDictionary *)complaintDic completion:(void (^)(void)) callback_  failure:(void (^)(void))callbackFail_ {
    callback = callback_;
    callbackFail = callbackFail_;
    
    NSString *date = [self getCurrentDate];
    NSString *loginUser= [UserDefaults objectForKey:@"loginUser"];
    
    NSString *pilgrimID;
    NSString *passportID;
    NSString *healthComments;
    
    if ([complaintDic objectForKey:@"pilgrimID"]) {
        pilgrimID = [complaintDic objectForKey:@"pilgrimID"];
    }else  {
        pilgrimID = @"";
    }
    if ([complaintDic objectForKey:@"passportID"]) {
        passportID = [complaintDic objectForKey:@"passportID"];
    }else {
        passportID = @"";
    }
    if ([complaintDic objectForKey:@"comments"]) {
        healthComments = [complaintDic objectForKey:@"comments"];
    }else {
        healthComments = @"";
    }
    
    [self checkCurrentLocation];
    
    NSDictionary *params = @{@"mainTypeId": [complaintDic objectForKey:@"mainTypeId"],
                             @"pilgrimID": pilgrimID,
                             @"passportID": passportID,
                             @"address": @"",
                             @"gender": [complaintDic objectForKey:@"gender"],
                             @"age": [complaintDic objectForKey:@"age"],
                             @"status": @0,
                             @"reportDate": date,
                             @"userName": loginUser,
                             @"xPosition": [complaintDic objectForKey:@"xPosition"],
                             @"yPosition": [complaintDic objectForKey:@"yPosition"],
                             @"latitude": currentLocationLat,
                             @"longitude": currentLocationLon,
                             @"healthStatus": [complaintDic objectForKey:@"healthStatus"],
                             @"comment": healthComments};
    
    NSDictionary *mainDictionary = @{@"hc": params};
    //NSLog(@"%@", params);
    //NSLog(@"%@", mainDictionary);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    [manager POST:sendPublicAffairsService parameters:mainDictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //NSLog(@"JSON: %@", responseObject);
        
        callback();
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"Error: %@", error);
        callbackFail();
    }];
}

- (void)sendDeathComplaint: (NSDictionary *)complaintDic completion:(void (^)(void)) callback_  failure:(void (^)(void))callbackFail_ {
    callback = callback_;
    callbackFail = callbackFail_;
    
    NSString *date = [self getCurrentDate];
    NSString *loginUser= [UserDefaults objectForKey:@"loginUser"];
    
    NSString *ticketID;
    NSString *deathComments;
    
    if ([complaintDic objectForKey:@"ticketID"]) {
        ticketID = [complaintDic objectForKey:@"ticketID"];
    }else {
        ticketID = @"";
    }
    if ([complaintDic objectForKey:@"comment"]) {
        deathComments = [complaintDic objectForKey:@"comment"];
    }else {
        deathComments = @"";
    }
    
    [self checkCurrentLocation];
    
    NSDictionary *params = @{@"mainTypeId": [complaintDic objectForKey:@"mainTypeId"],
                             @"address": [complaintDic objectForKey:@"address"],
                             @"pilgrimID": [complaintDic objectForKey:@"pilgrimID"],
                             @"isPermit": [complaintDic objectForKey:@"isPermit"],
                             @"permitID": [complaintDic objectForKey:@"permitID"],
                             @"isStamped": [complaintDic objectForKey:@"isStamped"],
                             @"isPassportCopies": [complaintDic objectForKey:@"isPassportCopies"],
                             @"isTicketExisted": [complaintDic objectForKey:@"isTicketExisted"],
                             @"ticketID": ticketID,
                             @"isDelegated": [complaintDic objectForKey:@"isDelegated"],
                             @"delegateID": [complaintDic objectForKey:@"delegateID"],
                             @"isOrginalPassport": [complaintDic objectForKey:@"isOriginalPassport"],
                             @"status": @0,
                             @"reportDate": date,
                             @"userName": loginUser,
                             @"xPosition": [complaintDic objectForKey:@"xPosition"],
                             @"yPosition": [complaintDic objectForKey:@"yPosition"],
                             @"latitude": currentLocationLat,
                             @"longitude": currentLocationLon,
                             @"comment": deathComments};
    
    NSDictionary *mainDictionary = @{@"dc": params};
    //NSLog(@"%@", params);
    //NSLog(@"%@", mainDictionary);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    [manager POST:sendDeathService parameters:mainDictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //NSLog(@"JSON: %@", responseObject);
        
        callback();
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"Error: %@", error);
        callbackFail();
    }];
}

- (void)deleteComplaint: (int)complaintId complaintType: (int)complaintType completion:(void (^)(void)) callback_  failure:(void (^)(void))callbackFail_ {
    callback = callback_;
    callbackFail = callbackFail_;
    
    NSDictionary *params = @{@"complaintId": [NSNumber numberWithInt:complaintId],
                             @"type": [NSNumber numberWithInt:complaintType]};
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    [manager POST:deleteComplaintService parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //NSLog(@"JSON: %@", responseObject);
        
        callback();
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"Error: %@", error);
        callbackFail();
    }];

}

- (void)updateComplaintStatus: (int)complaintId complaintType:(int)complaintType status:(int)status completion:(void (^)(void)) callback_  failure:(void (^)(void))callbackFail_ {
    callback = callback_;
    callbackFail = callbackFail_;
    
    NSDictionary *params = @{@"complaintId": [NSNumber numberWithInt:complaintId],
                             @"type": [NSNumber numberWithInt:complaintType],
                             @"status": [NSNumber numberWithInt:status]};
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    [manager POST:updateComplaintService parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //NSLog(@"JSON: %@", responseObject);
        
        callback();
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"Error: %@", error);
        callbackFail();
    }];
    
}

- (NSArray *) getMaintenanceCategories {
    
    NSArray *maintenanceTypes = [UserDefaults objectForKey:@"MaintenanceTypes"];
    
    NSMutableArray *maintenanceTypesNames = [[NSMutableArray alloc]init];
    
    for (NSDictionary *type in maintenanceTypes) {
        [maintenanceTypesNames addObject:[type objectForKey:@"name"]];
    }
    
    return maintenanceTypesNames;
}

- (NSArray *) getSubMaintenanceNames:(NSString *)categoryName {
    NSMutableArray *categorySubTypesArray = [[NSMutableArray alloc]init];

    NSArray *maintenanceTypes = [UserDefaults objectForKey:@"MaintenanceTypes"];

    id categoryId = 0;
    
    for (NSDictionary *category in maintenanceTypes) {
        if ([[category objectForKey:@"name"] isEqualToString:categoryName]) {
            categoryId = [category objectForKey:@"id"];
            break;
        }
    }
    
    if (categoryId > 0) {
        NSArray *maintenanceSubTypes = [[UserDefaults objectForKey:@"SRSCategories"] objectForKey:@"MaintenanaceSubTypes"];
        
        
        for (NSDictionary *subType in maintenanceSubTypes) {
            if ([[subType objectForKey:@"categoryId"] isEqual:categoryId]) {
                [categorySubTypesArray addObject:[subType objectForKey:@"name"]];
            }
        }

    }

    return categorySubTypesArray;
}

- (NSDictionary *)getMaintenanceTypeByName: (NSString *)categoryName {
    NSArray *maintenanceTypes = [UserDefaults objectForKey:@"MaintenanceTypes"];
    
    NSDictionary *categoryObject = [[NSDictionary alloc]init];
    
    for (NSDictionary *type in maintenanceTypes) {
        if ([[type objectForKey:@"name"] isEqualToString:categoryName]) {
            categoryObject = type;
            break;
        }
    }
    
    return categoryObject;
}

- (NSString *)getMaintenanceTypeById: (NSNumber *)mainId type:(NSString *)type {
    
    NSArray *maintenanceTypes = [UserDefaults objectForKey:type];
    
    NSString *mainName;
    
    for (NSDictionary *mainType in maintenanceTypes) {
        if ([mainId isEqual:[mainType objectForKey:@"id"]]) {
            mainName = [mainType objectForKey:@"name"];
            break;
        }
    }
    return mainName;
}

- (NSDictionary *)getMaintenanceSubTypeByName: (NSString *)subCategoryName {
    NSArray *maintenanceTypes = [[UserDefaults objectForKey:@"SRSCategories"] objectForKey:@"MaintenanaceSubTypes"];
    
    NSDictionary *subCategoryObject = [[NSDictionary alloc]init];
    
    for (NSDictionary *type in maintenanceTypes) {
        if ([[type objectForKey:@"name"] rangeOfString:subCategoryName].location != NSNotFound) {
            subCategoryObject = type;
            break;
        }
    }
    
    return subCategoryObject;
}

- (NSArray *) getRegions {
    
    NSArray *regions = [[UserDefaults objectForKey:@"SRSCategories"] objectForKey:@"Regions"];
    
    NSMutableArray *regionNames = [[NSMutableArray alloc]init];
    
    for (NSDictionary *region in regions) {
        [regionNames addObject:[region objectForKey:@"arname"]];
    }
    
    return regionNames;
}

- (NSArray *) getTransportationSubTypes: (id)categoryId {
    NSArray *transportaionSubTypes = [[UserDefaults objectForKey:@"SRSCategories"] objectForKey:@"TransportationSubTypes"];
    
    NSMutableArray *subTypesNames = [[NSMutableArray alloc]init];
    
    for (NSDictionary *subType in transportaionSubTypes) {
        if ([[subType objectForKey:@"categoryId"] isEqual:categoryId]) {
            [subTypesNames addObject:[subType objectForKey:@"name"]];
        }
    }
    
    return subTypesNames;
}

- (NSDictionary *)getTransSubTypeByName: (NSString *)subCategoryName {
    NSArray *transportaionSubTypes = [[UserDefaults objectForKey:@"SRSCategories"] objectForKey:@"TransportationSubTypes"];

    NSDictionary *subCategoryObject = [[NSDictionary alloc]init];

    for (NSDictionary *subType in transportaionSubTypes) {
        if ([[subType objectForKey:@"name"] isEqualToString:subCategoryName]) {
            subCategoryObject = subType;
            break;
        }
    }
    return subCategoryObject;
}

- (NSArray *) getBusCompanieaNames {
    NSArray *busCompanies = [[UserDefaults objectForKey:@"SRSCategories"] objectForKey:@"BusCompanies"];
    
    NSMutableArray *busNames = [[NSMutableArray alloc]init];
    
    for (NSDictionary *busCompany in busCompanies) {
        [busNames addObject:[busCompany objectForKey:@"name"]];
    }
         return busNames;
}

- (NSDictionary *)getBusObjectByName: (NSString *)companyName {
    NSArray *busCompanies = [[UserDefaults objectForKey:@"SRSCategories"] objectForKey:@"BusCompanies"];
    
    NSDictionary *busObject = [[NSDictionary alloc]init];
    
    for (NSDictionary *bus in busCompanies) {
        if ([[bus objectForKey:@"name"] isEqualToString:companyName]) {
            busObject = bus;
            break;
        }
    }
    return busObject;
}

-(NSString *)getDateFromSeconds:(double)seconds {
//    double seconds = [[attributes objectForKey:@"time"] doubleValue]/1000;
    
    NSDate *epochNSDate = [[NSDate alloc] initWithTimeIntervalSince1970:seconds];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    
    NSString *date = [dateFormatter stringFromDate:epochNSDate];
    //NSLog (@"Date: %@", date);
    
    return  date;
}

- (void) checkCurrentLocation {

    if ([UserDefaults objectForKey:@"currentLat"]) {
        currentLocationLat = [UserDefaults objectForKey:@"currentLat"];
    }
    else  {
        currentLocationLat = @0;
    }
    if ([UserDefaults objectForKey:@"currentLon"]) {
        currentLocationLon = [UserDefaults objectForKey:@"currentLon"];
    }
    else {
        currentLocationLon = @0;
    }
}

- (void) checkAvailDataToSend:(NSDictionary *)complaintDic {
    if ([complaintDic objectForKey:@"complaintName"]) {
        complainerName = [complaintDic objectForKey:@"complaintName"];
    }else  {
        complainerName = @"";
    }
    if ([complaintDic objectForKey:@"complaintMobile"]) {
        complainerPhone = [complaintDic objectForKey:@"complaintMobile"];
    }else {
        complainerPhone = @"";
    }
    if ([complaintDic objectForKey:@"comments"]) {
        comments = [complaintDic objectForKey:@"comments"];
    }else {
        comments = @"";
    }
    if ([complaintDic objectForKey:@"driverName"]) {
        driverName = [complaintDic objectForKey:@"driverName"];
    }else {
        driverName = @"";
    }
    if ([complaintDic objectForKey:@"guiderName"]) {
        guiderName = [complaintDic objectForKey:@"guiderName"];
    }else {
        guiderName = @"";
    }
    if ([complaintDic objectForKey:@"isHasPilgrims"]) {
        isHasPilgrims = [complaintDic objectForKey:@"isHasPilgrims"];
    }else {
        isHasPilgrims = @0;
    }
}

@end
