//
//  OptionsViewController.m
//  SEANJM
//
//  Created by Randa Al-Sadek on 6/13/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "OptionsViewController.h"
#import "MainViewController.h"
#import "FaultsTableViewController.h"
#import "Macros.h"
#import "LanguageController.h"
#import "Utility.h"
@interface OptionsViewController ()

@end

@implementation OptionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithTitle:LocalizedString(@"back")
                                                                 style:UIBarButtonItemStylePlain
                                                                target:nil
                                                                action:nil];
    
    [self.navigationItem setBackBarButtonItem:backItem];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO];
    [Utility setNavigationBarProperties:self.navigationController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)minaButtonPressed:(id)sender {
    FaultsTableViewController *faultsTableView = [[FaultsTableViewController alloc]init];
    [self.navigationController pushViewController:faultsTableView animated:YES];
    
}
- (IBAction)arafahButtonPressed:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:LocalizedString(@"notAvailable") delegate:self cancelButtonTitle:LocalizedString(@"ok") otherButtonTitles:nil, nil];
    [alert show];
}

@end
