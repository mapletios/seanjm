//
//  CustomTableViewCell.m
//  SEANJM
//
//  Created by Randa Al-Sadek on 9/9/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "CustomTableViewCell.h"

@implementation CustomTableViewCell {

}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
