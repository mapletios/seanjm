//
//  AddBusesViewController.m
//  SEANJM
//
//  Created by Abdul Kareem on 9/9/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "AddBusesViewController.h"
#import "AddUserCell.h"
#import "ConfirmationCell.h"
#import "Macros.h"
#import "LanguageController.h"
#import "AFNetworking.h"
#import "Utility.h"
#import "UserInformation.h"
#import "MBProgressHUD.h"
#import "ActionSheetStringPicker.h"
#import "BTSData.h"
#import "AddBusCell.h"

#define BTSgetBusDetails @"http://213.236.53.172/snbts/restserviceimpl.svc/getBusDetails"
@interface AddBusesViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    ConfirmationCell *footerView;
}

@property (nonatomic)MBProgressHUD *hudview;
@property (nonatomic,weak)IBOutlet UITableView *addUserTableView;
@property (nonatomic)NSString           *addbusesCellIdentifier;
@property (nonatomic)NSString           *addBusesFooterIdentifier;
@property (nonatomic)NSMutableArray     *addedBusArray;
@property (nonatomic)NSMutableArray     *allBusesArray;
@property (nonatomic)NSInteger          selectedIndex;
@property (nonatomic)NSMutableDictionary *busDic;
@property (nonatomic)NSMutableArray     *rowsPlaceholder;
@property (nonatomic)NSDictionary       *selectedTask;
@property (nonatomic)NSMutableDictionary *infoTask;
@property (nonatomic)BOOL               isNumberOfRoundViewShow;
@property (nonatomic)NSArray            *busesArray;
@property (nonatomic)NSMutableArray     *busTextFieldsArray;
@property (nonatomic)NSMutableArray     *busDicKeysArray;
@property (nonatomic)NSMutableArray     *inheritedBusArray;
@end

@implementation AddBusesViewController

-(id)initWithSelectedTask :(NSDictionary *)taskdictionary
{
    self = [super init];
    _selectedTask = [NSDictionary dictionaryWithDictionary:taskdictionary];
    return self;
}

-(id)initWithinfo :(NSDictionary *)infodictionary
{
    self =[super init];
    _infoTask = [NSMutableDictionary dictionaryWithDictionary:infodictionary];
    return self;
}


-(id)initWithNumberOfRoundsView
{
    self = [super init];
    _isNumberOfRoundViewShow = YES;
    return self;
}

-(id)initWithBusArray :(NSMutableArray *)bus_array
{
    self = [super init];
    _inheritedBusArray = bus_array;
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    
    if(_infoTask)
    {
        //[self loadInfo];
        _busDic = [NSMutableDictionary dictionaryWithDictionary:_infoTask];
        [_addUserTableView setUserInteractionEnabled:NO];
    }
    else if(_selectedTask)
        _busDic = [NSMutableDictionary dictionaryWithDictionary:_selectedTask];
    else
        _busDic = [NSMutableDictionary dictionary];
    
    _addedBusArray  = [NSMutableArray array];
    _busDicKeysArray  = [NSMutableArray arrayWithObjects:
                         @"routeId",
                         @"creator",
                         @"plateNumber",
                         @"groupId",
                         @"busCapacity",
                         @"roundsNumber",
                         @"driverName",
                         @"creationDate",
                         @"senderId",
                         @"senderLocationId",
                         @"receiverId",
                         @"receiverLocationId",
                         @"guiderId",
                         nil];
    
    if(_isNumberOfRoundViewShow)
    {
        _rowsPlaceholder = [NSMutableArray arrayWithObjects:kroundsNumber,nil];
        _addbusesCellIdentifier   = @"AddUserCell";
    }
    else
    {
        _addbusesCellIdentifier = @"AddBusCell";
        _rowsPlaceholder = [NSMutableArray arrayWithObjects:@"companyName",@"plateNumber",@"model",@"driverName",@"busCapacity",
                            @"senderLocationName",
                            @"senderName",@"receiverLocationName",@"receiverName",@"guiderName",@"roundsNumber", nil];
        _busTextFieldsArray = [NSMutableArray array];
    }
    
    _addBusesFooterIdentifier = @"ConfirmationCell";
    [_addUserTableView registerNib:[UINib nibWithNibName:_addbusesCellIdentifier bundle:[NSBundle mainBundle]]forCellReuseIdentifier:_addbusesCellIdentifier];

    [_addUserTableView registerNib:[UINib nibWithNibName:_addBusesFooterIdentifier bundle:[NSBundle mainBundle]]forCellReuseIdentifier:_addBusesFooterIdentifier];
    [self loadUsers];
    
    UITapGestureRecognizer *singleTapgesture =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:singleTapgesture];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [Utility setBlackShadowToAllLabels:_addUserTableView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.backItem.title = LocalizedString(@"back");
    self.navigationController.navigationBar.topItem.title = [[UserInformation getUserSelectedRoute] objectForKey:@"name"];
    
    
    [self.navigationItem setRightBarButtonItem:nil];
    
    NSIndexPath *tableSelection = [_addUserTableView indexPathForSelectedRow];
    [_addUserTableView deselectRowAtIndexPath:tableSelection animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if(![self.navigationController.viewControllers containsObject:self])
    {
        [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    }
}

-(void)loadInfo
{
    NSDictionary *busObject = [BTSData getBusInfoFromBusId:[_infoTask objectForKey:@"busId"]];
    if(busObject.count>0)
    {
        [_infoTask addEntriesFromDictionary:busObject];
    }
}

-(void)loadUsers
{
    _allBusesArray = [NSMutableArray arrayWithArray:[BTSData getUsers]];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark IBActions
-(IBAction)sendTapped:(id)sender
{
    
}

-(void)checkInputValuesValidation
{
    //For RoundNo View
    if(_isNumberOfRoundViewShow)
    {
        if([_busDic objectForKey:kroundsNumber])
        {
            [footerView.btnAdd setUserInteractionEnabled:YES];
            [footerView.btnAdd setHighlighted:NO];
            return;
        }
        [footerView.btnAdd setUserInteractionEnabled:NO];
        [footerView.btnAdd setHighlighted:YES];
        return;
    }
}

-(BOOL)checkInputValidationForBus
{
    //Bus Form
    for (NSString *busKey in _busDicKeysArray) {
        
        if(![_busDic objectForKey:busKey])
            return NO;
    }
    
    
    [footerView.btnAdd setUserInteractionEnabled:YES];
    [footerView.btnAdd setHighlighted:NO];
    return YES;
}

#pragma mark -
#pragma mark UITableView DataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _rowsPlaceholder.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section;
{
    if(!_infoTask)
    {
        footerView = [tableView dequeueReusableCellWithIdentifier:_addBusesFooterIdentifier];
        [footerView.btnAdd addTarget:self action:@selector(addButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [footerView.btnCancel addTarget:self action:@selector(cancelButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        [footerView.btnAdd setTitle:LocalizedString(@"save") forState:UIControlStateNormal];
        [footerView.btnCancel setTitle:LocalizedString(@"cancel") forState:UIControlStateNormal];
        [footerView.btnAdd.layer setCornerRadius:4.0];
        [footerView.btnAdd setClipsToBounds:YES];
        [footerView.btnCancel.layer setCornerRadius:4.0];
        [footerView.btnCancel setClipsToBounds:YES];
        
        
        if(_isNumberOfRoundViewShow)
        {
            [footerView.btnAdd setUserInteractionEnabled:NO];
            [footerView.btnAdd setHighlighted:YES];
        }
        [tableView setClipsToBounds:YES];
        return footerView.contentView;
    }
    else
        return [UIView new];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if(_isNumberOfRoundViewShow)
    {
        AddUserCell *addUserCell =[tableView dequeueReusableCellWithIdentifier:_addbusesCellIdentifier];
        addUserCell.txtUserName.delegate    = self;
        [addUserCell.txtUserName setPlaceholder:LocalizedString([_rowsPlaceholder objectAtIndex:indexPath.row])];
        [addUserCell.txtUserDetail setHidden:YES];
        addUserCell.txtUserName.tag = indexPath.row;
        [addUserCell.txtUserName setKeyboardType:UIKeyboardTypeNumberPad];
        [addUserCell setEditing:YES];
        [addUserCell.txtUserName setAccessibilityHint:kroundsNumber];
        return addUserCell;
    }
    else
    {
        AddBusCell *addbusCell =[tableView dequeueReusableCellWithIdentifier:_addbusesCellIdentifier];
        addbusCell.txtUserDetail.delegate = self;
        
        NSString *rightLabelText;
        
        if(indexPath.row == 2)
            rightLabelText = [NSString stringWithFormat:@"%@:",LocalizedString([_rowsPlaceholder objectAtIndex:indexPath.row])];
        else
            rightLabelText = [NSString stringWithFormat:@"*%@:",LocalizedString([_rowsPlaceholder objectAtIndex:indexPath.row])];
        
        [addbusCell.lblInfo setText:rightLabelText];
        
        [addbusCell.txtUserDetail setPlaceholder:LocalizedString([_rowsPlaceholder objectAtIndex:indexPath.row])];
        [addbusCell.txtUserDetail setUserInteractionEnabled:YES];
        [addbusCell.txtUserDetail setTextAlignment:NSTextAlignmentRight];
        [addbusCell.txtUserDetail setAccessibilityHint:[_rowsPlaceholder objectAtIndex:indexPath.row]];
        addbusCell.txtUserDetail.tag = indexPath.row;
        
        
        //Add bus textfield into an array so we can auto fill data later by type Bus No
        //PlateNO,Model,Driver Name
        if(indexPath.row >0 && indexPath.row <= 3)
        {
            [addbusCell.txtUserDetail setEnabled:NO];
            [_busTextFieldsArray addObject:addbusCell.txtUserDetail];
        }
        //Round NO
        if(indexPath.row == _rowsPlaceholder.count-1)
        {
            NSString *roundNo =[[BTSData getDefaultNumberOfRoundsFromSelectedRoute] stringValue];
            [addbusCell.txtUserDetail setText:roundNo];
            [addbusCell.txtUserDetail setKeyboardType:UIKeyboardTypeNumberPad];
            [_busDic setObject:roundNo forKey:kroundsNumber];
        }
        //Capacity
        if(indexPath.row == 4)
        {
            [addbusCell.txtUserDetail setKeyboardType:UIKeyboardTypeNumberPad];
            [addbusCell.txtUserDetail setText:@"50"];
            [_busDic setObject:@"50" forKey:@"busCapacity"];
        }
        
        [addbusCell.txtUserDetail addTarget:self action:@selector(dismissKeyboard) forControlEvents:UIControlEventEditingDidEndOnExit];
        
        if(_infoTask)
        {
            id castValue =[_infoTask objectForKey:[_rowsPlaceholder objectAtIndex:indexPath.row]];
            if(![castValue isKindOfClass:[NSString class]])castValue =[castValue stringValue];
            if([_infoTask objectForKey:addbusCell.txtUserDetail.accessibilityHint])
                [addbusCell.txtUserDetail setText:castValue];
        }
        
        return addbusCell;
    }
}

#pragma mark - TableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section;
{
    if(_infoTask)
        return 0.0;
    else
        return 80.0;
}

#pragma mark -
#pragma mark - IBActions

-(IBAction)addButtonTapped:(UIButton *)sender
{
    [self.view endEditing:YES];
    if(_isNumberOfRoundViewShow)
    {
        DispatchEvent(@"addNumberOfRounds", _busDic);
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    [_busDic setObject:[[UserInformation getUserSelectedRoute] objectForKey:@"id"] forKey:krouteId];
    [_busDic setObject:[Utility getcurrentDateOnly] forKey:@"creationDate"];
    [_busDic setObject:[UserInformation getUserId] forKey:kcreator];
    [_busDic setObject:[UserInformation getGroupID] forKey:kgroupId];
    [_busDic setObject:@"0" forKey:@"addItem"];
    
   //NSLog(@"addind dictionary %@",_busDic);
    if([self checkInputValidationForBus])
    {
        
        //Update User
        if(_selectedTask)
        {
            [UserDefaults setObject:_busDic forKey:kUpdateBus];
            [UserDefaults synchronize];
        }
        //Add new user
        else
        {
            BOOL isItemUnique = false;
            //Checking This item is already added OR not if atleast one item is different let it add
            if(_inheritedBusArray.count >0)
            {
                NSArray *filteredArray = [_inheritedBusArray filteredArrayUsingPredicate:
                                          [NSPredicate predicateWithFormat:
                                           @"companyName == %@ && plateNumber == %@ && driverName == %@ && senderId == %@ && senderLocationId == %@ && receiverId == %@ && receiverLocationId == %@ && guiderId == %@",[_busDic objectForKey:@"companyName"],[_busDic objectForKey:@"plateNumber"],
                                           [_busDic objectForKey:@"driverName"],[_busDic objectForKey:@"senderId"],
                                           [_busDic objectForKey:@"senderLocationId"],[_busDic objectForKey:@"receiverId"],
                                           [_busDic objectForKey:@"receiverLocationId"],[_busDic objectForKey:@"guiderId"]]];
                
            
                if(filteredArray.count == 0)
                    isItemUnique = YES;
            }

            if(isItemUnique || _inheritedBusArray.count == 0)
            {
                [UserDefaults setObject:_busDic forKey:kAddnewBus];
                [UserDefaults synchronize];
                [self.navigationController popViewControllerAnimated:YES];
                return;
            }
            else
            {
                [Utility showAlertViewWithTitle:@"" message:LocalizedString(@"alreadyAdded") cancelButtonTile:LocalizedString(@"ok")];
            }
        }
        
        
        
    }
    else
        [Utility showAlertViewWithTitle:@"" message:LocalizedString(@"pleasefillallfields") cancelButtonTile:LocalizedString(@"ok")];
}

-(IBAction)cancelButtonTapped:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITextField Delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField
{

    if(!_isNumberOfRoundViewShow)
    {
        
    //Sender locaiton, sender name, receiver locaiton ,
    if(textField.tag ==0 || textField.tag ==5 || textField.tag ==6 || textField.tag ==7 || textField.tag ==8 || textField.tag ==9)
    {
        [self.view endEditing:YES];
        NSArray *pickerValues =[NSMutableArray array];
        NSArray *allValue =[NSMutableArray array];
        
        //Sender Location Name
        if(textField.tag ==0)
        {
            allValue = [BTSData getBusCompanies];
            pickerValues = [allValue  valueForKey:@"name"];
        }
        
        //Sender Location Name
        if(textField.tag ==5)
        {
            allValue = [BTSData getSenderLocationsForCoordinator];
            pickerValues = [allValue  valueForKey:@"locationName"];
        }
        //Sender Name
        else if(textField.tag ==6)
        {
            allValue = [BTSData getSenderLocationsForCoordinator];
            pickerValues = [allValue  valueForKey:@"name"];
        }
        //Receiver Location Name
        else if(textField.tag ==7)
        {
            allValue = [BTSData getReceiverLocationsForCoordinator];
            pickerValues = [allValue  valueForKey:@"locationName"];
        }
        //Receiver
        else if(textField.tag ==8)
        {
            allValue = [BTSData getReceiverLocationsForCoordinator];
            pickerValues = [allValue  valueForKey:@"name"];
        }
        //Guider Name
        else if(textField.tag ==9)
        {
            allValue = [BTSData getAllGuidersForCoordinator];
            pickerValues = [allValue  valueForKey:@"name"];
        }
        
        if(pickerValues.count ==0)
        {
            [Utility showAlertViewWithTitle:@"" message:LocalizedString(@"nodata") cancelButtonTile:LocalizedString(@"ok")];
            return;
        }
        
        [ActionSheetStringPicker showPickerWithTitle:LocalizedString(@"select") rows:pickerValues initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
            [self dismissKeyboard];
            [textField setText:selectedValue];
            
            if(textField.tag ==0)
            {
                [_busDic setObject:[[allValue objectAtIndex:selectedIndex] objectForKey:@"name"]forKey:@"companyName"];
                [(UITextField *)[_busTextFieldsArray objectAtIndex:0] setEnabled:YES];
            }
            
            if(textField.tag ==5)
                [_busDic setObject:[[allValue objectAtIndex:selectedIndex] objectForKey:@"locationId"]forKey:@"senderLocationId"];
            else if(textField.tag ==6)
                [_busDic setObject:[[allValue objectAtIndex:selectedIndex] objectForKey:@"userId"]forKey:@"senderId"];
            else if(textField.tag ==7)
                [_busDic setObject:[[allValue objectAtIndex:selectedIndex] objectForKey:@"locationId"]forKey:@"receiverLocationId"];
            else if(textField.tag ==8)
                [_busDic setObject:[[allValue objectAtIndex:selectedIndex] objectForKey:@"userId"]forKey:@"receiverId"];
            else if(textField.tag ==9)
                [_busDic setObject:[[allValue objectAtIndex:selectedIndex] objectForKey:@"guiderId"]forKey:@"guiderId"];
            
            //[_busDic setObject:[UserInformation getUserId] forKey:textField.accessibilityHint];
        } cancelBlock:^(ActionSheetStringPicker *picker) {
            
        } origin:textField];
        
    }
    else if(textField.tag == _rowsPlaceholder.count-1)
    {
        
    }
    }
}

- (void)keyboardWillShow:(NSNotification *)sender
{
    CGSize kbSize = [[[sender userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    NSTimeInterval duration = [[[sender userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration animations:^{
        UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0);
        [_addUserTableView setContentInset:edgeInsets];
        [_addUserTableView setScrollIndicatorInsets:edgeInsets];
    }];
}

- (void)keyboardWillHide:(NSNotification *)sender
{
    NSTimeInterval duration = [[[sender userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration animations:^{
        UIEdgeInsets edgeInsets = UIEdgeInsetsZero;
        [_addUserTableView setContentInset:edgeInsets];
        [_addUserTableView setScrollIndicatorInsets:edgeInsets];
    }];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    textField.text = [Utility replaceArabicNumbers:textField.text];
    if(textField.text.length == 0)
    {
        [_busDic removeObjectForKey:textField.accessibilityHint];
        [self checkInputValuesValidation];
        [self.view endEditing:YES];
        return;
    }
    //For NumberoF Rounds Form
    if(_isNumberOfRoundViewShow)
    {
        if(![self checkNumbersValidation:textField])
        {
            textField.text = @"";
            [_busDic removeObjectForKey:textField.accessibilityHint];
        }
        else
            [_busDic setObject:textField.text forKey:textField.accessibilityHint];
        
        [self checkInputValuesValidation];
    }
    //For Buses Form
    else
    {
        if(textField.text.length == 0)
        {
            [_busDic removeObjectForKey:textField.accessibilityHint];
            return;
        }
        
        if(textField.tag == 1)
        { 
            //Get BusInfo From COnfig Service
            NSDictionary *busObject = [BTSData getBusFromPlateNumber:textField.text];
            
            if(busObject.count>0)
                [self autoFillBusData:busObject];
            else
            {
                [_busDic setObject:[textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"plateNumber"];
                //Call webservice to check it existed or not
                //[textField setText:@""];
                //[_busDic removeObjectForKey:@"busId"];
                if(textField.text.length >0)
                    [self getBusDetailService];
            }
            
            [self.view endEditing:YES];
        }
        [_busDic setObject:[textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:textField.accessibilityHint];
    }
}

-(void)autoFillBusData :(NSDictionary *)busObject
{
    if(busObject.count>0)
    {
        [_busDic setObject:[busObject objectForKey:@"Id"]forKey:@"busId"];
        for (UITextField  *busTextField in _busTextFieldsArray) {
            
            //@"BusNumber",@"BusType",@"BusCompany",@"DriverName"
            [busTextField setText:[busObject objectForKey:busTextField.accessibilityHint]];
            [_busDic setObject:[busObject objectForKey:busTextField.accessibilityHint]forKey:busTextField.accessibilityHint];
            //PlateNo
            if(busTextField.tag != 1)
                [busTextField setEnabled:NO];
        }
    }
    else
    {
        for (UITextField  *busTextField in _busTextFieldsArray) {
            //@"BusNumber",@"BusType",@"BusCompany",@"DriverName"
            [busTextField setEnabled:YES];
            if(busTextField.tag != 1)
            {
                NSLog(@"acce  key %@",busTextField.accessibilityHint);
                [_busDic removeObjectForKey:busTextField.accessibilityHint];
                [busTextField setText:@""];
            }
            
        }
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField.text.length == 0)
    {
        [_busDic removeObjectForKey:textField.accessibilityHint];
        [self checkInputValuesValidation];
    }
    
    return YES;
}

#pragma mark Phone No Validation
-(BOOL)checkNumbersValidation :(UITextField *)phoneNumber
{
    if(phoneNumber.text.length > 0)
    {
        NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:phoneNumber.text];
        return [numbersOnly isSupersetOfSet:characterSetFromTextField];
    }
    
    return NO;
}
- (BOOL)validatePhone:(NSString *)phoneNumber
{
    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    return [phoneTest evaluateWithObject:phoneNumber];
}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

///////****************************Check Bus Details****************////
-(void)getBusDetailService
{

    if(!_hudview)
    {
        _hudview = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        _hudview.mode = MBProgressHUDModeIndeterminate;
        _hudview.labelText = @"Loading...";
        [_hudview setDimBackground:YES];
    }
    
    NSMutableDictionary *parameters =[NSMutableDictionary dictionary];
    [parameters setObject:[[_busDic objectForKey:@"plateNumber"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"plateNumber"];
    
    [parameters setObject:[[_busDic objectForKey:@"companyName"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"companyName"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager POST:BTSgetBusDetails parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //NSLog(@"JSON: %@", responseObject);
        
        if(![responseObject  isKindOfClass:[NSNull class]])
        {
            //if(![[responseObject objectForKey:@"Result"] isKindOfClass:[NSNull class]] && ([responseObject count] != 0))
            {
                [self autoFillBusData:[responseObject objectForKey:@"Result"]];
            }
            
        }
        else {
            [Utility showAlertViewWithTitle:LocalizedString(@"error")  message:LocalizedString(@"error") cancelButtonTile:LocalizedString(@"ok")];
        }
        [_hudview hide:YES];
        _hudview = nil;
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [Utility showAlertViewWithTitle:LocalizedString(@"error")  message:LocalizedString(@"error") cancelButtonTile:LocalizedString(@"ok")];
        //[activityIndicator stopAnimating];
        [_hudview hide:YES];
        _hudview = nil;
    }];
}


@end
