//
//  RoutesViewController.m
//  SEANJM
//
//  Created by Abdul Kareem on 9/2/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "RoutesViewController.h"
#import "TwoTitleCell.h"
#import "Macros.h"
#import "LanguageController.h"
#import "HeaderView.h"
#import "BTSData.h"
#import "UserInformation.h"
#import "CoordinatorHeaderView.h"
#import "TasksViewController.h"
#import "AddUsersViewController.h"
#import "CoordinatorRoutes.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "BusesListViewController.h"
#import "AddBusesViewController.h"
#import "Utility.h"
#import "AdminReportsViewController.h"

#define BTSGetAllUsersAndGuiders @"http://213.236.53.172/snbts/restserviceimpl.svc/getUsersAndGuidersAssignedByManager"

#define BTSGetAllAssignedbusesByCoordinator @"http://213.236.53.172/snbts/restserviceimpl.svc/getBusesAssignedByCoordinatorExtended"

@interface RoutesViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    int numberOfBusRounds;
    NSInteger selectedRouteIndexForNumberOfRounds;
}
@property (nonatomic,weak)IBOutlet UITableView *routeTableView;
@property (nonatomic)NSString *routeCellIdentifier;
@property (nonatomic)NSMutableArray *assignedRouteArray;
@property (nonatomic)NSMutableArray *routeArray;
@property (nonatomic)NSMutableArray *busesArray;
@property (nonatomic)NSDictionary *userSelectedRole;
@property (nonatomic)MBProgressHUD *hudview;
@property (nonatomic)NSString *managerServiceURL;
@property (nonatomic)NSString *coordinatorServiceURL;

@end

@implementation RoutesViewController

-(id)initWithRole :(NSDictionary *)selected_role
{
    self = [super init];
    _userSelectedRole = selected_role;
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _assignedRouteArray = [NSMutableArray array];
    _routeArray = [NSMutableArray array];
    _busesArray = [NSMutableArray array];
    numberOfBusRounds = 1;
    [self loadRoutesForSlectedDesignation];
    
    _routeCellIdentifier = @"TwoTitleCell";
    [_routeTableView registerNib:[UINib nibWithNibName:_routeCellIdentifier bundle:[NSBundle mainBundle]]forCellReuseIdentifier:_routeCellIdentifier];
    [_routeTableView registerNib:[UINib nibWithNibName:@"HeaderView" bundle:[NSBundle mainBundle]]forCellReuseIdentifier:@"HeaderCell"];
    [_routeTableView registerNib:[UINib nibWithNibName:@"CoordinatorHeaderView" bundle:[NSBundle mainBundle]]forCellReuseIdentifier:@"CoorHeaderCell"];
    
    AddObserver(self, @selector(getNumberOfRoundsForBus:), @"addNumberOfRounds", nil);
    AddObserver(self, @selector(loadWebServiceForManager), @"loadWebServiceForManager", nil);
    AddObserver(self, @selector(loadWebServiceForCoordinator), @"loadWebServiceForCoordinator", nil);
}

-(void)viewWillDisappear:(BOOL)animated
{
    if(![self.navigationController.viewControllers containsObject:self])
    {
        RemoveObserver(@"addNumberOfRounds");
        RemoveObserver(@"loadWebServiceForManager");
        RemoveObserver(@"loadWebServiceForManager");
    };
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationItem.title = LocalizedString(@"back");
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    
    UIBarButtonItem *btnRefresh;
    if([[UserInformation getUserSelectedRoleId] isEqualToString:@"2"])
        btnRefresh=[[UIBarButtonItem alloc] initWithTitle:LocalizedString(@"refresh") style:UIBarButtonItemStylePlain target:self action:@selector(loadWebServiceForManager)];
    else if([[UserInformation getUserSelectedRoleId] isEqualToString:@"3"])
        btnRefresh=[[UIBarButtonItem alloc] initWithTitle:LocalizedString(@"refresh") style:UIBarButtonItemStylePlain target:self action:@selector(loadWebServiceForCoordinator)];
    
    [self.navigationItem setRightBarButtonItem:btnRefresh];
    
}


-(void)getNumberOfRoundsForBus : (NSNotification *)notification
{
    NSDictionary *numberOfRoundsDic =  notification.object;
    numberOfBusRounds = [[numberOfRoundsDic objectForKey:kroundsNumber] intValue];
    
    CoordinatorRoutes *coorRoute = [_routeArray objectAtIndex:selectedRouteIndexForNumberOfRounds];
    coorRoute.numberOfRounds = [NSNumber numberWithInt:numberOfBusRounds];
    [_routeTableView reloadData];
    
    [BTSData saveCoordinatorRoutesWithNumberOfRounds:_routeArray];
}

-(void)loadRoutesForSlectedDesignation
{
    @autoreleasepool {
        
    NSArray *allroutes =[NSArray arrayWithArray:[BTSData getRoutes]];
    
        //User is GM load all routes and select role as Manager
    if(([[[_userSelectedRole objectForKey:@"id"] stringValue] isEqualToString:kBTSManagerID]) && ([UserInformation getIsUserGM]))
    {
        _assignedRouteArray = allroutes.mutableCopy;
    }
    else
    {
        //Role for specific Route
        NSArray *userRoles =[UserInformation getUserRoles];
        NSMutableArray *matchRoutesArray =[NSMutableArray array];
        for (NSDictionary *role in userRoles) {

            if([[[role objectForKey:@"roleId"] stringValue] isEqualToString:[[_userSelectedRole objectForKey:@"id"] stringValue]])
                [matchRoutesArray addObject:[[role objectForKey:@"routeId"] stringValue]];
        }
        
        //Route for my role
        for (NSString *routeID in matchRoutesArray) {
            
            for (NSDictionary *route in allroutes) {
                if([routeID isEqualToString:[[route objectForKey:@"id"] stringValue]])
                    [_assignedRouteArray addObject:route];
            }
                
        }
        
    }

        if([[UserInformation getUserSelectedRoleId] isEqualToString:@"2"])
        {
            [self performSelector:@selector(loadWebServiceForManager) withObject:nil afterDelay:0.0005];
            
        }
        //Coordinator
        else if([[UserInformation getUserSelectedRoleId] isEqualToString:@"3"])
        {
            [self performSelector:@selector(loadWebServiceForCoordinator) withObject:nil afterDelay:0.0005];
        }
    }
    
}


-(void)loadWebServiceForManager
{
    _managerServiceURL = [NSString stringWithFormat:@"%@/%@/%@",BTSGetAllUsersAndGuiders,[UserInformation getUserId],[UserInformation getGroupID]];
    _routeArray = _assignedRouteArray;
    [self loadService:_managerServiceURL];
}

-(void)loadWebServiceForCoordinator
{
    _coordinatorServiceURL = [NSString stringWithFormat:@"%@/%@/%@",BTSGetAllAssignedbusesByCoordinator,[UserInformation getUserId],[UserInformation getGroupID]];
    [self loadService:_coordinatorServiceURL];
}
#pragma mar Manager

#pragma mark - Coordinator
//Manipulate Array for Coordinator Routes
-(void)loadService :(NSString *)service_URL
{
    if(![Utility checkInternetConnection])
        return;
    if(!_hudview)
    {
    _hudview = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _hudview.mode = MBProgressHUDModeIndeterminate;
    _hudview.labelText = @"Loading...";
    [_hudview setDimBackground:YES];
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager GET:service_URL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //NSLog(@"Assigned Buses List %@", responseObject);
        if(![responseObject  isKindOfClass:[NSNull class]])
        {
            //Coordinator
            if([operation.request.URL.relativeString isEqualToString:_coordinatorServiceURL])
            {
                _busesArray = [responseObject objectForKey:@"Result"];
                [UserDefaults setObject:_busesArray forKey:kBTS_C_Assignedbuses];
                [UserDefaults synchronize];
                [_hudview setHidden:YES];
                [self saveDictionary];
            }
            //////Manager
            else if([operation.request.URL.relativeString isEqualToString:_managerServiceURL])
            {
                //NSLog(@"Assigned users List %@", responseObject);
                [UserDefaults setObject:responseObject forKey:kBTS_M_AssignedUsersAndGuides];
                [UserDefaults synchronize];
                [_hudview setHidden:YES];
                DispatchEvent(@"UpdateUserGuidersListLoad", nil);
                [_routeTableView reloadData];
            }

        }
        [_hudview hide:YES];
        _hudview = nil;
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"BTS Error: %@", error);
        [_hudview setHidden:YES];
        _hudview = nil;
    }];
}

-(void)saveDictionary
{
    @autoreleasepool {
        [_routeArray removeAllObjects];
        NSArray *savedRoutes = [BTSData getCoordinatorRoutesWithNumberOfRounds];
    for (int a=0; a < _assignedRouteArray.count; a++) {
        
        CoordinatorRoutes *coorRoute;
        
        //It is already saved than don't update numberOfRounds Only RouteInfo and BusesArray
        if(([UserDefaults objectForKey:kCoordinatorRoutesWithBusesAndRounds]) && (_assignedRouteArray.count == savedRoutes.count))
        {
            coorRoute = [savedRoutes objectAtIndex:a];
        }
        else
        {
            coorRoute = [[CoordinatorRoutes alloc]init];
            coorRoute.numberOfRounds = [NSNumber numberWithInt:numberOfBusRounds];
        }
        coorRoute.routeInfo = [_assignedRouteArray objectAtIndex:a];
        
        
        for (NSDictionary *busDic in _busesArray) {
            
            if([[busDic objectForKey:krouteId] intValue] == [[coorRoute.routeInfo objectForKey:@"id"] intValue])
            {
                coorRoute.busesAssignedArray = [busDic objectForKey:kbtsbuses];
                break;
            }
        }

        [_routeArray addObject:coorRoute];
    }
    }
    [BTSData saveCoordinatorRoutesWithNumberOfRounds:_routeArray];
    DispatchEvent(@"UpdateBusListLoad", nil);
    [_routeTableView reloadData];
}

#pragma mark -
#pragma mark UITableView DataSource
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if([[[_userSelectedRole objectForKey:@"id"] stringValue] isEqualToString:kBTSCoordinatorID])
    {
        CoordinatorRoutes *coorRoute = [_routeArray objectAtIndex:section];
        
        
        CoordinatorHeaderView *CoorheaderView = [tableView dequeueReusableCellWithIdentifier:@"CoorHeaderCell"];
        [CoorheaderView.lblTopTitle setText:[coorRoute.routeInfo objectForKey:@"name"]];
        [CoorheaderView.lblRightTitle setText:[NSString stringWithFormat:@"عددالردود : "]];
        [CoorheaderView.lblLeftTitle setText:[NSString stringWithFormat:@"%d",[coorRoute.numberOfRounds intValue]]];
        [CoorheaderView.btnImage setBackgroundImage:[UIImage imageNamed:@"ic_addsharp"] forState:UIControlStateNormal];
        [CoorheaderView.btnImage addTarget:self action:@selector(addNumberOfRoundsTapped:) forControlEvents:UIControlEventTouchUpInside];
        [CoorheaderView.btnImage setTag:section];
        return CoorheaderView;
    }
    else
    {
        HeaderView *headerView = [tableView dequeueReusableCellWithIdentifier:@"HeaderCell"];
        [headerView setTitle:[[_assignedRouteArray objectAtIndex:section] objectForKey:@"name"]];
        return headerView;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if([[[_userSelectedRole objectForKey:@"id"] stringValue] isEqualToString:kBTSCoordinatorID])
        return 88;
    
    return 44;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _routeArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    TwoTitleCell *routeCell =[tableView dequeueReusableCellWithIdentifier:_routeCellIdentifier];
    [routeCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [routeCell.btnImage setBackgroundImage:[UIImage imageNamed:@"ic_addround"] forState:UIControlStateNormal];
    [routeCell.btnImage.superview setTag:indexPath.section];
    [routeCell.btnImage addTarget:self action:@selector(leftButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    if([[UserInformation getUserSelectedRoleId] isEqualToString:@"3"])
    {
        CoordinatorRoutes *coorObj = [_routeArray objectAtIndex:indexPath.section];
        routeCell.lblLeftTitle.text = [NSString stringWithFormat:@"%lu",(unsigned long)coorObj.busesAssignedArray.count];
        routeCell.lblRightTitle.text = [NSString stringWithFormat:@"عدد الإضافات : "];
    }
    else if([[UserInformation getUserSelectedRoleId] isEqualToString:@"2"])
    {
        NSArray *usersArray = [BTSData getAssignedUsersByManagerForSpecificRoute:
                                                 [[[_assignedRouteArray objectAtIndex:indexPath.section] objectForKey:@"id"] stringValue]];
        routeCell.lblLeftTitle.text = [NSString stringWithFormat:@"%lu",(unsigned long)usersArray.count];
        routeCell.lblRightTitle.text = [NSString stringWithFormat:@"عدد الإضافات : "];
        
        if([UserDefaults boolForKey:kisManagerInReports])
            [routeCell.btnImage setBackgroundImage:[UIImage imageNamed:@"ic_guides"] forState:UIControlStateNormal];
    }
    return routeCell;
}

#pragma mark - TableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark -
#pragma mark - IBActions

-(IBAction)leftButtonTapped:(UIButton *)sender
{
    [UserDefaults setObject:[_assignedRouteArray objectAtIndex:sender.superview.tag] forKey:kUserSelectedRoute];
    [UserDefaults synchronize];
    
    if([[UserInformation getUserSelectedRoleId] isEqualToString:kBTSManagerID])
    {
        if([UserDefaults boolForKey:kisManagerInReports])
        {
            AdminReportsViewController *objAdmin =[[AdminReportsViewController alloc]init];
            [self.navigationController pushViewController:objAdmin animated:YES];
        }
        else
        {
            TasksViewController *objTask =[[TasksViewController alloc]init];
            [self.navigationController pushViewController:objTask animated:YES];
        }
    }
    else if([[UserInformation getUserSelectedRoleId] isEqualToString:kBTSCoordinatorID])
    {
        CoordinatorRoutes * coorRoute =[_routeArray objectAtIndex:sender.superview.tag];
        //NSLog(@"selected route %@",coorRoute.busesAssignedArray);
        BusesListViewController *objBusesList =[[BusesListViewController alloc]initWithSelectedBusesOfRoute:coorRoute.busesAssignedArray];
        [self.navigationController pushViewController:objBusesList animated:YES];
    }

}

-(IBAction)addNumberOfRoundsTapped:(UIButton *)sender
{
    selectedRouteIndexForNumberOfRounds = sender.tag;
    AddBusesViewController *objAddRounds =[[AddBusesViewController alloc]initWithNumberOfRoundsView];
    [self.navigationController pushViewController:objAddRounds animated:YES];
}

@end
