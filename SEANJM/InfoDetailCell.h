//
//  InfoDetailCell.h
//  SEANJM
//
//  Created by Abdul Kareem on 9/20/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoDetailCell : UITableViewCell
@property (nonatomic,weak)IBOutlet UILabel *lblRightTitle;
@property (nonatomic,weak)IBOutlet UITextView *txtView;
@end
