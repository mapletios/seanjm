//
//  GuidersTaskListViewController.m
//  SEANJM
//
//  Created by Abdul Kareem on 9/8/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "GuidersTaskListViewController.h"

#import "TwoTitleCell.h"
#import "TaskCell.h"
#import "Macros.h"
#import "LanguageController.h"
#import "AFNetworking.h"
#import "Utility.h"
#import "UserInformation.h"
#import "MBProgressHUD.h"
#import "AddGuidersViewController.h"
#import "AFNetworking.h"
#import "ConfigurationManager.h"
#import "BTSData.h"

#define BTSAssignGuidersSerivice @"http://213.236.53.172/snbts/restserviceimpl.svc/assignGuidersToRoutes"
#define BTSRemoveGuidersFromRoute @"http://213.236.53.172/snbts/restserviceimpl.svc/removeGuidersFromSpecificRoute"

@interface GuidersTaskListViewController ()
{
    ConfigurationManager *config;
}
@property (nonatomic,weak)IBOutlet UITableView *tasksTableView;
@property (nonatomic)NSString *taskCellIdentifier;
@property (nonatomic)NSString *taskFooterIdentifier;
@property (nonatomic)NSMutableArray *oldTaskArray;
@property (nonatomic)NSMutableArray *taskArray;
@property (nonatomic)NSDictionary *userSelectedRole;
@property (nonatomic)NSDictionary *NewUserRoute;
@property (nonatomic)MBProgressHUD *hudview;
@property (nonatomic)NSInteger selectedIndexToEdit;
@property(nonatomic) NSMutableArray *sendUserArray;
@property(nonatomic) NSMutableArray *removingArray;

@end
@implementation GuidersTaskListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    _sendUserArray = [NSMutableArray array];
    _removingArray = [NSMutableArray array];
    config = [[ConfigurationManager alloc]init];
    
    _taskArray =[NSMutableArray array];
    _taskCellIdentifier = @"TwoTitleCell";
    _taskFooterIdentifier = @"TaskCell";
    
    [_tasksTableView registerNib:[UINib nibWithNibName:_taskCellIdentifier bundle:[NSBundle mainBundle]]forCellReuseIdentifier:_taskCellIdentifier];
    
    [_tasksTableView registerNib:[UINib nibWithNibName:_taskFooterIdentifier bundle:[NSBundle mainBundle]]forCellReuseIdentifier:_taskFooterIdentifier];
    
    AddObserver(self, @selector(UpdateUserGuidersListLoadFromService), @"UpdateUserGuidersListLoad", nil);
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    self.navigationController.navigationBar.topItem.title = [[UserInformation getUserSelectedRoute] objectForKey:@"name"];
    self.navigationController.navigationBar.backItem.title = LocalizedString(@"back");
    _NewUserRoute = [NSDictionary dictionary];
    UIBarButtonItem *btnNextButton =[[UIBarButtonItem alloc] initWithTitle:LocalizedString(@"send") style:UIBarButtonItemStylePlain target:self action:@selector(sendTapped:)];
    [self.navigationItem setRightBarButtonItem:btnNextButton];
    
    NSIndexPath *tableSelection = [_tasksTableView indexPathForSelectedRow];
    [_tasksTableView deselectRowAtIndexPath:tableSelection animated:YES];
    
    [self loadAllTasks];
}

-(void)viewDidDisappear:(BOOL)animated
{
    if(![self.navigationController.viewControllers containsObject:self])
    {
        RemoveObserver(@"UpdateUserGuidersListLoad");
    }
}


-(void)loadAllTasks
{
    /// Load from webservice ///
    //NSLog(@"user defaults %@",[UserDefaults dictionaryRepresentation]);
    if([UserDefaults objectForKey:kBTS_M_AssignedUsersAndGuides] && _taskArray.count ==0)
        _taskArray = (NSMutableArray *)[BTSData getAssignedGuidersByManagerForRoute];
    
    ////
    //Update Task Info
    if([[UserDefaults objectForKey:kUpdateGuider] count] >0)
    {
        _NewUserRoute = [UserDefaults objectForKey:kUpdateGuider];
        [_taskArray replaceObjectAtIndex:_selectedIndexToEdit withObject:_NewUserRoute];
        [UserDefaults removeObjectForKey:kUpdateGuider];
        [UserDefaults synchronize];
    }
    //Add New Task
    else if([[UserDefaults objectForKey:kAddnewGuider] count] >0)
    {
        
//        _NewUserRoute = [UserDefaults objectForKey:kAddnewGuider];
//        [_sendUserArray addObject:_NewUserRoute];
//        _taskArray = (NSMutableArray *)[_taskArray arrayByAddingObjectsFromArray:_sendUserArray];
//        [UserDefaults removeObjectForKey:kAddnewGuider];
//        [UserDefaults synchronize];
//        [_tasksTableView reloadData];
        
        
        _NewUserRoute = [UserDefaults objectForKey:kAddnewGuider];
        [_sendUserArray addObject:_NewUserRoute];
        [_taskArray addObject:_NewUserRoute];
        [UserDefaults removeObjectForKey:kAddnewGuider];
        [UserDefaults synchronize];
        [_tasksTableView reloadData];
        
    }
   
}

-(void)UpdateUserGuidersListLoadFromService
{
    if([UserDefaults objectForKey:kBTS_M_AssignedUsersAndGuides])
    {
        _taskArray = (NSMutableArray *)[BTSData getAssignedGuidersByManagerForRoute];
        [_tasksTableView reloadData];
    }
    
}

#pragma mark -
#pragma mark IBActions
-(IBAction)sendTapped:(id)sender
{
    if(![Utility checkInternetConnection])
        return;
    
    @autoreleasepool {
        [_sendUserArray removeAllObjects];
        for (NSMutableDictionary *castDic in _taskArray) {
            if( [castDic objectForKey:@"addItem"])
            {
                NSMutableDictionary *mutableDictionary = [castDic mutableCopy];
                [mutableDictionary removeObjectForKey:@"addItem"];
                [_sendUserArray addObject:mutableDictionary];
            }
        }
    }
    
    if(_sendUserArray.count >0)
    {
        NSString *serviceURL = [NSString stringWithFormat:@"%@",BTSAssignGuidersSerivice];
        [self sendRemoveRequest:serviceURL :_sendUserArray];
    }
    if(_removingArray.count >0)
    {
        NSString *serviceURL = [NSString stringWithFormat:@"%@",BTSRemoveGuidersFromRoute];
        [self sendRemoveRequest:serviceURL :_removingArray];
    }
    
    [_tasksTableView reloadData];
}

-(void)sendRemoveRequest:(NSString *)serviceURL :(NSMutableArray *)requestArray
{
    NSDictionary *sendingDictionary = @{@"guiders": requestArray};
    
    //   NSDictionary *sendingDictionary =[NSDictionary dictionaryWithObject:requestArray forKey:@"routesusers"];
    if(!_hudview)
    {
        _hudview = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        _hudview.mode = MBProgressHUDModeIndeterminate;
        _hudview.labelText = @"Syncing...";
        [_hudview setDimBackground:YES];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [manager POST:serviceURL parameters:sendingDictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            //NSLog(@"JSON: %@", responseObject);
            
            if(![responseObject  isKindOfClass:[NSNull class]])
            {
                if([responseObject objectForKey:@"Result"])
                {
                    
                    [self changeSavedStatusAfterUploadingSuccessfully];
                    [_hudview hide:YES];
                    [Utility showAlertViewWithTitle:LocalizedString(@"done")  message:LocalizedString(@"syncedsuccessfully") cancelButtonTile:LocalizedString(@"ok")];
                    
                    if([operation.request.URL.relativeString isEqualToString:BTSAssignGuidersSerivice])
                        [_sendUserArray removeAllObjects];
                    if([operation.request.URL.relativeString isEqualToString:BTSRemoveGuidersFromRoute])
                        [_removingArray removeAllObjects];
                    
                    DispatchEvent(@"loadWebServiceForManager", nil);
                    [self loadAllTasks];
                }
            }
            else {
                [Utility showAlertViewWithTitle:LocalizedString(@"error")  message:LocalizedString(@"error") cancelButtonTile:LocalizedString(@"ok")];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [Utility showAlertViewWithTitle:LocalizedString(@"error")  message:LocalizedString(@"error") cancelButtonTile:LocalizedString(@"ok")];
            //[activityIndicator stopAnimating];
            [_hudview hide:YES];
        }];
        
    });
}

-(void)changeSavedStatusAfterUploadingSuccessfully
{
    // NSArray *addItemArray = [_taskArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"addItem like '0'"]];
    
    NSMutableArray *finalArray =[NSMutableArray array];
    NSMutableDictionary *mutableDictionary;
    
    for (NSMutableDictionary *castDic in _taskArray) {
        mutableDictionary = [castDic mutableCopy];
        if( [castDic objectForKey:@"addItem"])
        {
            [mutableDictionary removeObjectForKey:@"addItem"];
        }
        if( [castDic objectForKey:@"deleteItem"])
        {
            [mutableDictionary removeObjectForKey:@"deleteItem"];
        }
        if(mutableDictionary.count>0)
            [finalArray addObject:mutableDictionary];
    }
    _taskArray = finalArray;
    
    [_tasksTableView reloadData];
    
}

#pragma mark -
#pragma mark UITableView DataSource


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _taskArray.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section;
{
    
    TaskCell *footerView = [tableView dequeueReusableCellWithIdentifier:_taskFooterIdentifier];
    footerView.lblTitle.text = LocalizedString(@"addNewGuider");
    [footerView.btnImage setBackgroundImage:[UIImage imageNamed:@"ic_addround"] forState:UIControlStateNormal];
    [footerView.btnImage setTag:section];
    [footerView.lblTitle setFont:[UIFont fontWithName:HELVETICA_BOLD size:14.0]];
    [footerView setBackgroundColor:[UIColor lightGrayColor]];
    [footerView.contentView setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    [footerView.btnImage addTarget:self action:@selector(leftFooterButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    return footerView.contentView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TwoTitleCell *taskCell =[tableView dequeueReusableCellWithIdentifier:_taskCellIdentifier forIndexPath:indexPath];
    
    taskCell.lblRightTitle.text = [NSString stringWithFormat:@"%@ ",[[_taskArray objectAtIndex:indexPath.row] objectForKey:@"name"]];
    NSString *stringForLeftLabel =[NSString stringWithFormat:@"%@ ",[[_taskArray objectAtIndex:indexPath.row] objectForKey:@"mobile"]];
    
    taskCell.lblLeftTitle.text = stringForLeftLabel;
    [taskCell.btnImage setContentMode:UIViewContentModeScaleAspectFit];
    [taskCell.btnImage setBackgroundImage:[UIImage imageNamed:@"ic_editing"] forState:UIControlStateNormal];
    [taskCell.btnImage.superview setTag:indexPath.row];
    [taskCell.btnImage addTarget:self action:@selector(leftButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [taskCell.btnImage setHidden:YES];
    [taskCell.btnImage setUserInteractionEnabled:NO];
    
    [taskCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [taskCell setBackgroundColor:[UIColor whiteColor]];
    
    if(_removingArray.count >0)
    {
        if([[_taskArray objectAtIndex:indexPath.row] objectForKey:@"deleteItem"])
            [taskCell setBackgroundColor:LightRedColor];
    }
    
    if(_NewUserRoute.count >0){
        if([[_taskArray objectAtIndex:indexPath.row] objectForKey:@"addItem"])
        {
            [taskCell setBackgroundColor:LightYellowColor];
        }
    }
    return taskCell;
}

#pragma mark - TableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section;
{
    return 44.0;
}

#pragma mark TABELVIEW EDITING

-(NSString *)tableView:(UITableView *)tableView titleForSwipeAccessoryButtonForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return @"Undo";
}

-(void)tableView:(UITableView *)tableView swipeAccessoryButtonPushedForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    [_removingArray removeObject:[_taskArray objectAtIndex:indexPath.row]];
    [[tableView cellForRowAtIndexPath:indexPath] setBackgroundColor:[UIColor whiteColor]];
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
        
        [tableView beginUpdates];
        NSMutableArray *tempArray = [NSMutableArray arrayWithArray:[_taskArray mutableCopy]];
        if(![_removingArray containsObject:[_taskArray objectAtIndex:indexPath.row]])
            [_removingArray addObject:[_taskArray objectAtIndex:indexPath.row]];
        
        
        if([_sendUserArray containsObject:[_taskArray objectAtIndex:indexPath.row]])
        {
            [_sendUserArray removeObject:[_taskArray objectAtIndex:indexPath.row]];
            [_removingArray removeObject:[_taskArray objectAtIndex:indexPath.row]];
            [tempArray removeObject:[_taskArray objectAtIndex:indexPath.row]];
            
        }
        [[tableView cellForRowAtIndexPath:indexPath] setBackgroundColor:LightRedColor];

        NSMutableDictionary *deleteDic = [NSMutableDictionary dictionaryWithDictionary:[_taskArray objectAtIndex:indexPath.row]];
        [deleteDic setObject:@"delete" forKey:@"deleteItem"];
        [_taskArray replaceObjectAtIndex:indexPath.row withObject:deleteDic];
        [tableView endUpdates];
        
        //_taskArray = tempArray;
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}


#pragma mark -
#pragma mark - IBActions
-(IBAction)leftButtonPressed:(UIButton *)sender
{
    _selectedIndexToEdit = sender.tag;
    AddGuidersViewController *objAdduser =[[AddGuidersViewController alloc]initWithSelectedTask:[_taskArray objectAtIndex:sender.superview.tag]];
    [self.navigationController pushViewController:objAdduser animated:YES];
}

//Add new task
-(IBAction)leftFooterButtonTapped:(UIButton *)sender
{
    AddGuidersViewController *objAdduser =[[AddGuidersViewController alloc]init];
    [self.navigationController pushViewController:objAdduser animated:YES];
}

@end
