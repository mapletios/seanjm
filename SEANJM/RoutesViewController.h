//
//  RoutesViewController.h
//  SEANJM
//
//  Created by Abdul Kareem on 9/2/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoutesViewController : UIViewController
-(id)initWithRole :(NSDictionary *)selected_role;
@end
