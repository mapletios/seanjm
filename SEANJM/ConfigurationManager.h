//
//  ConfigurationManager.h
//  SEANJM
//
//  Created by Randa Al-Sadek on 5/24/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConfigurationManager : NSObject 


/*********************BTS Configuration *********************************/
- (void)LoadGroupsForSpecifRoute: (NSString *)serviceURL completion:(void (^)(void)) callback_  failure:(void (^)(void))callbackFail_;
- (void)LoadBTSSAReportsForAllRoutes: (NSString *)serviceURL completion:(void (^)(void)) callback_  failure:(void (^)(void))callbackFail_;
- (void)LoadBTSConfiguration: (NSString *)serviceURL completion:(void (^)(void)) callback_  failure:(void (^)(void))callbackFail_;
/***********************TCS Configuration Methods************************/
- (void) loadTCSConfiguration: (void (^)(void)) callback_ failure:(void (^)(void)) callbackFail_;
//- (void) loadAllCategories:(void (^)(void)) callback_ failure:(void (^)(void)) callbackFail_;
- (NSArray *)getCategoriesNames;
- (NSArray *)getSubCategoriesNames:(NSString *)categoryName;
- (NSDictionary *)getCategoryObjectByName: (NSString *)categoryName;
- (NSDictionary *)getCategoryObjectById: (NSNumber *)categoryId;
- (NSDictionary *)getSubCategoryObjectByName: (NSString *)subCategoryName;
- (NSDictionary *)getSubCategoryObjectById: (NSNumber *)subCategoryId;
- (void)sendAllFaults: (NSArray *)allFaults completion:(void (^)(void)) callback_ failure:(void (^)(void)) callbackFail_;
- (void)loadFaultsForTent: (NSString *) tentNum completion:(void (^)(void)) callback_ failure:(void (^) (void))callbackFail_;
- (NSArray *)getOldFaults;
- (NSString *) getCurrentDate;

/***********************SRS Configuration Methods************************/

- (void) loadSRSConfiguration: (void (^)(void)) callback_ failure:(void (^)(void)) callbackFail_;
- (void)sendMaintenanceComplaint: (NSDictionary *)complaintDic completion:(void (^)(void)) callback_  failure:(void (^)(void))callbackFail_;
- (void)sendTransportationComplaint: (NSDictionary *)complaintDic completion:(void (^)(void)) callback_  failure:(void (^)(void))callbackFail_;
- (void)sendPublicAffairsComplaint: (NSDictionary *)complaintDic completion:(void (^)(void)) callback_  failure:(void (^)(void))callbackFail_;
- (void)sendDeathComplaint: (NSDictionary *)complaintDic completion:(void (^)(void)) callback_  failure:(void (^)(void))callbackFail_;
- (void)deleteComplaint: (int)complaintId complaintType: (int)complaintType completion:(void (^)(void)) callback_  failure:(void (^)(void))callbackFail_;
- (void)updateComplaintStatus: (int)complaintId complaintType: (int)complaintType status:(int)status completion:(void (^)(void)) callback_  failure:(void (^)(void))callbackFail_;

- (NSArray *)getMaintenanceCategories;
- (NSArray *) getSubMaintenanceNames:(NSString *)categoryName;
- (NSDictionary *)getMaintenanceTypeByName: (NSString *)categoryName;
- (NSString *)getMaintenanceTypeById: (NSNumber *)mainId type:(NSString *)type;
//- (NSString *)getTransportationTypeById: (NSNumber *)mainId;
- (NSDictionary *)getMaintenanceSubTypeByName: (NSString *)subCategoryName;
- (NSArray *) getRegions;
- (NSArray *) getTransportationSubTypes: (id)categoryId;
- (NSDictionary *)getTransSubTypeByName: (NSString *)subCategoryName;
- (NSArray *) getBusCompanieaNames;
- (NSDictionary *)getBusObjectByName: (NSString *)companyName;
- (NSString *) getDateFromSeconds: (double)seconds;

@property (nonatomic)NSNumber *currentLocationLat;
@property (nonatomic)NSNumber *currentLocationLon;
@end
