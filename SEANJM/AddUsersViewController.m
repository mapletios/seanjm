//
//  AddUsersViewController.m
//  SEANJM
//
//  Created by Abdul Kareem on 9/5/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "AddUsersViewController.h"
#import "AddUserCell.h"
#import "ConfirmationCell.h"
#import "Macros.h"
#import "LanguageController.h"
#import "AFNetworking.h"
#import "Utility.h"
#import "UserInformation.h"
#import "MBProgressHUD.h"
#import "ActionSheetStringPicker.h"
#import "BTSData.h"

@interface AddUsersViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    ConfirmationCell *footerView;
    NSInteger numberOfRows;
}
@property (nonatomic,weak)IBOutlet UITableView *addUserTableView;
@property (nonatomic)NSString       *addUserCellIdentifier;
@property (nonatomic)NSString       *addUserFooterIdentifier;
@property (nonatomic)NSMutableArray *addedUserArray;
@property (nonatomic)NSMutableArray *allUserArray;
@property (nonatomic)NSInteger selectedIndex;
@property (nonatomic)NSMutableDictionary *userDic;
@property (nonatomic)NSMutableArray *rowsPlaceholder;
@property (nonatomic)NSDictionary   *selectedTask;
@property (nonatomic)NSDictionary   *NewUserRoute;
@property (nonatomic)NSMutableArray *inheritedUsersArray;
@end

@implementation AddUsersViewController

-(id)initWithSelectedTask :(NSDictionary *)taskdictionary
{
    self =[super init];
    _selectedTask = [NSDictionary dictionaryWithDictionary:taskdictionary];
    
    return self;
}

-(id)initWithUserArray :(NSMutableArray *)user_array
{
    self = [super init];
    _inheritedUsersArray = user_array;
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    if(_selectedTask)
        _userDic = [NSMutableDictionary dictionaryWithDictionary:_selectedTask];
    else
        _userDic = [NSMutableDictionary dictionary];
    
    _addedUserArray =[NSMutableArray array];
    _rowsPlaceholder =[NSMutableArray arrayWithObjects:@"selectUser",@"selectRole",@"selectLocation", nil];
    numberOfRows = 2;
    _addUserCellIdentifier   = @"AddUserCell";
    _addUserFooterIdentifier = @"ConfirmationCell";
    
    [_addUserTableView registerNib:[UINib nibWithNibName:_addUserCellIdentifier bundle:[NSBundle mainBundle]]forCellReuseIdentifier:_addUserCellIdentifier];
    
    [_addUserTableView registerNib:[UINib nibWithNibName:_addUserFooterIdentifier bundle:[NSBundle mainBundle]]forCellReuseIdentifier:_addUserFooterIdentifier];
    
    
    [self loadUsers];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.topItem.title = [[UserInformation getUserSelectedRoute] objectForKey:@"name"];
    self.navigationController.navigationBar.backItem.title = LocalizedString(@"back");
    
    NSIndexPath *tableSelection = [_addUserTableView indexPathForSelectedRow];
    [_addUserTableView deselectRowAtIndexPath:tableSelection animated:YES];
    //self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}

-(void)loadUsers
{
    _allUserArray = [NSMutableArray arrayWithArray:[BTSData getUsers]];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark IBActions
-(IBAction)sendTapped:(id)sender
{
    
}

-(BOOL)checkInputValuesValidation
{
    if(numberOfRows==2)
    {
        if([_userDic objectForKey:kuserId] && [_userDic objectForKey:kroleId])
            return YES;
    }
    else if (numberOfRows ==3)
    {
        if([_userDic objectForKey:kuserId] && [_userDic objectForKey:kroleId] && (![[_userDic objectForKey:klocationId] isEqualToString:@"-1"]))
            return YES;
    }
    
    return NO;
}

#pragma mark -
#pragma mark UITableView DataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([_userDic objectForKey:klocationId] && (![[_userDic objectForKey:klocationId] isEqualToString:@"-1"]))
        numberOfRows = 3;
    
    return numberOfRows;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section;
{
    footerView = [tableView dequeueReusableCellWithIdentifier:_addUserFooterIdentifier];
    [footerView.btnAdd addTarget:self action:@selector(addButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [footerView.btnCancel addTarget:self action:@selector(cancelButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [footerView.btnAdd setTitle:LocalizedString(@"add") forState:UIControlStateNormal];
    [footerView.btnCancel setTitle:LocalizedString(@"cancel") forState:UIControlStateNormal];
    
    [footerView.btnAdd.layer setCornerRadius:4.0];
    [footerView.btnAdd setClipsToBounds:YES];
    [footerView.btnCancel.layer setCornerRadius:4.0];
    [footerView.btnCancel setClipsToBounds:YES];
    
    if([self checkInputValuesValidation])
    {
        [footerView.btnAdd setUserInteractionEnabled:YES];
        [footerView.btnAdd setHighlighted:NO];
        if(_selectedTask.count > 0)
            [footerView.btnAdd setTitle:LocalizedString(@"Update") forState:UIControlStateNormal];
    }
    else
    {
        [footerView.btnAdd setUserInteractionEnabled:NO];
        [footerView.btnAdd setHighlighted:YES];
    }
    return footerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    AddUserCell *addUserCell =[tableView dequeueReusableCellWithIdentifier:_addUserCellIdentifier];
    addUserCell.txtUserName.delegate    = self;
    [addUserCell.txtUserDetail setHidden:YES];
    [addUserCell.txtUserName setPlaceholder:LocalizedString([_rowsPlaceholder objectAtIndex:indexPath.row])];
    addUserCell.txtUserName.tag = indexPath.row;
    [addUserCell setEditing:YES];
    
    //This means user selected old task to update
    if(_selectedTask.count > 0)
    {
        if(indexPath.row ==0)
            [addUserCell.txtUserName setText:[_userDic objectForKey:@"name"]];
        else if(indexPath.row ==1)
            [addUserCell.txtUserName setText:LocalizedString([BTSData getRoleNameWithRoleId:[_userDic objectForKey:kroleId]])];
        else if(indexPath.row ==2)
            [addUserCell.txtUserName setText:[_userDic objectForKey:@"locationName"]];
    }
    
    return addUserCell;
}

#pragma mark - TableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section;
{
    return 80.0;
}

#pragma mark -
#pragma mark - IBActions

-(IBAction)addButtonTapped:(UIButton *)sender
{
    [_userDic setObject:[[UserInformation getUserSelectedRoute] objectForKey:@"id"] forKey:krouteId];
    [_userDic setObject:[Utility getcurrentDateOnly] forKey:@"creationDate"];
    [_userDic setObject:[UserInformation getUserId] forKey:kcreator];
    [_userDic setObject:[UserInformation getGroupID] forKey:kgroupId];
    [_userDic setObject:@"0" forKey:@"addItem"];
    
    //[_addedUserArray addObject:_userDic];
    
    
    //Update User
    if(_selectedTask)
    {
        [UserDefaults setObject:_userDic forKey:kUpdateUser];
        [UserDefaults synchronize];
    }
    //Add new user
    else
    {
        NSArray *duplicateValidationkeys =[NSArray arrayWithObjects:
                                           kuserId,
                                           kroleId,
                                           nil];
        BOOL isItemUnique = false;
        //Checking This item is already added OR not
        
        if(_inheritedUsersArray.count >0)
        {
            NSArray *filteredArray = [_inheritedUsersArray filteredArrayUsingPredicate:
                                     [NSPredicate predicateWithFormat:@"roleId == %@ && userId == %@",
                                      [_userDic objectForKey:kroleId],[_userDic objectForKey:kuserId]]];
            
            
            if(filteredArray.count == 0)
                isItemUnique = YES;
//            for (NSString *keyString in duplicateValidationkeys) {
//                
//                if(![[_inheritedUsersArray valueForKey:keyString] containsObject:[_userDic objectForKey:keyString]] &&
//                   [[_inheritedUsersArray valueForKey:keyString] containsObject:[_userDic objectForKey:keyString]])
//                {
//                    isItemUnique = YES;
//                    break;
//                }
//            }
        }
        if(isItemUnique || _inheritedUsersArray.count == 0)
        {
            [UserDefaults setObject:_userDic forKey:kAddnewUser];
            [UserDefaults synchronize];
            [self.navigationController popViewControllerAnimated:YES];
            return;
        }
        else
        {
            [Utility showAlertViewWithTitle:@"" message:LocalizedString(@"alreadyAdded") cancelButtonTile:LocalizedString(@"ok")];
        }
        
        
//        [UserDefaults setObject:_userDic forKey:kAddnewUser];
//        [UserDefaults synchronize];
    }

    //[self.navigationController popViewControllerAnimated:YES];
    
    //NSLog(@"addind dictionary %@",_userDic);
}

-(IBAction)cancelButtonTapped:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITextField Delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.view endEditing:YES];
    NSArray *itemsArray;
    
    if(textField.tag == 0)
        itemsArray = [NSArray arrayWithArray:[_allUserArray valueForKey:@"name"]];
    else if(textField.tag == 1)
        itemsArray = [NSArray arrayWithArray:[[BTSData getRoles] valueForKey:@"name"]];
    else
        itemsArray = [NSArray arrayWithArray:[[BTSData getLocations] valueForKey:@"name"]];
    
    [ActionSheetStringPicker showPickerWithTitle:LocalizedString(@"select") rows:itemsArray initialSelection:_selectedIndex doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        [textField setText:selectedValue];
        
        //User Name
        if(textField.tag == 0)
        {
            [_userDic setObject:[[_allUserArray objectAtIndex:selectedIndex] objectForKey:kuserId] forKey:kuserId];
            [_userDic setObject:[[_allUserArray objectAtIndex:selectedIndex] objectForKey:@"name"] forKey:@"name"];
            [_userDic setObject:@"-1" forKey:klocationId];
            [_userDic setObject:@"" forKey:@"locationName"];
        }
        //User Role
        else if(textField.tag ==1)
        {
            [_userDic setObject:[[[BTSData getRoles] objectAtIndex:selectedIndex] objectForKey:@"id"] forKey:kroleId];
            [_userDic setObject:[[[BTSData getRoles] objectAtIndex:selectedIndex] objectForKey:@"name"] forKey:@"roleName"];
            [_userDic setObject:@"-1" forKey:klocationId];
            [_userDic setObject:@"" forKey:@"locationName"];
            //Select Sender OR Receiver
            if(selectedIndex == 0 || selectedIndex ==1)
            {
                numberOfRows = 3;
            }
            else
                numberOfRows = 2;
        }
        //Location
        else if(textField.tag ==2)
        {
            [_userDic setObject:[[[[BTSData getLocations] objectAtIndex:selectedIndex] objectForKey:@"id"] stringValue] forKey:klocationId];
            [_userDic setObject:[[[BTSData getLocations] objectAtIndex:selectedIndex] objectForKey:@"name"] forKey:@"locationName"];
        }
        
        
        [_addUserTableView reloadData];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        //[textField setText:@""];
    } origin:textField];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    return YES;
}

@end
