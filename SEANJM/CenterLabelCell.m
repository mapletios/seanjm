//
//  CenterLabelCell.m
//  SEANJM
//
//  Created by Abdul Kareem on 9/20/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "CenterLabelCell.h"

@implementation CenterLabelCell
@synthesize lblTitle;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)setTitle:(NSString *)title
{
    [self.lblTitle setText:title];
}

@end
