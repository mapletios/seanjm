//
//  TaskListViewController.m
//  SEANJM
//
//  Created by Abdul Kareem on 9/5/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "TaskListViewController.h"
#import "TwoTitleCell.h"
#import "TaskCell.h"
#import "Macros.h"
#import "LanguageController.h"
#import "AFNetworking.h"
#import "Utility.h"
#import "UserInformation.h"
#import "MBProgressHUD.h"
#import "AddUsersViewController.h"
#import "AFNetworking.h"
#import "ConfigurationManager.h"
#import "BTSData.h"

#define BTSAssignUserSerivice @"http://213.236.53.172/snbts/restserviceimpl.svc/assignUsersToRoutes"
#define BTSRemoveUserFromRoute @"http://213.236.53.172/snbts/restserviceimpl.svc/removeUsersFromSpecificRoute"

@interface TaskListViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    ConfigurationManager *config;
}
@property (nonatomic,weak)IBOutlet UITableView *tasksTableView;
@property (nonatomic)NSString *taskCellIdentifier;
@property (nonatomic)NSString *taskFooterIdentifier;
@property (nonatomic)NSMutableArray *oldTaskArray;
@property (nonatomic)NSMutableArray *taskArray;
@property (nonatomic)NSDictionary *userSelectedRole;
@property (nonatomic)NSDictionary *NewUserRoute;
@property (nonatomic)MBProgressHUD *hudview;
@property (nonatomic)NSInteger selectedIndexToEdit;
@property(nonatomic) NSMutableArray *sendUserArray;
@property(nonatomic) NSMutableArray *removingArray;
@end

@implementation TaskListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _sendUserArray = [NSMutableArray array];
    _removingArray = [NSMutableArray array];
    config = [[ConfigurationManager alloc]init];
    
    _taskArray =[NSMutableArray array];
    _taskCellIdentifier = @"TwoTitleCell";
    _taskFooterIdentifier = @"TaskCell";
    
    [_tasksTableView registerNib:[UINib nibWithNibName:_taskCellIdentifier bundle:[NSBundle mainBundle]]forCellReuseIdentifier:_taskCellIdentifier];

    [_tasksTableView registerNib:[UINib nibWithNibName:_taskFooterIdentifier bundle:[NSBundle mainBundle]]forCellReuseIdentifier:_taskFooterIdentifier];
    
    AddObserver(self, @selector(UpdateUserGuidersListLoadFromService), @"UpdateUserGuidersListLoad", nil);
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController.navigationBar.topItem setTitle:@""];
    _NewUserRoute = [NSDictionary dictionary];
    UIBarButtonItem *btnNextButton =[[UIBarButtonItem alloc] initWithTitle:LocalizedString(@"send") style:UIBarButtonItemStylePlain target:self action:@selector(sendTapped:)];
    [self.navigationItem setRightBarButtonItem:btnNextButton];
    
//    NSIndexPath *tableSelection = [_tasksTableView indexPathForSelectedRow];
//    [_tasksTableView deselectRowAtIndexPath:tableSelection animated:YES];
    [self loadAllTasks];
}

-(void)viewDidDisappear:(BOOL)animated
{
    if(![self.navigationController.viewControllers containsObject:self])
    {
        RemoveObserver(@"UpdateUserGuidersListLoad");
    }
}

-(void)loadAllTasks
{
    /// Load from webservice ///

    if([UserDefaults objectForKey:kBTS_M_AssignedUsersAndGuides] && _taskArray.count ==0)
        _taskArray = (NSMutableArray *)[BTSData getAssignedUsersByManagerForRoute];
    
    ////
    //Update Task Info
    if([[UserDefaults objectForKey:kUpdateUser] count] >0)
    {
        _NewUserRoute = [UserDefaults objectForKey:kUpdateUser];
        [_taskArray replaceObjectAtIndex:_selectedIndexToEdit withObject:_NewUserRoute];
        [UserDefaults removeObjectForKey:kUpdateUser];
        [UserDefaults synchronize];
    }
    //Add New Task
   else if([[UserDefaults objectForKey:kAddnewUser] count] >0)
    {
        _NewUserRoute = [UserDefaults objectForKey:kAddnewUser];
        [_sendUserArray addObject:_NewUserRoute];
        [_taskArray addObject:_NewUserRoute];
        [UserDefaults removeObjectForKey:kAddnewUser];
        [UserDefaults synchronize];
        [_tasksTableView reloadData];
    }

}

-(void)UpdateUserGuidersListLoadFromService
{
    if([UserDefaults objectForKey:kBTS_M_AssignedUsersAndGuides])
    {
        _taskArray = (NSMutableArray *)[BTSData getAssignedUsersByManagerForRoute];
        [_tasksTableView reloadData];
    }
    
}

#pragma mark -
#pragma mark IBActions
-(IBAction)sendTapped:(id)sender
{
    if(![Utility checkInternetConnection])
        return;
    
    @autoreleasepool {
        [_sendUserArray removeAllObjects];
        for (NSMutableDictionary *castDic in _taskArray) {
            if( [castDic objectForKey:@"addItem"])
            {
                NSMutableDictionary *mutableDictionary = [castDic mutableCopy];
                [mutableDictionary removeObjectForKey:@"addItem"];
                [_sendUserArray addObject:mutableDictionary];
            }
        }
    }
    
    if(_sendUserArray.count >0)
    {
        NSString *serviceURL = [NSString stringWithFormat:@"%@",BTSAssignUserSerivice];
        [self addDeleteRequest:serviceURL :_sendUserArray];
    }
    if(_removingArray.count >0)
    {
        NSString *serviceURL = [NSString stringWithFormat:@"%@",BTSRemoveUserFromRoute];
        [self addDeleteRequest:serviceURL :_removingArray];
    }
}

-(void)addDeleteRequest:(NSString *)serviceURL :(NSMutableArray *)requestArray
{
    NSDictionary *sendingDictionary = @{@"routesusers": requestArray};

    if(!_hudview)
    {
        _hudview = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        _hudview.mode = MBProgressHUDModeIndeterminate;
        _hudview.labelText = @"Syncing...";
        [_hudview setDimBackground:YES];
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager POST:serviceURL parameters:sendingDictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {

        if(![responseObject  isKindOfClass:[NSNull class]])
        {
            if([[responseObject objectForKey:@"Result"] intValue] == 1)
            {
                [self changeSavedStatusAfterUploadingSuccessfully];
                [_hudview hide:YES];
                [Utility showAlertViewWithTitle:LocalizedString(@"done")  message:LocalizedString(@"syncedsuccessfully") cancelButtonTile:LocalizedString(@"ok")];
                
                if([operation.request.URL.relativeString isEqualToString:BTSAssignUserSerivice])
                    [_sendUserArray removeAllObjects];
                if([operation.request.URL.relativeString isEqualToString:BTSRemoveUserFromRoute])
                    [_removingArray removeAllObjects];
                
                //[_taskArray removeObjectsInArray:requestArray];
                DispatchEvent(@"loadWebServiceForManager", nil);
                [self loadAllTasks];
            }
            else
            {
                [_hudview hide:YES];
                
            [Utility showAlertViewWithTitle:LocalizedString(@"error")  message:LocalizedString(@"error") cancelButtonTile:LocalizedString(@"ok")];
                
            }
            
        }
        else {
            [_hudview hide:YES];
            [Utility showAlertViewWithTitle:LocalizedString(@"error")  message:LocalizedString(@"error") cancelButtonTile:LocalizedString(@"ok")];
        }
        _hudview =nil;
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [Utility showAlertViewWithTitle:LocalizedString(@"error")  message:LocalizedString(@"error") cancelButtonTile:LocalizedString(@"ok")];
        //[activityIndicator stopAnimating];
        [_hudview hide:YES];
        _hudview =nil;
    }];
}

-(void)changeSavedStatusAfterUploadingSuccessfully
{
   
    
    NSMutableArray *finalArray =[NSMutableArray array];
    NSMutableDictionary *mutableDictionary;
    
    for (NSMutableDictionary *castDic in _taskArray) {
        mutableDictionary = [castDic mutableCopy];
        if( [castDic objectForKey:@"addItem"])
        {
            [mutableDictionary removeObjectForKey:@"addItem"];
        }
        if( [castDic objectForKey:@"deleteItem"])
        {
            [mutableDictionary removeObjectForKey:@"deleteItem"];
        }
        if(mutableDictionary.count>0)
            [finalArray addObject:mutableDictionary];
    }
    _taskArray = finalArray;
    
    [_tasksTableView reloadData];
   
}

#pragma mark -
#pragma mark UITableView DataSource


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _taskArray.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section;
{
    
    TaskCell *footerView = [tableView dequeueReusableCellWithIdentifier:_taskFooterIdentifier];
    footerView.lblTitle.text = LocalizedString(@"addNewUser");
    [footerView.btnImage setBackgroundImage:[UIImage imageNamed:@"ic_addround"] forState:UIControlStateNormal];
    [footerView.btnImage setTag:section];
    [footerView.lblTitle setFont:[UIFont fontWithName:HELVETICA_BOLD size:14.0]];
    [footerView setBackgroundColor:[UIColor lightGrayColor]];
    [footerView.contentView setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    [footerView.btnImage addTarget:self action:@selector(leftFooterButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    return footerView.contentView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TwoTitleCell *taskCell =[tableView dequeueReusableCellWithIdentifier:_taskCellIdentifier forIndexPath:indexPath];

    taskCell.lblRightTitle.text = [NSString stringWithFormat:@"%@: ",[[_taskArray objectAtIndex:indexPath.row] objectForKey:@"name"]];
    NSString *stringForLeftLabel =[NSString stringWithFormat:@"%@ : %@",LocalizedString([BTSData getRoleNameWithRoleId:[[_taskArray objectAtIndex:indexPath.row] objectForKey:@"roleId"]]),[[_taskArray objectAtIndex:indexPath.row] objectForKey:@"locationName"]];
                                                                                     
    taskCell.lblLeftTitle.text = stringForLeftLabel;
    [taskCell.btnImage setContentMode:UIViewContentModeScaleAspectFit];
    [taskCell.btnImage setBackgroundImage:[UIImage imageNamed:@"ic_editing"] forState:UIControlStateNormal];
    [taskCell.btnImage.superview setTag:indexPath.row];
    [taskCell.btnImage addTarget:self action:@selector(leftButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [taskCell.btnImage setHidden:YES];
    [taskCell.btnImage setUserInteractionEnabled:NO];
    
    [taskCell setBackgroundColor:[UIColor whiteColor]];
    
    if(_removingArray.count >0)
    {
        if([[_taskArray objectAtIndex:indexPath.row] objectForKey:@"deleteItem"])
            [taskCell setBackgroundColor:LightRedColor];
    }
    
    if(_NewUserRoute.count >0){
        if([[_taskArray objectAtIndex:indexPath.row] objectForKey:@"addItem"])
        {
            [taskCell setBackgroundColor:LightYellowColor];
        }
    }
    return taskCell;
}

#pragma mark - TableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section;
{
    return 44.0;
}

#pragma mark TABELVIEW EDITING

-(NSString *)tableView:(UITableView *)tableView titleForSwipeAccessoryButtonForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return @"Undo";
}

-(void)tableView:(UITableView *)tableView swipeAccessoryButtonPushedForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    [_removingArray removeObject:[_taskArray objectAtIndex:indexPath.row]];
    [[tableView cellForRowAtIndexPath:indexPath] setBackgroundColor:[UIColor whiteColor]];
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
        
        [tableView beginUpdates];
        NSMutableArray *tempArray = [NSMutableArray arrayWithArray:[_taskArray mutableCopy]];
        
        if(![_removingArray containsObject:[_taskArray objectAtIndex:indexPath.row]])
            [_removingArray addObject:[_taskArray objectAtIndex:indexPath.row]];
       
        if([_sendUserArray containsObject:[_taskArray objectAtIndex:indexPath.row]])
        {
            [_sendUserArray removeObject:[_taskArray objectAtIndex:indexPath.row]];
            [_removingArray removeObject:[_taskArray objectAtIndex:indexPath.row]];
            [tempArray removeObject:[_taskArray objectAtIndex:indexPath.row]];           
        }

        NSMutableDictionary *deleteDic = [NSMutableDictionary dictionaryWithDictionary:[_taskArray objectAtIndex:indexPath.row]];
        [deleteDic setObject:@"delete" forKey:@"deleteItem"];
        
        [_taskArray replaceObjectAtIndex:indexPath.row withObject:deleteDic];
        
        [[tableView cellForRowAtIndexPath:indexPath] setBackgroundColor:LightRedColor];

        [tableView endUpdates];
        
        //_taskArray = tempArray;
        
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}


#pragma mark -
#pragma mark - IBActions
-(IBAction)leftButtonPressed:(UIButton *)sender
{
    _selectedIndexToEdit = sender.tag;
    AddUsersViewController *objAdduser =[[AddUsersViewController alloc]initWithSelectedTask:[_taskArray objectAtIndex:sender.superview.tag]];
    [self.navigationController pushViewController:objAdduser animated:YES];
}
//Add new task
-(IBAction)leftFooterButtonTapped:(UIButton *)sender
{
    AddUsersViewController *objAdduser =[[AddUsersViewController alloc]initWithUserArray:_taskArray];
    [self.navigationController pushViewController:objAdduser animated:YES];
}

@end
