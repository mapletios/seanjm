//
//  ComplainsListViewController.m
//  SEANJM
//
//  Created by Randa Al-Sadek on 9/8/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "ComplainsListViewController.h"
#import "ComplainStatusViewController.h"
#import "CustomTableViewCell.h"
#import "ConfigurationManager.h"
#import "LanguageController.h"
#import "Macros.h"
#import "Utility.h"
#import <ArcGIS/ArcGIS.h>

@interface ComplainsListViewController ()<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, UISearchBarDelegate> {
    
    __weak IBOutlet UITableView *complainsTableView;
    __weak IBOutlet UIActivityIndicatorView *activityIndicator;
    __weak IBOutlet UISearchBar *complainSearchBar;
    NSString *cellIdentifier;
    int complaintType;
    int complaintId;
    NSUInteger rowToDelete;
    ConfigurationManager *config;
    NSMutableArray *complainsSearchData;
    NSMutableArray *complainsData;
}

@end

@implementation ComplainsListViewController

@synthesize complainsArray, complainLayer;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithTitle:LocalizedString(@"back")
                                                                 style:UIBarButtonItemStylePlain
                                                                target:nil
                                                                action:nil];
    
    [self.navigationItem setBackBarButtonItem:backItem];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTable:) name:@"ReloadTable" object:nil];
    
    config = [[ConfigurationManager alloc]init];
   
    [self loadArrayFromGraphics:complainsArray];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [complainsSearchData count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CustomTableViewCell *cell = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }

    NSDictionary *complain = [complainsSearchData objectAtIndex:indexPath.row];
    
    cell.lbl1.text = [NSString stringWithFormat:@"%@: %@", LocalizedString(@"complainNumber"), [complain objectForKey:@"id"]];
    cell.lbl2.text = [NSString stringWithFormat:@"%@: %@",LocalizedString(@"complainType"), [complain objectForKey:@"mainType"]];
    cell.lbl3.text = [NSString stringWithFormat:@"%@: %@",LocalizedString(@"complainStatus"), [complain objectForKey:@"status"]];
    cell.lbl4.text = [NSString stringWithFormat:@"%@: %@",LocalizedString(@"complainDate"), [complain objectForKey:@"date"]];

    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    NSDictionary *selectedFault = [allFaults objectAtIndex:indexPath.row];
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
       
        NSDictionary *complain = [complainsSearchData objectAtIndex:indexPath.row];
        complaintId = [[complain objectForKey:@"id"]intValue];
        rowToDelete = indexPath.row;
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:LocalizedString(@"confirmation") message:LocalizedString(@"deleteComplaint") delegate:self cancelButtonTitle:LocalizedString(@"no") otherButtonTitles:LocalizedString(@"yes"), nil];
        [alert show];
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *feature = [complainsSearchData objectAtIndex:indexPath.row];
    
    ComplainStatusViewController *view = [[ComplainStatusViewController alloc]init];
    view.complain = feature;
    [self.navigationController pushViewController:view animated:YES];
}

#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [activityIndicator startAnimating];
        
        NSDictionary *feature = [complainsSearchData objectAtIndex:rowToDelete];
        [config deleteComplaint:complaintId complaintType:[[feature objectForKey:@"complaintType"] intValue] completion:^{
            
            [complainsSearchData removeObjectAtIndex:rowToDelete];
            [complainsTableView reloadData];
            [activityIndicator stopAnimating];
        }failure:^{
            
            [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"deleteError") cancelButtonTile:LocalizedString(@"ok")];
            [activityIndicator stopAnimating];
        }];
    }
}

#pragma mark - SearchBar Delegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
//    [self dismissSearchBar:searchBar];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    if (searchText.length) {
        complainsSearchData = [complainsData mutableCopy];

        [self startSearchForString:[Utility replaceArabicNumbers:searchText]];
    } else {
        
        complainsSearchData = [complainsData mutableCopy];
        [complainsTableView reloadData];
    }
}

- (void)startSearchForString:(NSString *)string {
    for (NSDictionary *complain in complainsData) {
        
        NSString *idString = [NSString stringWithFormat:@"%@",[complain objectForKey:@"id"]];
        if ([idString rangeOfString:string].location == NSNotFound) {
            [complainsSearchData removeObject:complain];
        }
    }
    [complainsTableView reloadData];
}

-(void)loadArrayFromGraphics: (NSArray *)newArray {
    complainsData = [[NSMutableArray alloc]init];
    
    for (AGSGraphic *graphic in newArray) {
        NSDictionary *attributes = graphic.allAttributes;
        
        NSNumber *mainTypeId = [attributes objectForKey:@"main_type_id"];
        
        NSString *mainType;
        switch (complainLayer) {
            case 0: {       //Transportation
                mainType = [config getMaintenanceTypeById:mainTypeId type:@"TransportationTypes"];
                complaintType = 1;
            }
                break;
            case 1: {       //Maintenance
                mainType = [config getMaintenanceTypeById:mainTypeId type:@"MaintenanceTypes"];
                complaintType = 3;
            }
                break;
            case 2: case 3: {       //Death
                mainType = [config getMaintenanceTypeById:mainTypeId type:@"HealthTypes"];
                
                if ([mainTypeId isEqualToNumber:@3]) {  //Death
                    complaintType = 4;
                }
                else  {             //PublicAffairs (Health)
                    complaintType = 2;
                }
            }
                break;
                
            default:
                break;
        }
        
        NSString *date = [config getDateFromSeconds:[[attributes objectForKey:@"time"] doubleValue]/1000];
        
        NSDictionary *statusValues = [UserDefaults objectForKey:@"MaintenanceStatus"];
        NSString *statusName = [statusValues objectForKey:[NSString stringWithFormat:@"%@",[attributes objectForKey:@"status"]]];
        
        NSArray *values = [[NSArray alloc]initWithObjects:[attributes objectForKey:@"complaint_id"], mainType, statusName, [NSNumber numberWithInt:complaintType], date, nil];
        NSArray *keys = [[NSArray alloc]initWithObjects:@"id", @"mainType", @"status", @"complaintType", @"date", nil];
        
        NSMutableDictionary *feature = [[NSMutableDictionary alloc]initWithObjects:values forKeys:keys];
        [complainsData addObject:feature];
    }
    
    complainsSearchData = [[NSMutableArray alloc]initWithArray:complainsData];
    
    cellIdentifier = @"CustomTableViewCell";
}

-(void)reloadTable: (NSNotification *)notification {
    NSArray *newComplains = notification.object;
    [self loadArrayFromGraphics:newComplains];
//    complainsSearchData = [newComplains mutableCopy];
    [complainsTableView reloadData];
}

@end
