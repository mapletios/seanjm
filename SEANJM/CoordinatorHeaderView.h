//
//  CoordinatorHeaderView.h
//  SEANJM
//
//  Created by Abdul Kareem on 9/5/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CoordinatorHeaderView : UITableViewCell
@property (nonatomic,weak)IBOutlet UILabel *lblTopTitle;
@property (nonatomic,weak)IBOutlet UILabel *lblRightTitle;
@property (nonatomic,weak)IBOutlet UILabel *lblLeftTitle;
@property (nonatomic,weak)IBOutlet UIButton *btnImage;

@end
