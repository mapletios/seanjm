//
//  HeaderView.h
//  SEANJM
//
//  Created by Abdul Kareem on 9/2/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderView : UITableViewCell
@property (nonatomic,weak)IBOutlet UILabel *lblTitle;
-(void)setTitle:(NSString *)title;
@end
