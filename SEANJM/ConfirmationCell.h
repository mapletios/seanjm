//
//  ConfirmationCell.h
//  SEANJM
//
//  Created by Abdul Kareem on 9/5/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfirmationCell : UITableViewCell
@property (nonatomic,weak)IBOutlet UIButton *btnAdd;
@property (nonatomic,weak)IBOutlet UIButton *btnCancel;
@end
