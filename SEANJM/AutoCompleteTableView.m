//
//  AutoCompleteTableView.m
//  SEANJM
//
//  Created by Abdul Kareem on 9/19/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "AutoCompleteTableView.h"
#import "Macros.h"
#import "Utility.h"
#import "Utility.h"
#import "Macros.h"
#import "BTSData.h"
#import "LanguageController.h"
#import "UserInformation.h"
#import "SingleTitleCell.h"
#import "HeaderView.h"

@interface AutoCompleteTableView ()<UITableViewDelegate>
@property (nonatomic)NSMutableArray         *autoCompleteArray;
@property (nonatomic)NSString               *autoCompleteCellIdentifier;
//@property (nonatomic)IBOutlet UITableView   *autoCompleteTableView;
@end

@implementation AutoCompleteTableView

-(id)initWithArray :(NSMutableArray *)list_Array
{
    self =[super init];
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _autoCompleteArray =[NSMutableArray array];
    // Uncomment the following line to preserve selection between presentations.
     self.clearsSelectionOnViewWillAppear = NO;
    [self.tableView setDelegate:self];
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    _autoCompleteCellIdentifier = @"HeaderCell";
    
        [self.tableView registerNib:[UINib nibWithNibName:@"HeaderView" bundle:[NSBundle mainBundle]]forCellReuseIdentifier:_autoCompleteCellIdentifier];
}

-(void)setArray :(NSMutableArray *)list_Array
{
    _autoCompleteArray = list_Array;
    [self.tableView reloadData];
}
#pragma mark -UITABLEVIEW DATASOURCE

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _autoCompleteArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    HeaderView *autoCell =[tableView dequeueReusableCellWithIdentifier:_autoCompleteCellIdentifier forIndexPath:indexPath];
    [autoCell.lblTitle setText:[_autoCompleteArray objectAtIndex:indexPath.row]];
    UIButton *btnCell =[UIButton buttonWithType:UIButtonTypeCustom];
    [btnCell setFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 44)];
    [btnCell addTarget:self.parentViewController action:@selector(filteredCellTapped:) forControlEvents:UIControlEventTouchUpInside];
    [btnCell setTag:indexPath.row];
    [autoCell addSubview:btnCell];
    return autoCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
 
}

#pragma mark - Table view data source



@end
