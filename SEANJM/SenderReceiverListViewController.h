//
//  SenderReceiverListViewController.h
//  SEANJM
//
//  Created by Abdul Kareem on 9/16/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SenderReceiverListViewController : UIViewController
-(id)initWithSelectedBusesOfRoute :(NSArray *)busesForRoute;
@end
