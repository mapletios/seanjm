//
//  AdminReportsViewController.m
//  SEANJM
//
//  Created by Abdul Kareem on 9/19/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "AdminReportsViewController.h"
#import "MBProgressHUD.h"
#import "Utility.h"
#import "Macros.h"
#import "BTSData.h"
#import "ConfigurationManager.h"
#import "HeaderView.h"
#import "ReportCell.h"
#import "LanguageController.h"
#import "SAGroupViewController.h"
#import "AFNetworking.h"
#import "UserInformation.h"
#import "ActionSheetStringPicker.h"
#import "BusInfoViewController.h"
#import "AutoCompleteTableView.h"
#import "ActionSheetStringPicker.h"

#define BTSADgetReportRouteGroupBusesExtended @"http://213.236.53.172/snbts/restserviceimpl.svc/getReportRouteGroupBusesExtended"
//Bus Detail
#define BTSADgetReportAccordingRouteGroupBus @"http://213.236.53.172/snbts/restserviceimpl.svc/getReportAccordingRouteGroupBus"

//All Buses For Routes
#define  BTSADgetBusesForAllRoutesForSpesificGroup @"http://213.236.53.172/snbts/restserviceimpl.svc/getBusesForAllRoutesForSpesificGroup"
@interface AdminReportsViewController ()<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>
{
    __weak IBOutlet UITextField *txtPlateNo;
    ConfigurationManager *config;
    BOOL isFromRouteOption;
    BusInfoViewController *infoBus;
    AutoCompleteTableView *objAutoComplete;
    NSInteger busSelectedIndex;
}
@property (nonatomic) MBProgressHUD             *hudview;
@property (nonatomic)NSMutableArray             *reportKeysArray;
@property (nonatomic,weak)IBOutlet UITableView  *reportTableView;
@property (nonatomic)NSString                   *reportCellIdentifier;
@property (nonatomic)NSMutableDictionary        *inheritedSelectedRoute;
@property (nonatomic)NSArray                    *groupsInfoKeys;
@property (nonatomic)NSMutableArray             *groupArray;
@property (nonatomic)NSString                   *reportService;
@property (nonatomic)NSString                   *allBusService;
@property (nonatomic)NSMutableArray             *allBussesArray;
@property (nonatomic)NSMutableArray             *autToFilteredArray;

@end

@implementation AdminReportsViewController
-(id)initWithSelectedRoute :(NSDictionary *)selected_route isFromRoute :(BOOL)isfrom_route
{
    self =[super init];
    
    if(isfrom_route)
    {
        _inheritedSelectedRoute = [NSMutableDictionary dictionaryWithDictionary:selected_route];
        isFromRouteOption = isfrom_route;
        
        //For these keys we need to show Detail information
        _groupsInfoKeys =[NSArray arrayWithObjects:@"activeGroupsCount",
                          @"finishedGroupsCount",
                          @"notStartedGroupsCount",
                          @"groupsTotalCountsNumber",nil];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    config =[[ConfigurationManager alloc]init];
    _groupArray =[NSMutableArray array];
    _autToFilteredArray =[NSMutableArray array];
    
    _reportKeysArray = [NSMutableArray array];
    
    
    _reportCellIdentifier = @"ReportCell";
    [_reportTableView registerNib:[UINib nibWithNibName:_reportCellIdentifier bundle:[NSBundle mainBundle]]forCellReuseIdentifier:_reportCellIdentifier];
    
    [_reportTableView registerNib:[UINib nibWithNibName:@"HeaderView" bundle:[NSBundle mainBundle]]forCellReuseIdentifier:@"HeaderCell"];
    
    //Call Reports Service
     _reportService =[NSString stringWithFormat:@"%@/%@",BTSADgetReportRouteGroupBusesExtended,[UserInformation getGroupID]];
    [self performSelector:@selector(loadServiceWithRequestUrlString:) withObject:_reportService afterDelay:0.0005];
    
    //Call All Buses Service
    _allBusService =[NSString stringWithFormat:@"%@/%@",BTSADgetBusesForAllRoutesForSpesificGroup,[UserInformation getGroupID]];
    [self performSelector:@selector(loadServiceWithRequestUrlString:) withObject:_allBusService afterDelay:0.0005];
    [_reportTableView reloadData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(busInfoClosed) name:@"BusInfo_close"object:nil];
    
    UITapGestureRecognizer *singleTapgesture =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:singleTapgesture];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) name:UITextFieldTextDidChangeNotification object:txtPlateNo];
    
    self.navigationController.navigationBar.topItem.title = [[UserInformation getUserSelectedRoute] objectForKey:@"name"];
    self.navigationController.navigationBar.backItem.title = LocalizedString(@"back");
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
}

-(void)loadServiceWithRequestUrlString :(NSString *)url_String
{
    if(![Utility checkInternetConnection])
        return;
    
    if(!_hudview)
    {
        _hudview = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        _hudview.mode = MBProgressHUDModeIndeterminate;
        _hudview.labelText = @"Loading...";
        [_hudview setDimBackground:YES];
    }

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

    [manager GET:url_String parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
       // //NSLog(@"Group INfo List %@", responseObject);
        if(![responseObject  isKindOfClass:[NSNull class]])
        {
            if([responseObject objectForKey:@"Result"])
            {
                
                NSArray *resultArray = [responseObject objectForKey:@"Result"];
                int routeID =[[[UserInformation getUserSelectedRoute] objectForKey:@"id"] intValue];
                NSArray *filteredArray =[resultArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"routeId == %d",routeID]];
                if([operation.request.URL.relativeString isEqualToString:_reportService])
                {
                    if(filteredArray.count > 0)
                    {
                        _inheritedSelectedRoute =[[filteredArray firstObject] objectForKey:@"btsroutebusesreport"];
                        _reportKeysArray = [NSMutableArray arrayWithArray:[_inheritedSelectedRoute allKeys]];
                        [_reportTableView reloadData];
                        
                    }
                }
                else if([operation.request.URL.relativeString isEqualToString:_allBusService])
                {
                    _allBussesArray = [NSMutableArray arrayWithArray:[[filteredArray firstObject] objectForKey:@"btsBuses"]];
                    _autToFilteredArray = _allBussesArray;
                }

            }
            
            
        }
        [_hudview hide:YES];
        _hudview = nil;
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"BTS Error: %@", error);
        [_hudview setHidden:YES];
        _hudview = nil;
    }];
}

-(void)loadBusDetails
{
    if(![Utility checkInternetConnection])
        return;
    
    if(!_hudview)
    {
    _hudview = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _hudview.mode = MBProgressHUDModeIndeterminate;
    _hudview.labelText = @"Loading...";
    [_hudview setDimBackground:YES];
    }
    NSString *serviceURl =[NSString stringWithFormat:@"%@",BTSADgetReportAccordingRouteGroupBus];
    
    NSMutableDictionary *parameters =[NSMutableDictionary dictionary];
    [parameters setObject:[UserInformation getGroupID] forKey:kgroupId];
    [parameters setObject:[[UserInformation getUserSelectedRoute] objectForKey:@"id"] forKey:krouteId];
    [parameters setObject:[[_allBussesArray objectAtIndex:busSelectedIndex] objectForKey:@"plateNumber"] forKey:@"plateNumber"];
    [parameters setObject:[[_allBussesArray objectAtIndex:busSelectedIndex] objectForKey:@"companyName"] forKey:@"companyName"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [manager POST:serviceURl parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //NSLog(@"Group INfo List %@", responseObject);
        if(![responseObject  isKindOfClass:[NSNull class]])
        {
            if([responseObject objectForKey:@"Result"])
            {
                NSDictionary *resultDic = [responseObject objectForKey:@"Result"];
                if(resultDic.count >0)
                    [self infoLoad:resultDic];
            }
            
            
        }
        [_hudview setHidden:YES];
        _hudview = nil;
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"BTS Error: %@", error);
        [_hudview setHidden:YES];
        _hudview = nil;
    }];
}


-(void)infoLoad :(NSDictionary *)bus_dic
{
    infoBus =[[BusInfoViewController alloc]initWithBusDetail:bus_dic];
    [self.navigationController pushViewController:infoBus animated:YES];
}

#pragma mark -
#pragma mark UITableView DataSource

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    HeaderView *headerView = [tableView dequeueReusableCellWithIdentifier:@"HeaderCell"];
    [headerView setTitle:LocalizedString(@"reportsBTS")];
    
    return headerView;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _reportKeysArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ReportCell *reportCell =[tableView dequeueReusableCellWithIdentifier:_reportCellIdentifier forIndexPath:indexPath];
    
    if(isFromRouteOption)
    {
        reportCell.lblLeftTitle.text = [NSString stringWithFormat:@"%@",
                                        [[_inheritedSelectedRoute objectForKey:@"btsroutegroups"]
                                         objectForKey:[_reportKeysArray objectAtIndex:indexPath.row]]];
        
    }
    else
    {
        reportCell.lblLeftTitle.text = [NSString stringWithFormat:@"%@",
                                        [_inheritedSelectedRoute objectForKey:[_reportKeysArray objectAtIndex:indexPath.row]]];
    }
    reportCell.lblRightTitle.text = [NSString stringWithFormat:@"  %@ :",LocalizedString([_reportKeysArray objectAtIndex:indexPath.row])];
    
    
    
    if(![_groupsInfoKeys containsObject:[_reportKeysArray objectAtIndex:indexPath.row]])
    {
        [reportCell.btnImage setHidden:YES];
        [reportCell.btnImage setUserInteractionEnabled:NO];
    }
    else
    {
        [reportCell.btnImage setContentMode:UIViewContentModeScaleAspectFit];
        [reportCell.btnImage setBackgroundImage:[UIImage imageNamed:@"ic_info"] forState:UIControlStateNormal];
        [reportCell.btnImage.superview setTag:indexPath.row];
        [reportCell.btnImage addTarget:self action:@selector(leftButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    [reportCell setBackgroundColor:[UIColor whiteColor]];
    
    
    return reportCell;
}

#pragma mark - TableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

#pragma mark -
#pragma mark TEXTField Delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.view endEditing:YES];
    [ActionSheetStringPicker showPickerWithTitle:LocalizedString(@"select") rows:[_allBussesArray valueForKey:@"displayName"] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        [textField setText:selectedValue];
        busSelectedIndex = selectedIndex;
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:textField];
    
    /*
    if(!objAutoComplete)
    {
        
        objAutoComplete =[[AutoCompleteTableView alloc]init];
        [objAutoComplete.view.layer setBorderWidth:3.0];
        [objAutoComplete.view.layer setBorderColor:[UIColor darkGrayColor].CGColor];
        [objAutoComplete.view.layer setCornerRadius:3.0];
        [objAutoComplete.view setClipsToBounds:YES];
    }

    [objAutoComplete.view setFrame:CGRectMake(CGRectGetMinX(txtPlateNo.frame), CGRectGetMaxY(txtPlateNo.frame), CGRectGetWidth(txtPlateNo.frame)*1.5, 300)];
    //[objAutoComplete.view setCenter:self.view.center];
    objAutoComplete.view.alpha=0.0;
    
    [self addChildViewController:objAutoComplete];
    [self.view addSubview:objAutoComplete.view];
    [objAutoComplete didMoveToParentViewController:self];
    [objAutoComplete setArray:_allBussesArray];
    [objAutoComplete.tableView reloadData];
    
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^(void) {
        
        objAutoComplete.view.alpha=1.0;
        
        
    } completion:^(BOOL finished) {
        
    }];
    
    //[objAutoComplete.view setCenter:self.view.center];
    */
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^(void) {
        
        objAutoComplete.view.alpha=0.0;
    } completion:^(BOOL finished) {
        
        
        [objAutoComplete.view removeFromSuperview];
        [objAutoComplete removeFromParentViewController];
        [objAutoComplete didMoveToParentViewController:nil];
        
    }];
}

- (void)textFieldDidChange:(NSNotification *)notification {
    // Do whatever you like to respond to text changes here.
    
    UITextField *textfield =notification.object;
    [objAutoComplete setArray:[self filteringArray:_allBussesArray :textfield.text]];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    

    return YES;
}
-(IBAction)leftButtonPressed:(UIButton *)sender
{
//    if([[[_inheritedSelectedRoute objectForKey:@"btsroutegroups"] objectForKey:[_reportKeysArray objectAtIndex:sender.superview.tag]] intValue] >0)
//    {
//        NSString *groupInfokey = [_reportKeysArray objectAtIndex:sender.superview.tag];
//        
//        SAGroupViewController *objSAGroup =[[SAGroupViewController alloc]initWithSelectedOption:groupInfokey];
//        [self.navigationController pushViewController:objSAGroup animated:YES];
//    }
}

-(IBAction)filteredCellTapped:(UIButton *)sender
{
    [txtPlateNo setText:[_autToFilteredArray objectAtIndex:sender.tag]];
}

-(void)busInfoClosed
{
    [UIView animateWithDuration:0.8 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^(void) {
        
        infoBus.view.alpha=0.0;
    } completion:^(BOOL finished) {
        
        
        [infoBus.view removeFromSuperview];
        [infoBus removeFromParentViewController];
        [infoBus didMoveToParentViewController:nil];
        
    }];
}


-(NSMutableArray *)filteringArray :(NSMutableArray *)fullArray :(NSString *)typingString
{
    _autToFilteredArray = (NSMutableArray *)[fullArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF contains[cd] %@",typingString]];
    
    return _autToFilteredArray;
}
-(IBAction)showBusDetailTapped:(id)sender
{
    [self.view endEditing:YES];
    if(txtPlateNo.text.length >0)
        [self loadBusDetails];
    else
        [Utility showAlertViewWithTitle:@"" message:LocalizedString(@"pleaseselectPlateNo") cancelButtonTile:LocalizedString(@"ok")];
}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

@end
