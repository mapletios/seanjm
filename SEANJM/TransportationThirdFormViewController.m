//
//  TransportationThirdFormViewController.m
//  SEANJM
//
//  Created by Randa Al-Sadek on 7/23/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "TransportationThirdFormViewController.h"
#import "MapCommViewController.h"
#import "ActionSheetStringPicker.h"
#import "ConfigurationManager.h"
#import "Utility.h"
#import "LanguageController.h"
#import "Macros.h"
#import "PNTToolbar.h"

@interface TransportationThirdFormViewController ()<UITextFieldDelegate, UITextViewDelegate> {
    __weak IBOutlet UILabel *pointLatLbl;
    __weak IBOutlet UILabel *pointLonLbl;
    __weak IBOutlet UITextField *pointLatTxt;
    __weak IBOutlet UITextField *pointLonTxt;
    __weak IBOutlet UIScrollView *scrollView;
    __weak IBOutlet UILabel *areaLbl;
    __weak IBOutlet UITextField *areaTxt;
    __weak IBOutlet UILabel *busNumLbl;
    __weak IBOutlet UITextField *busNumTxt;
    __weak IBOutlet UILabel *companyNameLbl;
    __weak IBOutlet UITextField *companyNameTxt;
    __weak IBOutlet UILabel *reportedByLbl;
    __weak IBOutlet UITextField *reportedByTxt;
    __weak IBOutlet UILabel *reportedNumLbl;
    __weak IBOutlet UITextField *reportedNumTxt;
    __weak IBOutlet UILabel *driverLbl;
    __weak IBOutlet UITextField *driverTxt;
    __weak IBOutlet UILabel *changeBusLbl;
    __weak IBOutlet UIButton *changeBusCheck;
    __weak IBOutlet UILabel *commentsLbl;
    __weak IBOutlet UITextView *commentsTxtView;
    __weak IBOutlet UIButton *sendButton;
    __weak IBOutlet UIButton *mapButton;
    
    NSMutableArray  *inputAccessoryArray;
    __weak IBOutlet UIActivityIndicatorView *activityIndicator;
    
    ConfigurationManager *config;
    NSMutableArray *mapFeature;
    NSNumber *isBusChanged;
}

@end

@implementation TransportationThirdFormViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.navigationItem setTitle:LocalizedString(@"escapeComplain")];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    tap.cancelsTouchesInView = NO;
    
    [self.view addGestureRecognizer:tap];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(returnMapPoint:) name:@"ReturnMapPoint" object:nil];
    
    [Utility setFormsWithPointLat:pointLatLbl pointLon:pointLonLbl mapButton:mapButton sendButton:sendButton username:reportedByTxt userPhone:reportedNumTxt];
    
    [Utility setTextViewBorder:commentsTxtView];
 
    config = [[ConfigurationManager alloc]init];
    
    [Utility setScrollViewSize:scrollView];
    
    isBusChanged = @0;
    [Utility setWhiteShadowToAllLabels:scrollView];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    inputAccessoryArray = [NSMutableArray array];
    for (id subview in scrollView.subviews) {
        if([subview isKindOfClass:[UITextField class]] || [subview isKindOfClass:[UITextView class]])
        {
            [inputAccessoryArray addObject:subview];
        }
    }
    
    PNTToolbar *toolbar = [PNTToolbar defaultToolbar];
    toolbar.navigationButtonsTintColor = [UIColor blueColor];
    toolbar.mainScrollView = scrollView;
    toolbar.inputFields = inputAccessoryArray;
    
    
   
}
-(void)viewDidDisappear:(BOOL)animated
{
    if(![self.navigationController.viewControllers containsObject:self])
        DispatchEvent(kStopLocation, nil);
}


-(void)viewWillAppear:(BOOL)animated
{
     DispatchEvent(kStartLocation, nil);
    [Utility applyBlueRoundedButton:sendButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma UITextFieldDelegate 
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField == areaTxt) {
        [self.view endEditing:YES];
        
        NSArray *faultTypesNames = [config getTransportationSubTypes:@3];
        [ActionSheetStringPicker showPickerWithTitle:@"" rows:faultTypesNames initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
            
            textField.text = selectedValue;
            
        } cancelBlock:nil origin:textField];
    }
    else if (textField == companyNameTxt) {
        [self.view endEditing:YES];
        
        NSArray *busCompaniesNames = [config getBusCompanieaNames];
        [ActionSheetStringPicker showPickerWithTitle:@"" rows:busCompaniesNames initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
            
            textField.text = selectedValue;
            
        } cancelBlock:nil origin:textField];
    }
    else {
        [scrollView setContentOffset:CGPointMake(0, CGRectGetMinY(textField.frame)-5) animated:YES];
        return YES;
    }
    return NO;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [scrollView setContentOffset:CGPointMake(0, CGRectGetMinY(textView.frame)-5) animated:YES];
}

#pragma mark -
#pragma mark - Keyboard Delegate


-(void)keyboardWillHide:(NSNotification *)notification
{
    [scrollView setContentOffset:CGPointZero animated:YES];
}

- (IBAction)sendButtonPressed:(id)sender {
    
    if(![Utility checkInternetConnection])
        return;
    
    else if(![Utility isCurrentLocationAvailable])
    {
        [Utility showAlertViewWithTitle:LocalizedString(@"locationservice") message:LocalizedString(@"checklocationservice") cancelButtonTile:LocalizedString(@"ok")];
        return;
    }
    
    if ([busNumTxt.text length] == 0 || [companyNameTxt.text length] == 0) {
        [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"emptyFields") cancelButtonTile:LocalizedString(@"ok")];
    }
    else if ([pointLatTxt.text length] == 0 || [pointLonTxt.text length] == 0) {
        [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"emptyMapPoint") cancelButtonTile:LocalizedString(@"ok")];
    }
    else {
        NSDictionary *subCategory = [config getTransSubTypeByName:areaTxt.text];
        NSDictionary *busObject = [config getBusObjectByName:companyNameTxt.text];
        
        NSArray *data = [[NSArray alloc]initWithObjects:@3, (NSNumber *)[subCategory objectForKey:@"id"], (NSNumber *)[busObject objectForKey:@"id"], busNumTxt.text, pointLatTxt.text, pointLonTxt.text, isBusChanged, nil];
        
        NSArray *dataKeys = [[NSArray alloc]initWithObjects:@"mainTypeId", @"subTypeId", @"companyID", @"busID", @"xPosition", @"yPosition", @"isBusChanged", nil];
        
        NSMutableDictionary *complaintDic = [[NSMutableDictionary alloc]initWithObjects:data forKeys:dataKeys];
        
        if ([reportedByTxt.text length] > 0)
            [complaintDic setObject:reportedByTxt.text forKey:@"complaintName"];
        if ([reportedNumTxt.text length] > 0)
            [complaintDic setObject:reportedNumTxt.text forKey:@"complaintMobile"];
        if ([driverTxt.text length] > 0)
            [complaintDic setObject:driverTxt.text forKey:@"driverName"];
        if ([commentsTxtView.text length] > 0)
            [complaintDic setObject:commentsTxtView.text forKey:@"comments"];
        
        [activityIndicator setHidden:NO];
        [activityIndicator startAnimating];
        
        [config sendTransportationComplaint:complaintDic completion:^{
            [self resetView];
            [Utility showAlertViewWithTitle:LocalizedString(@"success") message:LocalizedString(@"successMessage") cancelButtonTile:LocalizedString(@"ok")];
            [activityIndicator stopAnimating];
            [self.navigationController popViewControllerAnimated:YES];

        }failure:^{
            [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"reportNotSent") cancelButtonTile:LocalizedString(@"ok")];
            [activityIndicator stopAnimating];
        }];
    }
}

- (IBAction)changeBusCheckPressed:(id)sender {
    if ([isBusChanged isEqualToNumber:@0]) {
        isBusChanged = [Utility setButtonChecked:isBusChanged button:(UIButton *)sender];
        NSLog(@"%@", isBusChanged);
    }
    else {
        isBusChanged = [Utility setButtonUnChecked:isBusChanged button:(UIButton *)sender];
        NSLog(@"%@", isBusChanged);
    }
}

- (IBAction)mapButtonPressed:(id)sender {
    
    if(![Utility checkInternetConnection])
        return;
    
    MapCommViewController *mapView = [[MapCommViewController alloc]init];
    mapView.mapType = 3;
    mapView.showReport = NO;
    [self.navigationController pushViewController:mapView animated:YES];
}

- (void)returnMapPoint:(NSNotification *)notification {
    mapFeature = notification.object;
    pointLatTxt.text = [mapFeature objectAtIndex:0];
    pointLonTxt.text = [mapFeature objectAtIndex:1];
}

- (void)resetView {
    areaTxt.text = @"";
    companyNameTxt.text = @"";
    busNumTxt.text = @"";
    driverTxt.text = @"";
    commentsTxtView.text = @"";
    
    isBusChanged = @1;
    [self changeBusCheckPressed:nil];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

@end
