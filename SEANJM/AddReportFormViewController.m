//
//  AddReportFormViewController.m
//  SEANJM
//
//  Created by Randa Al-Sadek on 5/20/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "AddReportFormViewController.h"
#import "ActionSheetStringPicker.h"
#import "FaultsTableViewController.h"
#import "ConfigurationManager.h"
#import "Macros.h"
#import "LanguageController.h"
#import "Utility.h"
#import <QuartzCore/QuartzCore.h>
#import "PNTToolbar.h"

@interface AddReportFormViewController ()<UITextFieldDelegate, UITextViewDelegate> {
    
    __weak IBOutlet UILabel *categoryLbl;
    __weak IBOutlet UILabel *subCategoryLbl;
    __weak IBOutlet UILabel *countLbl;
    __weak IBOutlet UILabel *commentsLbl;
    __weak IBOutlet UILabel *statusLbl;
    __weak IBOutlet UITextField *categoryTxt;
    __weak IBOutlet UITextField *typeTxt;
    __weak IBOutlet UITextField *quantityTxt;
    __weak IBOutlet UITextView *commentsTxtView;
    __weak IBOutlet UITextField *statusTxt;
    __weak IBOutlet UILabel *statusLabel;
    __weak IBOutlet UIButton *addFaultButton;
    __weak IBOutlet UIScrollView *scrollView;
    
    NSMutableArray  *inputAccessoryArray;
    
    ConfigurationManager *config;
    id categoryId;
    int selectedStatus;
    int flag;
    
}

@end

@implementation AddReportFormViewController

@synthesize selectedFault, editMode;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    addFaultButton.titleLabel.text = LocalizedString(@"done");
    categoryLbl.text = LocalizedString(@"category");
    subCategoryLbl.text = LocalizedString(@"subcategory");
    countLbl.text = LocalizedString(@"count");
    commentsLbl.text = LocalizedString(@"comments");
    
    [Utility setTextViewBorder:commentsTxtView];
    
    [self.navigationItem setTitle: LocalizedString(@"newReport")];
    config = [[ConfigurationManager alloc]init];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                           initWithTarget:self
                                           action:@selector(dismissKeyboard)];
    tap.cancelsTouchesInView = NO;
    
    [self.view addGestureRecognizer:tap];
            	
    quantityTxt.text = @"1";
    selectedStatus = -1;
    flag = -1;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [Utility setScrollViewSize:scrollView];
    
    inputAccessoryArray = [NSMutableArray array];
    for (id subview in scrollView.subviews) {
        if([subview isKindOfClass:[UITextField class]] || [subview isKindOfClass:[UITextView class]])
        {
            [inputAccessoryArray addObject:subview];
        }
    }
    
    PNTToolbar *toolbar = [PNTToolbar defaultToolbar];
    toolbar.navigationButtonsTintColor = [UIColor blueColor];
    toolbar.mainScrollView = scrollView;
    toolbar.inputFields = inputAccessoryArray;
}

-(void)viewWillAppear:(BOOL)animated {
    
    
    
    if (selectedFault.count > 0) {
        categoryTxt.text = [selectedFault objectForKey:@"name"];
        typeTxt.text = [selectedFault objectForKey:@"type"];
        quantityTxt.text = [NSString stringWithFormat:@"%@", [selectedFault valueForKey:@"count"]];
        commentsTxtView.text = [selectedFault objectForKey:@"comments"];
        [self getStatusText:[selectedFault valueForKey:@"status"]];
        [statusLabel setHidden:NO];
        [statusTxt setHidden:NO];
        
        if ([selectedFault valueForKey:@"flag"]) {
            flag = [[selectedFault valueForKey:@"flag"]intValue];
        }
        
        if (![[UserDefaults objectForKey:@"loginUser"] isEqualToString:[selectedFault objectForKey:@"username"]]) {
            [categoryTxt setUserInteractionEnabled:NO];
            [typeTxt setUserInteractionEnabled:NO];
            [quantityTxt setUserInteractionEnabled:NO];
        }
        else {
            [categoryTxt setUserInteractionEnabled:YES];
            [typeTxt setUserInteractionEnabled:YES];
            [quantityTxt setUserInteractionEnabled:YES];
        }
    }
    else {
        [statusLabel setHidden:YES];
        [statusTxt setHidden:YES];
    }
    [Utility applyBlueRoundedButton:addFaultButton];
    
    if (self.editMode) {
        [categoryTxt setEnabled:NO];
        [typeTxt setEnabled:NO];
    }
    else {
        [categoryTxt setEnabled:YES];
        [typeTxt setEnabled:YES];
    }
}

#pragma mark - TextField Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {

    
    if (textField == categoryTxt)
    {
        [self.view endEditing:YES];
        
        NSArray *categoriesNamesArray = [config getCategoriesNames];
        
        [ActionSheetStringPicker showPickerWithTitle:@"" rows:categoriesNamesArray initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
            
            textField.text = selectedValue;
            
            typeTxt.text = @"";
            
        } cancelBlock:nil origin:textField];
        
    }
    else if (textField == typeTxt)
    {
        [self.view endEditing:YES];
        
        NSArray *subCategoriesNamesArray = [config getSubCategoriesNames:categoryTxt.text];
        
        [ActionSheetStringPicker showPickerWithTitle:@"" rows:subCategoriesNamesArray initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                textField.text = selectedValue;
        } cancelBlock:nil origin:textField];

    }
    else if (textField == quantityTxt) {
        [textField resignFirstResponder];
        [self.view endEditing:NO];
        return YES;
    }
    
    else if (textField == statusTxt) {
        [self.view endEditing:YES];
        
        NSArray *statusArray = [[NSArray alloc]initWithObjects:LocalizedString(@"notFixed"), LocalizedString(@"fixed"), LocalizedString(@"inProgress"), nil];
        
        [ActionSheetStringPicker showPickerWithTitle:@"" rows:statusArray initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
            textField.text = selectedValue;
            selectedStatus = (int)selectedIndex;
        } cancelBlock:nil origin:textField];

    }
    
    return NO;
}

#pragma mark -
#pragma mark - Keyboard Delegate
- (void)keyboardWillShow:(NSNotification *)notification
{
//    CGPoint keyboardPoint = [[[notification userInfo]objectForKey:UIKeyboardFrameEndUserInfoKey]CGRectValue].origin;
    
    for (UIView *view in scrollView.subviews) {
        if (view.isFirstResponder) {
//            CGFloat viewY = CGRectGetMaxY(view.frame);
            
//            CGFloat difference = (keyboardPoint.y - Navigation_StatusBar_Height-64.0f) - viewY;
            [scrollView setContentOffset:CGPointMake(0,CGRectGetMinY(view.frame)-5) animated:YES];
        }
    }
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [scrollView setContentOffset:CGPointZero animated:YES];
}
- (IBAction)addFaultButtonPressed:(id)sender {
    
    if ([categoryTxt.text length] == 0 || [typeTxt.text length] == 0 || [quantityTxt.text length] == 0) {
        [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"emptyFields") cancelButtonTile:LocalizedString(@"ok")];
    }
    else {
                
        NSDictionary *selectedCategory = [config getCategoryObjectByName:categoryTxt.text];
        NSDictionary *selectedSubCategory = [config getSubCategoryObjectByName:typeTxt.text];
        
        NSString *date = [config getCurrentDate];
        
        NSNumber *cost = [NSNumber numberWithInt:[[selectedSubCategory objectForKey:@"cost"]intValue]];
        NSNumber *quantity = [NSNumber numberWithInt:[quantityTxt.text intValue]];
        
        NSMutableDictionary *fault = [[NSMutableDictionary alloc]init];
        
        [fault setObject:categoryTxt.text forKey:@"name"];
        [fault setObject:typeTxt.text forKey:@"type"];
        [fault setObject:[selectedCategory objectForKey:@"id"] forKey:@"categoryId"];
        [fault setObject:[selectedSubCategory objectForKey:@"id"] forKey:@"subCategoryId"];
        [fault setValue:cost forKey:@"cost"];
        [fault setValue:quantity forKey:@"count"];
        [fault setObject:commentsTxtView.text forKey:@"comments"];
        
        if (selectedStatus == -1) {
            selectedStatus = 0;
        }
        
        [fault setValue:[NSNumber numberWithInt:selectedStatus] forKey:@"status"];
        
        if (editMode) {
            [fault setObject:[selectedFault objectForKey:@"username"] forKey:@"username"];
            [fault setObject:[UserDefaults objectForKey:@"loginUser"] forKey:@"username_u"];
            [fault setObject:@"" forKey:@"username_d"];
            
            [fault setObject:[selectedFault objectForKey:@"reportDetailsDate"] forKey:@"reportDetailsDate"];
            [fault setObject:date forKey:@"reportDetailsDate_u"];
            [fault setObject:@"" forKey:@"reportDetailsDate_d"];
            [fault setObject:[selectedFault objectForKey:@"oldFault"] forKey:@"oldFault"];
            
//            if (flag > -1) {
//                [fault setValue:@3 forKey:@"flag"];
//            }
//            else {
                [fault setValue:@1 forKey:@"flag"];
//            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SaveToFaults" object:fault];
        }
        else {
            [fault setObject:[UserDefaults objectForKey:@"loginUser"] forKey:@"username"];
            [fault setObject:@"" forKey:@"username_u"];
            [fault setObject:@"" forKey:@"username_d"];
            
            [fault setObject:date forKey:@"reportDetailsDate"];
            [fault setObject:@"" forKey:@"reportDetailsDate_u"];
            [fault setObject:@"" forKey:@"reportDetailsDate_d"];
            
            [fault setValue:@0 forKey:@"flag"];
            [fault setObject:[NSNumber numberWithBool:NO] forKey:@"oldFault"];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AddToFaults" object:fault];
        }
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)getStatusText: (NSNumber *)statusIndex {
    switch ([statusIndex intValue]) {
        case 0: {
            statusTxt.text = LocalizedString(@"notFixed");
            break;
        }
        case 1: {
            statusTxt.text = LocalizedString(@"fixed");
            break;
        }
        case 2: {
            statusTxt.text = LocalizedString(@"inProgress");
            break;
        }
               default: {
            statusTxt.text = LocalizedString(@"notFixed");
        }
    }
}

-(void)checkIfFaultExists {
   [[NSNotificationCenter defaultCenter] postNotificationName:@"SaveToFaults" object:nil];

}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

@end
