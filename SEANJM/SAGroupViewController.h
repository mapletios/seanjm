//
//  SAGroupViewController.h
//  SEANJM
//
//  Created by Abdul Kareem on 9/18/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SAGroupViewController : UIViewController
-(id)initWithSelectedOption :(NSString *)option_key;
@end
