//
//  BTSData.m
//  SEANJM
//
//  Created by Abdul Kareem on 9/3/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "BTSData.h"
#import "Macros.h"
#import "UserInformation.h"
#import "CoordinatorRoutes.h"
@implementation BTSData

+(NSArray *)getRoutes
{
    if([[UserDefaults objectForKey:kBTSCONFIGURATION] objectForKey:kbtsroutes])
        return [[UserDefaults objectForKey:kBTSCONFIGURATION] objectForKey:kbtsroutes];
    else
        return @[];
}

+(NSArray *)getRoles
{
    if([[UserDefaults objectForKey:kBTSCONFIGURATION] objectForKey:kbtsroles])
        return [[UserDefaults objectForKey:kBTSCONFIGURATION] objectForKey:kbtsroles];
    else
        return @[];
}

+(NSArray *)getBusCompanies
{
    if([[UserDefaults objectForKey:kBTSCONFIGURATION] objectForKey:kbtsbuscompanies])
        return [[UserDefaults objectForKey:kBTSCONFIGURATION] objectForKey:kbtsbuscompanies];
    else
        return @[];
}

+(NSString *)getBusCompanyNameWithBusId :(NSString *)bus_id
{
    if([[UserDefaults objectForKey:kBTSCONFIGURATION] objectForKey:kbtsbuscompanies])
    {
        NSArray *locationArray = [BTSData getBusCompanies];
        NSArray *filteredarray = [locationArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id == %d",bus_id.integerValue]];
        if(filteredarray.count >0)
            return [[filteredarray firstObject] objectForKey:@"name"];
    }
    return @"";
}


+(NSArray *)getAllBuses
{
    if([[UserDefaults objectForKey:kBTSCONFIGURATION] objectForKey:kbtsbuses])
        return [[UserDefaults objectForKey:kBTSCONFIGURATION] objectForKey:kbtsbuses];
    else
        return @[];
}

+(NSDictionary *)getBusFromPlateNumber :(NSString *)plateNo
{
    if([[UserDefaults objectForKey:kBTSCONFIGURATION] objectForKey:kbtsbuses])
    {
        NSArray *busesArray = [BTSData getAllBuses];
        NSArray *filteredarray = [busesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"plateNumber == %@",plateNo]];
    if(filteredarray.count >0)
        return [filteredarray firstObject];
    }
    return @{};
}

+(NSString *)getBusPlateNumberFromBusId :(NSString *)busId
{
    if([[UserDefaults objectForKey:kBTSCONFIGURATION] objectForKey:kbtsbuses])
    {
        NSArray *busesArray = [BTSData getAllBuses];
        NSArray *filteredarray = [busesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id == %@",busId]];
        if(filteredarray.count >0)
            return [[filteredarray firstObject] objectForKey:@"plateNumber"];
    }
    return @"";
}

+(NSDictionary *)getBusInfoFromBusId :(NSString *)busId
{
    if([[UserDefaults objectForKey:kBTSCONFIGURATION] objectForKey:kbtsbuses])
    {
        NSArray *busesArray = [BTSData getAllBuses];
        NSArray *filteredarray = [busesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id == %@",busId]];
        if(filteredarray.count >0)
            return [filteredarray firstObject];
    }
    return @{};
}

+(NSArray *)getLocations
{
    if([[UserDefaults objectForKey:kBTSCONFIGURATION] objectForKey:kbtslocations])
        return [[UserDefaults objectForKey:kBTSCONFIGURATION] objectForKey:kbtslocations];
    else
        return @[];
}

+(NSArray *)getUsers
{
    if([[UserDefaults objectForKey:kBTSCONFIGURATION] objectForKey:kbtsusers])
        return [[UserDefaults objectForKey:kBTSCONFIGURATION] objectForKey:kbtsusers];
    else
        return @[];
}


+(NSArray *)getAllUsersForCoordinator
{
    if([[UserDefaults objectForKey:kBTSCOORDINATORCONFIGURATION] objectForKey:kbtsusers])
        return [[UserDefaults objectForKey:kBTSCOORDINATORCONFIGURATION] objectForKey:kbtsusers];
    else
        return @[];
}
+(NSArray *)getAllGuidersForCoordinator
{
    if([[UserDefaults objectForKey:kBTSCOORDINATORCONFIGURATION] objectForKey:kbtsguiders])
        return [[UserDefaults objectForKey:kBTSCOORDINATORCONFIGURATION] objectForKey:kbtsguiders];
    else
        return @[];
}

+(NSArray *)getSenderLocationsForCoordinator
{
    if([[UserDefaults objectForKey:kBTSCOORDINATORCONFIGURATION] objectForKey:kbtsusers])
    {
        @autoreleasepool {
            
            NSArray  *allusers = [BTSData getAllUsersForCoordinator];
            NSString *SelectedroutID =[[[UserInformation getUserSelectedRoute] objectForKey:@"id"] stringValue];
            NSArray *filteredarray = [allusers filteredArrayUsingPredicate:
                                      [NSPredicate predicateWithFormat:@"routeId == %d && roleId == 0",SelectedroutID.integerValue]];
            return filteredarray;
        }
    }
    else
        return @[];
}


+(NSArray *)getReceiverLocationsForCoordinator
{
    if([[UserDefaults objectForKey:kBTSCOORDINATORCONFIGURATION] objectForKey:kbtsusers])
    {
        @autoreleasepool {
            
            NSArray  *allusers = [BTSData getAllUsersForCoordinator];
            NSString *SelectedroutID =[[[UserInformation getUserSelectedRoute] objectForKey:@"id"] stringValue];
            NSArray *filteredarray = [allusers filteredArrayUsingPredicate:
                                      [NSPredicate predicateWithFormat:@"routeId == %d && roleId == 1",SelectedroutID.integerValue]];
            return filteredarray;
        }
    }
    else
        return @[];
}

+(NSArray *)getGuidersForCoordinator
{
    if([[UserDefaults objectForKey:kBTSCOORDINATORCONFIGURATION] objectForKey:kbtsguiders])
    {
        @autoreleasepool {
            
            NSArray  *allusers = [BTSData getAllUsersForCoordinator];
            NSString *SelectedroutID =[[[UserInformation getUserSelectedRoute] objectForKey:@"id"] stringValue];
            NSArray *filteredarray = [allusers filteredArrayUsingPredicate:
                                      [NSPredicate predicateWithFormat:@"routeId == %d",SelectedroutID.integerValue]];
            return filteredarray;
        }
    }
    else
        return @[];
}

+(NSArray *)getAssignedUsersByManagerForRoute
{
    if([[UserDefaults objectForKey:kBTS_M_AssignedUsersAndGuides] objectForKey:@"btsusers"])
    {
        @autoreleasepool {
            
        //return [[UserDefaults objectForKey:kBTS_M_AssignedUsersAndGuides] objectForKey:@"btsusers"];
        NSArray  *allusers = [[UserDefaults objectForKey:kBTS_M_AssignedUsersAndGuides] objectForKey:@"btsusers"];
        NSString *SelectedroutID =[[[UserInformation getUserSelectedRoute] objectForKey:@"id"] stringValue];
        NSMutableArray  *userBelongsToRoute = [NSMutableArray  array];
        
        for (NSDictionary *btsUser in allusers) {
            if([[[btsUser objectForKey:krouteId] stringValue] isEqualToString:SelectedroutID])
                [userBelongsToRoute addObject:btsUser];
        }
        
        return userBelongsToRoute;
            
        }
    }
    
    else
        return @[];
}

+(NSArray *)getAssignedUsersByManagerForSpecificRoute :(NSString *)routeID
{
    if([[UserDefaults objectForKey:kBTS_M_AssignedUsersAndGuides] objectForKey:@"btsusers"])
    {
        @autoreleasepool {
            
            //return [[UserDefaults objectForKey:kBTS_M_AssignedUsersAndGuides] objectForKey:@"btsusers"];
            NSArray  *allusers = [[UserDefaults objectForKey:kBTS_M_AssignedUsersAndGuides] objectForKey:@"btsusers"];
            NSMutableArray  *userBelongsToRoute = [NSMutableArray  array];
            
            for (NSDictionary *btsUser in allusers) {
                if([[[btsUser objectForKey:krouteId] stringValue] isEqualToString:routeID])
                    [userBelongsToRoute addObject:btsUser];
            }
            
            return userBelongsToRoute;
            
        }
    }
    
    else
        return @[];
}

+(NSArray *)getAssignedBusesByCoordinatorForRoute
{
    if([UserDefaults objectForKey:kBTS_C_Assignedbuses])
    {
        @autoreleasepool {
            
            //return [[UserDefaults objectForKey:kBTS_M_AssignedUsersAndGuides] objectForKey:@"btsusers"];
            NSArray  *allBuses = [UserDefaults objectForKey:kBTS_C_Assignedbuses];
            NSString *SelectedbusID =[[[UserInformation getUserSelectedRoute] objectForKey:@"id"] stringValue];
            NSMutableArray  *busesBelongsToRoute = [NSMutableArray  array];
            
            for (NSDictionary *btsBus in allBuses) {
                if([[[btsBus objectForKey:krouteId] stringValue] isEqualToString:SelectedbusID])
                {
                    busesBelongsToRoute = [btsBus objectForKey:kbtsbuses];
                    break;
                }
            }
            
            return busesBelongsToRoute;
            
        }
    }
    
    else
        return @[];
}

+(NSArray *)getAssignedGuidersByManagerForRoute
{
    if([[UserDefaults objectForKey:kBTS_M_AssignedUsersAndGuides] objectForKey:kbtsguiders])
    {
        @autoreleasepool {

            NSArray  *allguiders = [[UserDefaults objectForKey:kBTS_M_AssignedUsersAndGuides] objectForKey:kbtsguiders];
            NSString *SelectedroutID =[[[UserInformation getUserSelectedRoute] objectForKey:@"id"] stringValue];
            NSMutableArray  *guidersBelongsToRoute = [NSMutableArray  array];
            
            for (NSDictionary *btsGuider in allguiders) {
                if([[[btsGuider objectForKey:krouteId] stringValue] isEqualToString:SelectedroutID])
                    [guidersBelongsToRoute addObject:btsGuider];
            }
            
            return guidersBelongsToRoute;
            
        }
    }
    else
        return @[];
}

+(NSString *)getLocationNameWithLocationId :(NSString *)location_id
{
     if([[UserDefaults objectForKey:kBTSCONFIGURATION] objectForKey:kbtslocations])
     {
       NSArray *locationArray = [NSArray arrayWithArray:[[UserDefaults objectForKey:kBTSCONFIGURATION] objectForKey:kbtslocations]];
       NSArray *filteredarray = [locationArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id == %d",location_id.integerValue]];
         if(filteredarray.count >0)
             return [[filteredarray firstObject] objectForKey:@"name"];
     }
    return @"";
}

+(NSString *)getRoleNameWithRoleId :(NSString *)role_id
{
    if([[UserDefaults objectForKey:kBTSCONFIGURATION] objectForKey:kbtsroles])
    {
        NSArray *locationArray = [NSArray arrayWithArray:[[UserDefaults objectForKey:kBTSCONFIGURATION] objectForKey:kbtsroles]];
        NSArray *filteredarray = [locationArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id == %d",role_id.integerValue]];
        if(filteredarray.count >0)
            return [[filteredarray firstObject] objectForKey:@"name"];
    }
    return @"";
}

+(NSString *)getRouteNameWithRouteId :(NSString *)route_id
{
    if([[UserDefaults objectForKey:kBTSCONFIGURATION] objectForKey:kbtsroutes])
    {
        NSArray *locationArray = [NSArray arrayWithArray:[[UserDefaults objectForKey:kBTSCONFIGURATION] objectForKey:kbtsroutes]];
        NSArray *filteredarray = [locationArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id == %d",route_id.integerValue]];
        if(filteredarray.count >0)
            return [[filteredarray firstObject] objectForKey:@"name"];
    }
    return @"";
}

//Coordinator
+(NSArray *)getCoordinatorRoutesWithNumberOfRounds
{
    if([UserDefaults objectForKey:kCoordinatorRoutesWithBusesAndRounds])
    {
        NSData *unarchievedData = [UserDefaults objectForKey:kCoordinatorRoutesWithBusesAndRounds];
        return [NSKeyedUnarchiver unarchiveObjectWithData:unarchievedData];
    }
    else
        return @[];
}

+(NSNumber *)getDefaultNumberOfRoundsFromSelectedRoute
{
    if([UserDefaults objectForKey:kCoordinatorRoutesWithBusesAndRounds])
    {
        NSData   *unarchievedData = [UserDefaults objectForKey:kCoordinatorRoutesWithBusesAndRounds];
        NSArray  *coordinatorRoutes = [NSKeyedUnarchiver unarchiveObjectWithData:unarchievedData];
        int SelectedroutID = [[[UserInformation getUserSelectedRoute] objectForKey:@"id"] intValue];
        
        for (CoordinatorRoutes *coorObj in coordinatorRoutes) {

            if([[coorObj.routeInfo objectForKey:@"id"] intValue] == SelectedroutID)
            {
                return coorObj.numberOfRounds;
            }
                
        }
    }
        return @0;
}

+(void)removeCoordinatorRoutesWithNumberOfRounds
{
    if([UserDefaults objectForKey:kCoordinatorRoutesWithBusesAndRounds])
    {
        [UserDefaults removeObjectForKey:kCoordinatorRoutesWithBusesAndRounds];
        [UserDefaults synchronize];
    }
    
}

+(void)saveCoordinatorRoutesWithNumberOfRounds :(NSArray *)routes_array
{
    NSData *archivedData =[NSKeyedArchiver archivedDataWithRootObject:routes_array];
    [UserDefaults setObject:archivedData forKey:kCoordinatorRoutesWithBusesAndRounds];
    [UserDefaults synchronize];
    
    
}

//Super Admin

+(NSArray *)getSuperAdminAllReportsForRoutes
{
    if([UserDefaults objectForKey:kSARAllRoutesReports])
        return [UserDefaults objectForKey:kSARAllRoutesReports];
    else
        return @[];
}

+(NSArray *)getAllGroups
{
    if([UserDefaults objectForKey:kAllGroups])
        return [UserDefaults objectForKey:kAllGroups];
    else
        return @[];
}


+(NSDictionary *)getSuperAdminReportForRoute :(int)route_id
{
    if([UserDefaults objectForKey:kSARAllRoutesReports])
    {
        @autoreleasepool {
            
            NSArray  *allusers = [BTSData getSuperAdminAllReportsForRoutes];
            NSArray *filteredarray = [allusers filteredArrayUsingPredicate:
                                      [NSPredicate predicateWithFormat:@"routeId == %d",route_id]];
            return [filteredarray firstObject];
        }
    }
    else
        return @{};
}

@end
