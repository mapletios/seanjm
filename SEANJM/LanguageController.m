//
//  Language.m
//  ACTVET
//
//  Created by Burhanuddin Sunelwala on 12/27/14.
//  Copyright (c) 2014 adec. All rights reserved.
//

#import "LanguageController.h"

@interface LanguageController() {
    
    NSBundle *languageBundle;
}

@end

@implementation LanguageController

+ (instancetype)sharedController {
    
    static LanguageController *sharedController = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        sharedController = [[self alloc] init];
    });
    return sharedController;
}

- (instancetype)init {
    
    if (self = [super init]) {
        
        languageBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"ar" ofType:@"lproj"]];
//        NSString *languageSelected = [[NSUserDefaults standardUserDefaults] objectForKey:@"SelectedLanguage"];
//        
//        if (languageSelected) {
//            
//            [self setLanguage:languageSelected];
//            languageBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:languageSelected ofType:@"lproj"]];
//        } else {
//            
//            [self setLanguage:@"en"];
//            languageBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"en" ofType:@"lproj"]];
//        }
    }
    return self;
}

- (void)setLanguage:(Language)language {
    
    _language = language;
    
//    [[NSUserDefaults standardUserDefaults] setObject:self.l forKey:@"SelectedLanguage"];
    NSString *langCode;
    switch (language) {
        case Arabic:
            langCode = @"ar";
            break;
            
        default:
            langCode = @"ar";
            break;
    }
    languageBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:langCode ofType:@"lproj"]];
//    dispatchEvent(@"LanguageChanged", nil);
}

- (NSString *)localizedStringForKey:(NSString *)key {
    
    return [languageBundle localizedStringForKey:key value:@"" table:nil];
}

- (NSString *)localizedNumberForNumber:(NSNumber *)number decimalDigits:(NSUInteger)digits {
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setMaximumFractionDigits:digits];
    if (self.language == Arabic) {
        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"ar"]];
    } else {
        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];
    }
    return [formatter stringFromNumber:number];
}

- (UIImage *)localizedImageNamed:(NSString *)imageName {
    
    imageName = [imageName stringByAppendingString:@"@2x"];
    NSString *pathForImage = [languageBundle pathForResource:imageName ofType:@"png"];
    return [UIImage imageWithContentsOfFile:pathForImage];
}

- (NSString *)localizedKey:(NSString *)key {
    
    switch (self.language) {
        case Arabic:
            return [key stringByAppendingString:@"_ar"];
            
        default:
            return key;
    }
}

@end
