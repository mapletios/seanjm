//
//  InfoViewController.m
//  SEANJM
//
//  Created by Abdul Kareem on 8/25/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "InfoViewController.h"
#import "Macros.h"
#import "Utility.h"

@interface InfoViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic)NSMutableArray *userInfoArray;
@property (nonatomic)NSArray *titleArray;
@property (nonatomic)UITableView *infoTableView;
@property (nonatomic)CGFloat headerHeight;

@end

@implementation InfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _headerHeight = 45.0;
    _titleArray = @[@"الاسم:",@"الهاتف:",@"مكتب:"];
    _userInfoArray = [NSMutableArray array];
    
    
    //[Utility LoadVisualEffectView:self.view];
    [self loadUserData];
    [self createTableView];
}

-(void)loadUserData
{
    [_userInfoArray addObject:[UserDefaults objectForKey:@"userFullName"]];
    [_userInfoArray addObject:[UserDefaults objectForKey:@"userPhone"]];
    if(![[UserDefaults objectForKey:@"officeNumber"] isEqualToString:@"-1"])
        [_userInfoArray addObject:[UserDefaults objectForKey:@"officeNumber"]];
}

-(IBAction)infoRemove:(id)sender
{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"Info_close"object:self];
}

#pragma mark -TableView Initialization
-(void)createTableView
{
    _infoTableView =[[UITableView alloc]initWithFrame:CGRectMake(0, 0, 280,126) style:UITableViewStylePlain];
    if([[UserDefaults objectForKey:@"officeNumber"] isEqualToString:@"-1"])
        [_infoTableView setFrame:CGRectMake(0, 0, 280,85)];
    [_infoTableView setRowHeight:40.0f];
//  [_infoTableView setEstimatedSectionFooterHeight:5];
    [_infoTableView setDelegate:self];
    [_infoTableView setDataSource:self];
    [_infoTableView setCenter:CGPointMake(SCREEN_SIZE.width/2, SCREEN_SIZE.height/2)];
    [_infoTableView setBackgroundColor:[UIColor clearColor]];
    [_infoTableView setSectionFooterHeight:5.0];
    [_infoTableView setSectionIndexColor:[UIColor whiteColor]];
    [_infoTableView setScrollEnabled:NO];
    [_infoTableView setBackgroundColor:[UIColor whiteColor]];
    [_infoTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.view addSubview:_infoTableView];
}

#pragma mark -UITABLEVIEW DATASOURCE
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _userInfoArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell"];
    UITableViewCell *userinfoCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!userinfoCell) {
        
        userinfoCell =[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
         UIView *headerView =[[UIView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.frame), _headerHeight)];
        [userinfoCell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        UILabel *lblDescription =[[UILabel alloc]initWithFrame:CGRectMake(10, 5, CGRectGetWidth(headerView.frame)-100, _headerHeight-10)];
        [lblDescription setBackgroundColor:[UIColor clearColor]];
        [lblDescription setTextColor:[UIColor grayColor]];
        [lblDescription setFont:[UIFont fontWithName:HELVETICA size:14.0]];
        [lblDescription setText:[_userInfoArray objectAtIndex:indexPath.row]];
        [lblDescription setTextAlignment:NSTextAlignmentRight];
        [headerView addSubview:lblDescription];
        
        UILabel *lblTitle =[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetWidth(_infoTableView.frame)-60, 5, 50, _headerHeight-10)];
        [lblTitle setBackgroundColor:[UIColor clearColor]];
        [lblTitle setTextColor:[UIColor colorWithRed:80.0/255.0 green:127.0/255.0 blue:185.0/255.0 alpha:1.0]];
        [lblTitle setFont:[UIFont fontWithName:HELVETICA_BOLD size:14.0]];
        [lblTitle setText:[_titleArray objectAtIndex:indexPath.row]];
        [lblTitle setTextAlignment:NSTextAlignmentRight];
        [headerView addSubview:lblTitle];
        
        
        UIView *imgLine = [[UIView alloc]initWithFrame:CGRectMake(10, CGRectGetHeight(headerView.frame)-10, CGRectGetWidth(headerView.frame)-20, 1)];
        [imgLine setBackgroundColor:[UIColor colorWithRed:80.0/255.0 green:127.0/255.0 blue:185.0/255.0 alpha:1.0]];
        
        [headerView addSubview:imgLine];
        [headerView setBackgroundColor:[UIColor clearColor]];
        [userinfoCell addSubview:headerView];
        
    }
    return userinfoCell;
}

@end
