//
//  TasksViewController.m
//  SEANJM
//
//  Created by Abdul Kareem on 9/5/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "TasksViewController.h"
#import "TaskCell.h"
#import "Macros.h"
#import "LanguageController.h"
#import "AFNetworking.h"
#import "Utility.h"
#import "UserInformation.h"
#import "MBProgressHUD.h"
#import "TaskListViewController.h"
#import "BTSData.h"
#import "GuidersTaskListViewController.h"


@interface TasksViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,weak)IBOutlet UITableView *tasksTableView;
@property (nonatomic)NSString *routeCellIdentifier;
@property (nonatomic)NSMutableArray *taskArray;
@property (nonatomic)NSMutableArray *iconArray;
@property (nonatomic)NSDictionary *userSelectedRole;
@property (nonatomic)MBProgressHUD *hudview;
@end

@implementation TasksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _taskArray = [NSMutableArray arrayWithArray:@[@"al-ahlam",@"al-Murshiden"]];
    _iconArray = [NSMutableArray arrayWithArray:@[@"ic_tasks",@"ic_guides"]];
    _routeCellIdentifier = @"TaskCell";
    [_tasksTableView registerNib:[UINib nibWithNibName:_routeCellIdentifier bundle:[NSBundle mainBundle]]forCellReuseIdentifier:_routeCellIdentifier];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.topItem.title = [[UserInformation getUserSelectedRoute] objectForKey:@"name"];
    self.navigationController.navigationBar.backItem.title = LocalizedString(@"back");
    
    NSIndexPath *tableSelection = [_tasksTableView indexPathForSelectedRow];
    [_tasksTableView deselectRowAtIndexPath:tableSelection animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark UITableView DataSource

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _taskArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    TaskCell *taskCell =[tableView dequeueReusableCellWithIdentifier:_routeCellIdentifier];
    taskCell.lblTitle.text = LocalizedString([_taskArray objectAtIndex:indexPath.row]);
    [taskCell.btnImage setBackgroundImage:[UIImage imageNamed:[_iconArray objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
    [taskCell.btnImage setContentMode:UIViewContentModeScaleAspectFit];
    [taskCell.btnImage setTag:indexPath.row];
    [taskCell.btnImage addTarget:self action:@selector(leftButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    return taskCell;
}

#pragma mark - TableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row == 0)
    {
        TaskListViewController *objTaskList =[[TaskListViewController alloc]init];
        [self.navigationController pushViewController:objTaskList animated:YES];
    }
    else if(indexPath.row ==1)
    {
        GuidersTaskListViewController *objGuiders =[[GuidersTaskListViewController alloc]init];
        [self.navigationController pushViewController:objGuiders animated:YES];
    }
}

#pragma mark -
#pragma mark - IBActions
-(IBAction)leftButtonTapped:(UIButton *)sender
{

}

@end
