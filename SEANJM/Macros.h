//
//  Macros.h
//  SEANJM
//
//  Created by Randa Al-Sadek on 6/14/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#ifndef SEANJM_Macros_h
#define SEANJM_Macros_h

#define UserDefaults                            [NSUserDefaults standardUserDefaults]
#define NotificationCenter                      [NSNotificationCenter defaultCenter]
#define DispatchEvent(n, o)                     [NotificationCenter postNotificationName:n object:o]
#define AddObserver(id, s, n, o)                [NotificationCenter addObserver:id selector:s name:n object:o]
#define RemoveObserver(n)                       [NotificationCenter removeObserver:n]
#define kStartLocation      @"startCurrentLocation"
#define kStopLocation       @"stopCurrentLocation"

#define customBlue              [UIColor colorWithRed:0.0/255.0 green:59/255.0 blue:150/255.0 alpha:1.0f]
#define customGrey              [UIColor colorWithRed:0.898f green:0.898f blue:0.898f alpha:1.0f]
#define BlueButtons             [UIColor colorWithRed:100/255.0 green:150/255.0 blue:250/255.0 alpha:1.0]
#define HELVETICA @"HelveticaNeue"
#define HELVETICA_BOLD @"HelveticaNeue-Bold"
#define SCREEN_SIZE [UIScreen mainScreen].bounds.size
#define Navigation_StatusBar_Height 64.0

#define kUserSelectedRole @"UserSelectedRole" 
#define kUserSelectedRoute @"UserSelectedRoute"

/****************User information keys *******************/
#define kloginUser      @"loginUser"
#define kofficeNumber   @"officeNumber"
#define kuserFullName   @"userFullName"
#define kuserPhone      @"userPhone"
#define kuserIDs        @"userIDs"

#define kuserGroupId    @"userGroupId"
#define kuserIsGM       @"userIsGM"
#define kuserRoles      @"userRoles"
#define kloginUserId    @"loginUserId"
#define kisSRS          @"isSRS"
#define kIsSuperAdmin   @"IsSuperAdmin"

#define kAdminID        @"41F29A80-14DB-43E7-9C5D-E654B0DA14BB"


/**********BTS CONFIGURATION KEYS*************/
//Colors
#define LightYellowColor [UIColor colorWithRed:253.0/255.0 green:255.0/255.0 blue:208.0/255.0 alpha:1.0]
#define LightRedColor [UIColor colorWithRed:255.0/255.0 green:116.0/255.0 blue:113.0/255.0 alpha:1.0]

/**Webservice Keys**/
#define kgroupId        @"groupId"
#define klocationId     @"locationId"
#define kroleId         @"roleId"
#define krouteId        @"routeId"
#define kuserId         @"userId"
#define kcreator        @"creator"
#define kroundsNumber   @"roundsNumber"

#define kBTSConfigLoaded    @"BTSConfigLoaded"
#define kBTSCoordinatorConfigLoaded    @"BTSCoordinatorConfigLoaded"
#define kBTSCONFIGURATION   @"BTSCONFIGURATION"
#define kBTSCOORDINATORCONFIGURATION   @"BTSCOORDINATORCONFIGURATION"

#define kbtsbuscompanies    @"btsbuscompanies"
#define kbtsbuses           @"btsbuses"
#define kbtslocations       @"btslocations"
#define kbtsroles           @"btsroles"
#define kbtsroutes          @"btsroutes"
#define kbtsusers           @"btsusers"
#define kbtsguiders         @"btsguiders"
#define kroutesbuses        @"routesbuses"
#endif

/*******************BTS ROLE**************/
#define kBTSSenderID        @"0"
#define kBTSReceiverID      @"1"
#define kBTSManagerID       @"2"
#define kBTSCoordinatorID   @"3"

/*******************User**************/

//Manager
#define kBTS_M_AssignedUsersAndGuides @"BTS_M_AssignedUsersAndGuides"
#define kAddnewUser @"AddnewUser"
#define kUpdateUser @"UpdateUser"
#define kAddnewGuider @"AddnewGuider"
#define kUpdateGuider @"UpdateGuider"

//Coordinator
#define kBTS_C_Assignedbuses @"kBTS_C_Assignedbuses"
#define kCoordinatorRoutesWithBusesAndRounds @"CoordinatorRoutesWithBusesAndRounds"
#define kAddnewBus @"AddnewBus"
#define kUpdateBus @"UpdateBus"

//Sender Receiver
#define kBTS_SR_Assignedbuses @"kBTS_SR_Assignedbuses"
#define kSendBus @"SendBus"

//Super Admin
#define kSARAllRoutesReports   @"SARAllRoutesReports"
#define kAllGroups            @"AllGroups"

//Admin
#define kisManagerInReports @"isManagerInReports"
