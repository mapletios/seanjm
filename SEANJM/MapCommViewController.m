    //
//  MapCommViewController.m
//  SEANJM
//
//  Created by Randa Al-Sadek on 7/2/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "MapCommViewController.h"
#import "AFNetworking.h"
#import <ArcGIS/ArcGIS.h>
#import "Macros.h"
#import "LanguageController.h"
#import "Utility.h"
#import "CustomCalloutViewController.h"
#import "ConfigurationManager.h"
#import "ComplainsListViewController.h"

#define basemapService @"http://213.236.53.172/arcgis/rest/services/NJM/BaseMap/MapServer"
#define maintenanceServiceLayer @"http://213.236.53.172/arcgis/rest/services/NJM/SRS/MapServer/"

@interface MapCommViewController ()<AGSLayerDelegate, AGSCalloutDelegate, AGSMapViewTouchDelegate,AGSMapViewLayerDelegate, AGSQueryTaskDelegate> {
    
    __weak IBOutlet AGSMapView *SRSMapView;
    __weak IBOutlet UIButton *btnLocation;
    __weak IBOutlet UIActivityIndicatorView *acivityIndicator;
    NSString *serviceString;
    NSString *latCoordinate;
    NSString *lonCoordinate;
    NSString *defString;
    int layerID;
    int queryCount;     //To query two layers Health & Death
    ConfigurationManager *config;
    
    AGSQueryTask *queryTask;
    AGSQuery *query;
    
    AGSQueryTask *queryTask2;
    AGSQuery *query2;
    
    NSMutableArray *complainsList;
    BOOL isControllerPresent;
}

@end

@implementation MapCommViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    config = [[ConfigurationManager alloc]init];
    
    complainsList = [[NSMutableArray alloc]init];
    
    [SRSMapView setAllowCallout:YES];
    
    [SRSMapView.callout setDelegate:self];
    
    [SRSMapView setTouchDelegate:self];
    [SRSMapView setLayerDelegate:self];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithTitle:LocalizedString(@"back")
                                                                 style:UIBarButtonItemStylePlain
                                                                target:nil
                                                                action:nil];
    
    
    
    [self.navigationItem setBackBarButtonItem:backItem];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshMap) name:@"RefreshMap" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshMapFeatures) name:@"RefreshMapFeatures" object:nil];
    
    if (self.showReport) {
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ic_list"] style:UIBarButtonItemStyleDone target:self action:@selector(showComplainsList)];
        [self.navigationItem setRightBarButtonItem:doneButton];
    }
    else {
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]initWithTitle:LocalizedString(@"done") style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonPressed)];
        [self.navigationItem setRightBarButtonItem:doneButton];
    }
   
    AGSOpenStreetMapLayer *streetLayer = [[AGSOpenStreetMapLayer alloc] init];
    [streetLayer setDelegate:self];
    
    [SRSMapView insertMapLayer:streetLayer atIndex:0];
    
    NSURL* basemapURL = [NSURL URLWithString:basemapService];
    
    AGSTiledMapServiceLayer *tiledLayer = [AGSTiledMapServiceLayer tiledMapServiceLayerWithURL: basemapURL];
    [tiledLayer setDelegate:self];
    
    [SRSMapView insertMapLayer:tiledLayer withName:@"Basemap" atIndex:1];
    
    AGSLayerDefinition *layerDef;
    
    switch (self.mapType) {
        case 0: {       //Maintenance
            layerID = 1;
            defString = [NSString stringWithFormat:@"user_id_creater = '%@' and status <> -1", [UserDefaults objectForKey:kloginUserId]];
            break;
        }
        case 1:case 5: {       //PublicAffairs(Health)
            layerID = 2;
            defString = [NSString stringWithFormat:@"user_id_creater = '%@' and status <> -1", [UserDefaults objectForKey:kloginUserId]];
            break;
        }
        case 2: {       //Death
            layerID = 3;
            defString = [NSString stringWithFormat:@"user_id_create = '%@' and status <> -1", [UserDefaults objectForKey:kloginUserId]];
            break;
        }
        case 3: {       //Transportation
            layerID = 0;
            defString = [NSString stringWithFormat:@"user_id_creater = '%@' and status <> -1", [UserDefaults objectForKey:kloginUserId]];
            break;
        }
            default:
            break;
    }
    
    serviceString = [NSString stringWithFormat:@"%@%d",maintenanceServiceLayer, layerID];
    
    layerDef = [AGSLayerDefinition layerDefinitionWithLayerId:layerID definition:defString];

    AGSFeatureLayer *maintetnanceFeatureLayer = [[AGSFeatureLayer alloc] initWithURL:[NSURL URLWithString:serviceString] mode:AGSFeatureLayerModeOnDemand];
    [maintetnanceFeatureLayer setDelegate:self];
    [maintetnanceFeatureLayer setDefinitionExpression:layerDef.definition];
    [maintetnanceFeatureLayer setOutFields:@[@"*"]];
    [SRSMapView insertMapLayer:maintetnanceFeatureLayer withName:@"Maintenance Layer" atIndex:2];
    
    
    //Add Death Layer(layer 3) if reports button pressed
    if (self.mapType == 5) {
        
        defString = [NSString stringWithFormat:@"user_id_create = '%@' and status <> -1", [UserDefaults objectForKey:kloginUserId]];
        layerDef = [AGSLayerDefinition layerDefinitionWithLayerId:3 definition:defString];

        AGSFeatureLayer *deadComplaintFeatureLayer = [[AGSFeatureLayer alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%d",maintenanceServiceLayer, 3]] mode:AGSFeatureLayerModeOnDemand];
        [deadComplaintFeatureLayer setDelegate:self];
        [deadComplaintFeatureLayer setDefinitionExpression:layerDef.definition];
        [deadComplaintFeatureLayer setOutFields:@[@"*"]];
        [SRSMapView insertMapLayer:deadComplaintFeatureLayer withName:@"Dead Complaint Layer" atIndex:3];
    }
    
    [self mapZoom];
}

-(void)viewWillAppear:(BOOL)animated {
    if ([complainsList count] > 0) {
        [complainsList removeAllObjects];
    }
}

#pragma mark -
#pragma mark IBActions
-(IBAction)zoomToCurrentLocationTapped:(id)sender
{
    [SRSMapView.locationDisplay setAutoPanMode:AGSLocationDisplayAutoPanModeCompassNavigation];
    [SRSMapView.locationDisplay startDataSource];
    SRSMapView.locationDisplay.navigationPointHeightFactor  = 0.8;
    //[mapView.locationDisplay showsPing];
    [btnLocation setSelected:YES];
}

#pragma mark - AGSLayerDelegate
- (void)mapViewDidLoad:(AGSMapView *) mapView {
    
    [mapView.locationDisplay addObserver:self forKeyPath:@"location" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    if ([keyPath isEqualToString:@"location"]) {
        
        //if(mapView.locationDisplay.location.point == nil)
        {
            latCoordinate = [NSString stringWithFormat:@"%f", SRSMapView.locationDisplay.location.point.x];
            lonCoordinate = [NSString stringWithFormat:@"%f", SRSMapView.locationDisplay.location.point.y];
            [btnLocation setSelected:YES];
            
            [SRSMapView.callout showCalloutAt:[AGSPoint pointWithX:SRSMapView.locationDisplay.location.point.x y:SRSMapView.locationDisplay.location.point.y spatialReference:[AGSSpatialReference spatialReferenceWithWKID:102100]] screenOffset:CGPointMake(0, 0) animated:YES];
            SRSMapView.callout.detail =[NSString stringWithFormat:@"Latitude: %0.3f Longitude: %0.3f",SRSMapView.locationDisplay.location.point.x, SRSMapView.locationDisplay.location.point.y];
            SRSMapView.callout.accessoryButtonHidden = YES;
            
        }
    }
}

- (void) layerDidLoad: (AGSLayer*) layer{
    NSLog(@"%s %@", __PRETTY_FUNCTION__, layer.name);
}

- (void) layer : (AGSLayer*) layer didFailToLoadwithError:(NSError*) error {
    NSLog(@"%s Error: %@", __PRETTY_FUNCTION__, error);
}

#pragma mark - AGSMapViewTouchDelegate
-(void)mapView:(AGSMapView *)mapView didClickAtPoint:(CGPoint)screen mapPoint:(AGSPoint *)mappoint features:(NSDictionary *)features {
    NSLog(@"%@", mappoint);
    
    if (self.showReport && [features count] == 0) {
        [SRSMapView.callout removeFromSuperview];
    } else if (!self.showReport && [features count] == 0){
        latCoordinate = [NSString stringWithFormat:@"%f", mappoint.x];
        lonCoordinate = [NSString stringWithFormat:@"%f", mappoint.y];
        [mapView.callout showCalloutAt:[AGSPoint pointWithX:mappoint.x y:mappoint.y spatialReference:[AGSSpatialReference spatialReferenceWithWKID:102100]] screenOffset:CGPointMake(0, 0) animated:YES];
        mapView.callout.detail =[NSString stringWithFormat:@"Latitude: %0.3f Longitude: %0.3f",mappoint.x, mappoint.y];
        mapView.callout.accessoryButtonHidden = YES;
        SRSMapView.callout.customView =nil;
    }
}

#pragma mark - AGSCalloutDelegate
-(BOOL)callout:(AGSCallout *)callout willShowForLocationDisplay:(AGSLocationDisplay *)locationDisplay {
    
    
    callout.title = @"Current Location";
    callout.detail = @"";
    SRSMapView.callout.accessoryButtonHidden = YES;
    
    return YES;
}

-(void)showCalloutAt:(AGSPoint*)mapLocation screenOffset:(CGPoint)screenOffset animated:(BOOL)animated;
{
    
}

-(BOOL)callout:(AGSCallout *)callout willShowForFeature:(id<AGSFeature>)feature layer:(AGSLayer<AGSHitTestable> *)layer mapPoint:(AGSPoint *)mapPoint {
    NSLog(@"%@", mapPoint);
    
    if (self.showReport) {
        AGSGraphic *graphic = (AGSGraphic *)feature;
        NSDictionary *attributes = graphic.allAttributes;
        
        AGSFeatureLayer *flayer = (AGSFeatureLayer *)layer;
        
        //Specify the callout's contents
        NSString *complaintID = (NSString *)[attributes objectForKey:@"complaint_id"];
        NSString *mainType;
        
        switch (flayer.layerId) {
            case 0: {       //Transportation
                mainType = [config getMaintenanceTypeById:[attributes objectForKey:@"main_type_id"] type:@"TransportationTypes"];
            }
                break;
            case 1: {       //Maintenance
                mainType = [config getMaintenanceTypeById:[attributes objectForKey:@"main_type_id"] type:@"MaintenanceTypes"];
            }
                break;
            case 2:
            case 3: {       //PublicAffairs (Health) & Death
                mainType = [config getMaintenanceTypeById:[attributes objectForKey:@"main_type_id"] type:@"HealthTypes"];
            }
                break;
                
            default:
                break;
        }
        
        NSString *date = [config getDateFromSeconds:[[attributes objectForKey:@"time"] doubleValue]/1000];
        
        NSDictionary *statusValues = [UserDefaults objectForKey:@"MaintenanceStatus"];
        NSString *statusName = [statusValues objectForKey:[NSString stringWithFormat:@"%@",[attributes objectForKey:@"status"]]];
        
        CustomCalloutViewController *customCallout = [[CustomCalloutViewController alloc]init];
        customCallout.calloutType = @"SRS";
        customCallout.txt1 = complaintID;
        customCallout.txt2 = mainType;
        customCallout.txt3 = date;
        customCallout.txt4 = statusName;
        
        SRSMapView.callout.customView = customCallout.view;
    }
    else {
        //Specify the callout's contents
        SRSMapView.callout.detail =[NSString stringWithFormat:@"Latitude: %0.3f Longitude: %0.3f",mapPoint.x, mapPoint.y];
        SRSMapView.callout.accessoryButtonHidden = YES;
    }
 
    return YES;
}

- (void) showComplainsList {
    [self createQuery];
}

-(void)refreshMap {
    
    [[SRSMapView mapLayerForName:@"Maintenance Layer"]refresh];
    if ([SRSMapView mapLayerForName:@"Dead Complaint Layer"]) {
        [[SRSMapView mapLayerForName:@"Dead Complaint Layer"]refresh];
    }
}

-(void)refreshMapFeatures {
    [self createQuery];
}

-(void) createQuery {
    NSString *whereClause;
    
    //Query to get complains by specific user
    if (self.mapType == 5) {    //Query both layer Health and Death
        if ([complainsList count] > 0) {
            [complainsList removeAllObjects];
        }
        queryCount = 0;
        queryTask = [[AGSQueryTask alloc]initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@2", maintenanceServiceLayer]]];
        whereClause = [NSString stringWithFormat:@"user_id_creater = '%@' and status <> -1", [UserDefaults objectForKey:kloginUserId]];
        
        queryTask2 = [[AGSQueryTask alloc]initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@3", maintenanceServiceLayer]]];
        
        NSString *whereClause2 = [NSString stringWithFormat:@"user_id_create = '%@' and status <> -1", [UserDefaults objectForKey:kloginUserId]];
        [queryTask2 setDelegate:self];
        
        query2 = [AGSQuery query];
        [query2 setOutFields:@[@"*"]];
        [query2 setWhereClause:whereClause2];
        
        [queryTask2 executeWithQuery:query2];
    }
    else {
        queryTask = [[AGSQueryTask alloc]initWithURL:[NSURL URLWithString:serviceString]];
        whereClause = [NSString stringWithFormat:@"%@ and status <> -1", defString];
    }
    [queryTask setDelegate:self];
    
    query = [AGSQuery query];
    [query setOutFields:@[@"*"]];
    [query setWhereClause:whereClause];
    
    [queryTask executeWithQuery:query];
    [acivityIndicator startAnimating];
}


- (void) doneButtonPressed {
    
    if(!([Utility isValueNilNullOrEmpty:latCoordinate] || [Utility isValueNilNullOrEmpty:lonCoordinate]))
    {
        NSArray *pointDetails = [[NSArray alloc]initWithObjects:latCoordinate, lonCoordinate, nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ReturnMapPoint" object:pointDetails];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)mapZoom {
    AGSEnvelope *envelope = [[AGSEnvelope alloc]initWithXmin:4359746.411056668 ymin:2427017.94681346 xmax:4455412.809961469 ymax:2475482.0700164745 spatialReference:[AGSSpatialReference spatialReferenceWithWKID:102100]];
    
    [SRSMapView zoomToEnvelope:envelope animated:YES];
}

#pragma mark AGSQueryTaskDelegate 

-(void)queryTask:(AGSQueryTask *)queryTask operation:(NSOperation *)op didExecuteWithFeatureSetResult:(AGSFeatureSet *)featureSet {
    
    ComplainsListViewController *view = [[ComplainsListViewController alloc]init];

    if (self.mapType == 5) {
        [complainsList addObjectsFromArray:featureSet.features];
        queryCount++;
        
        if (queryCount == 2) {
            view.complainsArray = complainsList;
            view.complainLayer = layerID;
            
            
            [self pushComplainViewController:view];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadTable" object:complainsList];
            
        }
        
    }
    else {
        view.complainsArray = featureSet.features;
        view.complainLayer = layerID;
        
        [self pushComplainViewController:view];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadTable" object:featureSet.features];

    }
    [acivityIndicator stopAnimating];
}

-(void)queryTask:(AGSQueryTask *)queryTask operation:(NSOperation *)op didFailWithError:(NSError *)error {
    [acivityIndicator stopAnimating];
}

-(void)pushComplainViewController :(id)viewController
{
    isControllerPresent = FALSE;
    for (id controller in self.navigationController.viewControllers) {
        NSLog(@"%@", controller);
        if([controller isKindOfClass:[viewController class]])
        {
            isControllerPresent = YES;
            break;
        }
    }
    if(!isControllerPresent)
        [self.navigationController pushViewController:viewController animated:YES];
}

@end
