//
//  AddGuidersViewController.m
//  SEANJM
//
//  Created by Abdul Kareem on 9/8/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "AddGuidersViewController.h"
#import "AddUserCell.h"
#import "ConfirmationCell.h"
#import "Macros.h"
#import "LanguageController.h"
#import "AFNetworking.h"
#import "Utility.h"
#import "UserInformation.h"
#import "MBProgressHUD.h"
#import "ActionSheetStringPicker.h"
#import "BTSData.h"

@interface AddGuidersViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    ConfirmationCell *footerView;
    NSInteger numberOfRows;
}
@property (nonatomic,weak)IBOutlet UITableView *addUserTableView;
@property (nonatomic)NSString *addUserCellIdentifier;
@property (nonatomic)NSString *addUserFooterIdentifier;
@property (nonatomic)NSMutableArray *addedUserArray;
@property (nonatomic)NSMutableArray *allUserArray;
@property (nonatomic)NSInteger selectedIndex;
@property (nonatomic)NSMutableDictionary *userDic;
@property (nonatomic)NSMutableArray *rowsPlaceholder;
@property (nonatomic)NSDictionary *selectedTask;
@end

@implementation AddGuidersViewController

-(id)initWithSelectedTask :(NSDictionary *)taskdictionary
{
    self =[super init];
    _selectedTask = [NSDictionary dictionaryWithDictionary:taskdictionary];
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    if(_selectedTask)
        _userDic = [NSMutableDictionary dictionaryWithDictionary:_selectedTask];
    else
        _userDic = [NSMutableDictionary dictionary];
    
    _addedUserArray =[NSMutableArray array];
    _rowsPlaceholder =[NSMutableArray arrayWithObjects:@"guiderName",@"guiderMobile",@"selectLocation", nil];
    numberOfRows = 2;
    _addUserCellIdentifier   = @"AddUserCell";
    _addUserFooterIdentifier = @"ConfirmationCell";
    
    [_addUserTableView registerNib:[UINib nibWithNibName:_addUserCellIdentifier bundle:[NSBundle mainBundle]]forCellReuseIdentifier:_addUserCellIdentifier];
    
    [_addUserTableView registerNib:[UINib nibWithNibName:_addUserFooterIdentifier bundle:[NSBundle mainBundle]]forCellReuseIdentifier:_addUserFooterIdentifier];
    
    
    UITapGestureRecognizer *tapGesture =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
    [self loadUsers];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.topItem.title = [[UserInformation getUserSelectedRoute] objectForKey:@"name"];
    self.navigationController.navigationBar.backItem.title = LocalizedString(@"back");
    //[self.navigationItem setRightBarButtonItem:nil];
    
    NSIndexPath *tableSelection = [_addUserTableView indexPathForSelectedRow];
    [_addUserTableView deselectRowAtIndexPath:tableSelection animated:YES];
    //self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}

-(void)loadUsers
{
    _allUserArray = [NSMutableArray arrayWithArray:[BTSData getUsers]];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark IBActions
-(IBAction)sendTapped:(id)sender
{
    
}

-(void)checkInputValuesValidation
{

    if([_userDic objectForKey:@"name"] && [_userDic objectForKey:@"mobile"])
    {
        [footerView.btnAdd setUserInteractionEnabled:YES];
        [footerView.btnAdd setHighlighted:NO];
        if(_selectedTask.count > 0)
            [footerView.btnAdd setTitle:LocalizedString(@"Update") forState:UIControlStateNormal];
        
    }
    else
    {
        [footerView.btnAdd setUserInteractionEnabled:NO];
        [footerView.btnAdd setHighlighted:YES];
    }

}

#pragma mark -
#pragma mark UITableView DataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([_userDic objectForKey:klocationId] && (![[_userDic objectForKey:klocationId] isEqualToString:@"-1"]))
        numberOfRows = 3;
    
    return numberOfRows;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section;
{
    footerView = [tableView dequeueReusableCellWithIdentifier:_addUserFooterIdentifier];
    [footerView.btnAdd addTarget:self action:@selector(addButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [footerView.btnCancel addTarget:self action:@selector(cancelButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [footerView.btnAdd setTitle:LocalizedString(@"add") forState:UIControlStateNormal];
    [footerView.btnCancel setTitle:LocalizedString(@"cancel") forState:UIControlStateNormal];
    
    [footerView.btnAdd setUserInteractionEnabled:NO];
    [footerView.btnAdd setHighlighted:YES];
    
    [footerView.btnAdd.layer setCornerRadius:4.0];
    [footerView.btnAdd setClipsToBounds:YES];
    [footerView.btnCancel.layer setCornerRadius:4.0];
    [footerView.btnCancel setClipsToBounds:YES];
    return footerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    AddUserCell *addUserCell =[tableView dequeueReusableCellWithIdentifier:_addUserCellIdentifier];
    addUserCell.txtUserName.delegate    = self;
    [addUserCell.txtUserDetail setHidden:YES];
    [addUserCell.txtUserName setPlaceholder:LocalizedString([_rowsPlaceholder objectAtIndex:indexPath.row])];
    addUserCell.txtUserName.tag = indexPath.row;
    [addUserCell setEditing:YES];
    
    
    if(indexPath.row == 0)
        [addUserCell.txtUserName setAccessibilityHint:@"name"];
    else if(indexPath.row == 1)
    {
        [addUserCell.txtUserName setAccessibilityHint:@"mobile"];
        [addUserCell.txtUserDetail setKeyboardType:UIKeyboardTypeNumberPad];
    }
    
    
    //This means user selected old task to update
    if(_selectedTask.count > 0)
    {
        if(indexPath.row ==0)
            [addUserCell.txtUserName setText:[_userDic objectForKey:@"name"]];
        else if(indexPath.row ==1)
            [addUserCell.txtUserName setText:LocalizedString([BTSData getRoleNameWithRoleId:[_userDic objectForKey:kroleId]])];
        else if(indexPath.row ==2)
            [addUserCell.txtUserName setText:[_userDic objectForKey:@"locationName"]];
    }
    
    return addUserCell;
}

#pragma mark - TableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section;
{
    return 80.0;
}

#pragma mark -
#pragma mark - IBActions

-(IBAction)addButtonTapped:(UIButton *)sender
{
    [_userDic setObject:[[UserInformation getUserSelectedRoute] objectForKey:@"id"] forKey:krouteId];
    [_userDic setObject:[Utility getcurrentDateOnly] forKey:@"creationDate"];
    [_userDic setObject:[UserInformation getUserId] forKey:kcreator];
    [_userDic setObject:[UserInformation getGroupID] forKey:kgroupId];
    [_userDic setObject:@"0" forKey:@"addItem"];
    
    
    //Update User
    if(_selectedTask)
    {
        [UserDefaults setObject:_userDic forKey:kUpdateGuider];
        [UserDefaults synchronize];
    }
    //Add new user
    else
    {
        [UserDefaults setObject:_userDic forKey:kAddnewGuider];
        [UserDefaults synchronize];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
    //NSLog(@"addind dictionary %@",_userDic);
}

-(IBAction)cancelButtonTapped:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITextField Delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField.tag ==1)
        [textField setKeyboardType:UIKeyboardTypeNumberPad];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    textField.text = [Utility replaceArabicNumbers:textField.text];
    if(textField.text.length == 0)
    {
        [_userDic removeObjectForKey:textField.accessibilityHint];
        return;
    }
    
    if(textField.tag == 1)
    {
        if(![self checkNumbersValidation:textField])
            textField.text = @"";
        
        if([self checkNumbersValidation:textField])
            [_userDic setObject:textField.text forKey:textField.accessibilityHint];
        else
            [_userDic removeObjectForKey:textField.accessibilityHint];
    }
    else
        [_userDic setObject:textField.text forKey:textField.accessibilityHint];
    
    
    
    [self checkInputValuesValidation];
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField.text.length <= 1)
    {
        [_userDic removeObjectForKey:textField.accessibilityHint];
        [self checkInputValuesValidation];
        
    }
    
    return YES;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    return YES;
}

#pragma mark Phone No Validation
-(BOOL)checkNumbersValidation :(UITextField *)phoneNumber
{
    if(phoneNumber.text.length > 0)
    {
        NSCharacterSet *numbersOnly = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:phoneNumber.text];
        return [numbersOnly isSupersetOfSet:characterSetFromTextField];
    }
    
    return NO;
}
- (BOOL)validatePhone:(NSString *)phoneNumber
{
    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    return [phoneTest evaluateWithObject:phoneNumber];
}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

@end
