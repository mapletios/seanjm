//
//  HomeCommViewController.m
//  SEANJM
//
//  Created by Randa Al-Sadek on 6/21/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "HomeCommViewController.h"
#import "MaintenanceOptionsViewController.h"
#import "PublicAffairsOptionsViewController.h"
#import "TransportationOptionsViewController.h"
#import "LanguageController.h"
#import "Macros.h"
#import "Utility.h"

@interface HomeCommViewController ()

@end

@implementation HomeCommViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithTitle:LocalizedString(@"back")
                                                                 style:UIBarButtonItemStylePlain
                                                                target:self
                                                                action:nil];
    [self.navigationItem setBackBarButtonItem:backItem];
    [self.navigationController setNavigationBarHidden:NO];
    [Utility setNavigationBarProperties:self.navigationController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)maintenanceButtonPressed:(id)sender {
    MaintenanceOptionsViewController *view = [[MaintenanceOptionsViewController alloc]init];
    [self.navigationController pushViewController:view animated:YES];
}

- (IBAction)publicAffairsButtonPressed:(id)sender {
    PublicAffairsOptionsViewController *view = [[PublicAffairsOptionsViewController alloc]init];
    [self.navigationController pushViewController:view animated:YES];
}

- (IBAction)transportButtonPressed:(id)sender {
    TransportationOptionsViewController *view = [[TransportationOptionsViewController alloc]init];
    [self.navigationController pushViewController:view animated:YES];
}

@end
