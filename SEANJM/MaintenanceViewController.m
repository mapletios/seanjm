//
//  MaintenanceViewController.m
//  SEANJM
//
//  Created by Randa Al-Sadek on 6/20/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "MaintenanceViewController.h"
#import "MapCommViewController.h"
#import "ConfigurationManager.h"
#import "ActionSheetStringPicker.h"
#import "Macros.h"
#import "LanguageController.h"
#import "Utility.h"
#import <UIKit/UIKit.h>
#import "PNTToolbar.h"

@interface MaintenanceViewController ()<UITextFieldDelegate, UITextViewDelegate> {
    __weak IBOutlet UITextField *comTypeTxt;
    __weak IBOutlet UITextField *areaTxt;
    __weak IBOutlet UITextField *faultTypeTxt;
    __weak IBOutlet UITextField *usernameTxt;
    __weak IBOutlet UITextField *phoneTxt;
    __weak IBOutlet UITextView *commentsTxtView;
    __weak IBOutlet UILabel *pointLatLbl;
    __weak IBOutlet UILabel *pointLonLbl;
    __weak IBOutlet UITextField *pointLatTxt;
    __weak IBOutlet UITextField *pointLonTxt;
    __weak IBOutlet UIButton *mapButton;
    __weak IBOutlet UIButton *sendButton;
    __weak IBOutlet UIScrollView *scrollView;
    __weak IBOutlet UIActivityIndicatorView *activityIndicator;
    
    NSMutableArray  *inputAccessoryArray;
    ConfigurationManager *config;
    NSMutableArray *mapFeature;
}

@end

@implementation MaintenanceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    tap.cancelsTouchesInView = NO;
    
    [self.view addGestureRecognizer:tap];

    [self.navigationItem setTitle:LocalizedString(@"maintenanceComplain")];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(returnMapPoint:) name:@"ReturnMapPoint" object:nil];
    
    config = [[ConfigurationManager alloc]init];
    
    [Utility setFormsWithPointLat:pointLatLbl pointLon:pointLonLbl mapButton:mapButton sendButton:sendButton username:usernameTxt userPhone:phoneTxt];
    [Utility setTextViewBorder:commentsTxtView];
    
    [Utility setScrollViewSize:scrollView];
    
    [Utility setWhiteShadowToAllLabels:scrollView];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    inputAccessoryArray = [NSMutableArray array];
    for (id subview in scrollView.subviews) {
        if([subview isKindOfClass:[UITextField class]] || [subview isKindOfClass:[UITextView class]])
        {
            [inputAccessoryArray addObject:subview];
        }
    }
    
    PNTToolbar *toolbar = [PNTToolbar defaultToolbar];
    toolbar.navigationButtonsTintColor = [UIColor blueColor];
    toolbar.mainScrollView = scrollView;
    toolbar.inputFields = inputAccessoryArray;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationItem setTitle:LocalizedString(@"maintenanceComplain")];
    [self.navigationController setNavigationBarHidden:NO];
    [Utility applyBlueRoundedButton:sendButton];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
   
    if (textField == comTypeTxt)
    {
        [self.view endEditing:YES];
        
        NSArray *categoriesNamesArray = [config getMaintenanceCategories];
        
        [ActionSheetStringPicker showPickerWithTitle:@"" rows:categoriesNamesArray initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
            
            textField.text = selectedValue;
            
        } cancelBlock:nil origin:textField];
    }
    else if (textField == faultTypeTxt)
    {
        [self.view endEditing:YES];
        
        NSArray *subCategoriesNamesArray = [config getSubMaintenanceNames:comTypeTxt.text];
        
        [ActionSheetStringPicker showPickerWithTitle:@"" rows:subCategoriesNamesArray initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
            
            textField.text = selectedValue;
            
        } cancelBlock:nil origin:textField];
    }
    else if (textField == areaTxt) {
        [self.view endEditing:YES];
        
        NSArray *regionsArray = [config getRegions];
        
        [ActionSheetStringPicker showPickerWithTitle:@"" rows:regionsArray initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
            
            textField.text = selectedValue;
            
        } cancelBlock:nil origin:textField];
    }
    else {
        [scrollView setContentOffset:CGPointMake(0, CGRectGetMinY(textField.frame)-5) animated:YES];
        return YES;
    }
    
    return NO;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [scrollView setContentOffset:CGPointMake(0, CGRectGetMinY(textView.frame)-5) animated:YES];
}
#pragma mark -
#pragma mark - Keyboard Delegate

-(void)keyboardWillHide:(NSNotification *)notification
{
    [scrollView setContentOffset:CGPointZero animated:YES];
}

- (IBAction)sendButtonPressed:(id)sender {
    
    if(![Utility checkInternetConnection])
        return;
    
    else if(![Utility isCurrentLocationAvailable])
    {
        [Utility showAlertViewWithTitle:LocalizedString(@"locationservice") message:LocalizedString(@"checklocationservice") cancelButtonTile:LocalizedString(@"ok")];
        return;
    }
    
    if ([areaTxt.text length] == 0 || [faultTypeTxt.text length] == 0) {
        [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"emptyFields") cancelButtonTile:LocalizedString(@"ok")];
    }
    else if ([pointLatTxt.text length] == 0 || [pointLonTxt.text length] == 0) {
        [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"emptyMapPoint") cancelButtonTile:LocalizedString(@"ok")];
    }
    else {
        NSDictionary *category = [config getMaintenanceTypeByName:comTypeTxt.text];
        NSDictionary *subCategory = [config getMaintenanceSubTypeByName:faultTypeTxt.text];
        
        NSArray *data = [[NSArray alloc]initWithObjects:areaTxt.text, (NSNumber *)[category objectForKey:@"id"], (NSNumber *)[subCategory objectForKey:@"id"],pointLatTxt.text, pointLonTxt.text, nil];
        NSArray *dataKeys = [[NSArray alloc]initWithObjects:@"region", @"categoryId", @"subCategoryId", @"xPosition", @"yPosition", nil];
        NSMutableDictionary *complaintDic = [[NSMutableDictionary alloc]initWithObjects:data forKeys:dataKeys];
        
        if ([usernameTxt.text length] > 0)
            [complaintDic setObject:usernameTxt.text forKey:@"complaintName"];
        if ([phoneTxt.text length] > 0)
            [complaintDic setObject:phoneTxt.text forKey:@"complaintMobile"];
        if ([commentsTxtView.text length] > 0)
            [complaintDic setObject:commentsTxtView.text forKey:@"comments"];
        
        [activityIndicator setHidden:NO];
        [activityIndicator startAnimating];
        
        [config sendMaintenanceComplaint:complaintDic completion:^{
            [self resetView];
            [Utility showAlertViewWithTitle:LocalizedString(@"success") message:LocalizedString(@"successMessage") cancelButtonTile:LocalizedString(@"ok")];
            [activityIndicator stopAnimating];
            [self.navigationController popViewControllerAnimated:YES];
            
        }failure:^{
            [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"reportNotSent") cancelButtonTile:LocalizedString(@"ok")];
            [activityIndicator stopAnimating];
        }];
    }
}

- (IBAction)mapButtonPressed:(id)sender {
    
    if(![Utility checkInternetConnection])
        return;
    
    MapCommViewController *mapView = [[MapCommViewController alloc]init];
    mapView.mapType = 0;
    mapView.showReport = NO;
    [self.navigationController pushViewController:mapView animated:YES];
}

- (void)returnMapPoint:(NSNotification *)notification {
    mapFeature = notification.object;
    pointLatTxt.text = [mapFeature objectAtIndex:0];
    pointLonTxt.text = [mapFeature objectAtIndex:1];
}

- (void)resetView {
    comTypeTxt.text = @"";
    areaTxt.text = @"";
    faultTypeTxt.text = @"";
    commentsTxtView.text = @"";
}


-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

@end
