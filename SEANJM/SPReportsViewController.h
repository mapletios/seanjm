//
//  SPReportsViewController.h
//  SEANJM
//
//  Created by Abdul Kareem on 9/18/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPReportsViewController : UIViewController
-(id)initWithSelectedRoute :(NSDictionary *)selected_route isFromRoute :(BOOL)isfrom_route;
@end
