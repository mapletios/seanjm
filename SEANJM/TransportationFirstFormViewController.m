//
//  TransportationFirstFormViewController.m
//  SEANJM
//
//  Created by Randa Al-Sadek on 7/23/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "TransportationFirstFormViewController.h"
#import "MapCommViewController.h"
#import "ActionSheetStringPicker.h"
#import "Utility.h"
#import "ConfigurationManager.h"
#import "LanguageController.h"
#import "Macros.h"
#import "PNTToolbar.h"

@interface TransportationFirstFormViewController ()<UITextFieldDelegate, UITextViewDelegate> {
    
    __weak IBOutlet UILabel *pointLatLbl;
    __weak IBOutlet UILabel *pointLonLbl;
    __weak IBOutlet UITextField *pointLatTxt;
    __weak IBOutlet UITextField *pointLonTxt;
    __weak IBOutlet UIScrollView *scrollView;
    __weak IBOutlet UILabel *faultLbl;
    __weak IBOutlet UITextField *faultTxt;
    __weak IBOutlet UILabel *busNumLbl;
    __weak IBOutlet UITextField *busNumTxt;
    __weak IBOutlet UILabel *companyNameLbl;
    __weak IBOutlet UITextField *companyNameTxt;
    __weak IBOutlet UILabel *reportedByLbl;
    __weak IBOutlet UITextField *reportedByTxt;
    __weak IBOutlet UILabel *reportedPhoneLbl;
    __weak IBOutlet UITextField *reportedPhoneTxt;
    __weak IBOutlet UILabel *driveLbl;
    __weak IBOutlet UITextField *driverTxt;
    __weak IBOutlet UILabel *leaderLbl;
    __weak IBOutlet UITextField *leaderTxt;
    __weak IBOutlet UILabel *changeBusLbl;
    __weak IBOutlet UIButton *changeBusCheck;
    __weak IBOutlet UILabel *pplAvailLbl;
    __weak IBOutlet UIButton *pplAvailCheck;
    __weak IBOutlet UILabel *commentsLbl;
    __weak IBOutlet UITextView *commentsTxtView;
    __weak IBOutlet UIButton *sendButton;
    __weak IBOutlet UIButton *mapButton;
    __weak IBOutlet UIActivityIndicatorView *activityIndicator;
    
    NSMutableArray  *inputAccessoryArray;
    
    ConfigurationManager *config;
    NSMutableArray *mapFeature;
    NSNumber *isBusChanged;
    NSNumber *isHasPilgrims;
    CGRect contentRect;
}

@end

@implementation TransportationFirstFormViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    tap.cancelsTouchesInView = NO;
    
    [self.view addGestureRecognizer:tap];
    
    NSString *title;
    switch (self.formType) {
        case 1:
            title = LocalizedString(@"busFaultsComplain");
            break;
        case 2:
            title = LocalizedString(@"busGuideComplain");
            break;
        default:
            break;
    }
    
    [self.navigationItem setTitle:title];

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(returnMapPoint:) name:@"ReturnMapPoint" object:nil];
    
    [Utility setFormsWithPointLat:pointLatLbl pointLon:pointLonLbl mapButton:mapButton sendButton:sendButton username:reportedByTxt userPhone:reportedPhoneTxt];
    
    [Utility setTextViewBorder:commentsTxtView];
    
    config = [[ConfigurationManager alloc]init];
    
    contentRect = CGRectZero;
    for (UIView *view in scrollView.subviews) {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    contentRect.size.height +=20;
    scrollView.contentSize = contentRect.size;
    
    isBusChanged = @0;
    isHasPilgrims = @0;
    
    [Utility setWhiteShadowToAllLabels:scrollView];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    inputAccessoryArray = [NSMutableArray array];
    for (id subview in scrollView.subviews) {
        if([subview isKindOfClass:[UITextField class]] || [subview isKindOfClass:[UITextView class]])
        {
            [inputAccessoryArray addObject:subview];
        }
    }
    
    PNTToolbar *toolbar = [PNTToolbar defaultToolbar];
    toolbar.navigationButtonsTintColor = [UIColor blueColor];
    toolbar.mainScrollView = scrollView;
    toolbar.inputFields = inputAccessoryArray;
    
    
}
-(void)viewDidDisappear:(BOOL)animated
{
    if(![self.navigationController.viewControllers containsObject:self])
        DispatchEvent(kStopLocation, nil);
}


-(void)viewWillAppear:(BOOL)animated {
    
    DispatchEvent(kStartLocation, nil);
    
    if (self.formType == 2) {
        [faultLbl removeFromSuperview];
        [faultTxt removeFromSuperview];
    }
    [Utility applyBlueRoundedButton:sendButton];
}

- (void)viewDidLayoutSubviews {
    
    scrollView.contentSize = contentRect.size;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma UITextFieldDelegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField == faultTxt) {
        [self.view endEditing:YES];
        
        NSArray *faultTypesNames = [config getTransportationSubTypes:@1];
        [ActionSheetStringPicker showPickerWithTitle:@"" rows:faultTypesNames initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
            
            textField.text = selectedValue;
            
        } cancelBlock:nil origin:textField];
    }
    else if (textField == companyNameTxt) {
        [self.view endEditing:YES];
        
        NSArray *busCompaniesNames = [config getBusCompanieaNames];
        [ActionSheetStringPicker showPickerWithTitle:@"" rows:busCompaniesNames initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
            
            textField.text = selectedValue;
            
        } cancelBlock:nil origin:textField];
    }
    else {

        [scrollView setContentOffset:CGPointMake(0, CGRectGetMinY(textField.frame)-5) animated:YES];
        return YES;
    }
    
    return NO;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [scrollView setContentOffset:CGPointMake(0, CGRectGetMinY(textView.frame)-5) animated:YES];
}

#pragma mark -
#pragma mark - Keyboard Delegate


-(void)keyboardWillHide:(NSNotification *)notification
{
    [scrollView setContentOffset:CGPointZero animated:YES];
}

- (IBAction)sendButtonPressed:(id)sender {
    
    if(![Utility checkInternetConnection])
        return;
    
    else if(![Utility isCurrentLocationAvailable])
    {
        [Utility showAlertViewWithTitle:LocalizedString(@"locationservice") message:LocalizedString(@"checklocationservice") cancelButtonTile:LocalizedString(@"ok")];
        return;
    }
    
    if (self.formType == 2) {
        faultTxt.text = @"إرشاد";
    }
    
    if ([faultTxt.text length] == 0 || [busNumTxt.text length] == 0 || [companyNameTxt.text length] == 0 || [driverTxt.text length] == 0 || [leaderTxt.text length] == 0) {
        
        [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"emptyFields") cancelButtonTile:LocalizedString(@"ok")];
    }
    else if ([pointLatTxt.text length] == 0 || [pointLonTxt.text length] == 0) {
        [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"emptyMapPoint") cancelButtonTile:LocalizedString(@"ok")];
    }
    else {
        NSDictionary *subCategory = [config getTransSubTypeByName:faultTxt.text];
        NSDictionary *busObject = [config getBusObjectByName:companyNameTxt.text];
        
        NSArray *data = [[NSArray alloc]initWithObjects:[NSNumber numberWithInt:self.formType], (NSNumber *)[subCategory objectForKey:@"id"], (NSNumber *)[busObject objectForKey:@"id"], busNumTxt.text, pointLatTxt.text, pointLonTxt.text, driverTxt.text, leaderTxt.text, isBusChanged, isHasPilgrims, nil];
        
        NSArray *dataKeys = [[NSArray alloc]initWithObjects:@"mainTypeId", @"subTypeId", @"companyID", @"busID", @"xPosition", @"yPosition", @"driverName", @"guiderName", @"isBusChanged", @"isHasPilgrims", nil];
        
        NSMutableDictionary *complaintDic = [[NSMutableDictionary alloc]initWithObjects:data forKeys:dataKeys];
        
        if ([reportedByTxt.text length] > 0)
            [complaintDic setObject:reportedByTxt.text forKey:@"complaintName"];
        if ([reportedPhoneTxt.text length] > 0)
            [complaintDic setObject:reportedPhoneTxt.text forKey:@"complaintMobile"];
        if ([commentsTxtView.text length] > 0)
            [complaintDic setObject:commentsTxtView.text forKey:@"comments"];

        [activityIndicator setHidden:NO];
        [activityIndicator startAnimating];
        
        [config sendTransportationComplaint:complaintDic completion:^{
            [self resetView];
            [Utility showAlertViewWithTitle:LocalizedString(@"success") message:LocalizedString(@"successMessage") cancelButtonTile:LocalizedString(@"ok")];
            [activityIndicator stopAnimating];
            [self.navigationController popViewControllerAnimated:YES];

        }failure:^{
            [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"reportNotSent") cancelButtonTile:LocalizedString(@"ok")];
            [activityIndicator stopAnimating];
        }];
    }

}

- (IBAction)checkButtonPressed:(id)sender {
    if ((UIButton *)sender == changeBusCheck && [isBusChanged isEqualToNumber:@0]) {
        isBusChanged = [Utility setButtonChecked:isBusChanged button:(UIButton *)sender];
    }
    else if ((UIButton *)sender == changeBusCheck && [isBusChanged isEqualToNumber:@1]) {
        isBusChanged = [Utility setButtonUnChecked:isBusChanged button:(UIButton *)sender];
    }
    
    if ((UIButton *)sender == pplAvailCheck && [isHasPilgrims isEqualToNumber:@0]) {
        isHasPilgrims = [Utility setButtonChecked:isHasPilgrims button:(UIButton *)sender];
    }
    else if ((UIButton *)sender == pplAvailCheck && [isHasPilgrims isEqualToNumber:@1]) {
        isHasPilgrims = [Utility setButtonUnChecked:isHasPilgrims button:(UIButton *)sender];
    }
}

- (IBAction)mapButtonPressed:(id)sender {
    
    if(![Utility checkInternetConnection])
        return;
    
    MapCommViewController *mapView = [[MapCommViewController alloc]init];
    mapView.mapType = 3;
    mapView.showReport = NO;
    [self.navigationController pushViewController:mapView animated:YES];
}

- (void)returnMapPoint:(NSNotification *)notification {
    mapFeature = notification.object;
    pointLatTxt.text = [mapFeature objectAtIndex:0];
    pointLonTxt.text = [mapFeature objectAtIndex:1];
}

- (void)resetView {
    faultTxt.text = @"";
    commentsTxtView.text = @"";
    busNumTxt.text = @"";
    driverTxt.text = @"";
    leaderTxt.text = @"";
    companyNameTxt.text = @"";
    
    isBusChanged = @1;
    isHasPilgrims = @1;
    
    [self checkButtonPressed:nil];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

@end
