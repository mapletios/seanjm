//
//  MaintenanceOptionsViewController.m
//  SEANJM
//
//  Created by Randa Al-Sadek on 9/6/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "MaintenanceOptionsViewController.h"
#import "LanguageController.h"
#import "Utility.h"
#import "MapCommViewController.h"
#import "MaintenanceViewController.h"

@interface MaintenanceOptionsViewController ()

@end

@implementation MaintenanceOptionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithTitle:LocalizedString(@"back")
                                                                 style:UIBarButtonItemStylePlain
                                                                target:nil
                                                                action:nil];
    
    [self.navigationItem setBackBarButtonItem:backItem];
    
    [self.navigationItem setTitle:LocalizedString(@"maintenanceComplain")];
    
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)maintenanceButtonPressed:(id)sender {
    MaintenanceViewController *view = [[MaintenanceViewController alloc]init];
    [self.navigationController pushViewController:view animated:YES];
}

- (IBAction)reportsButtonPressed:(id)sender {
    if(![Utility checkInternetConnection])
        return;
    
    MapCommViewController *mapView = [[MapCommViewController alloc]init];
    mapView.mapType = 0;
    mapView.showReport = YES;
    [self.navigationController pushViewController:mapView animated:YES];

}

@end
