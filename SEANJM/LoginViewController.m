//
//  LoginViewController.m
//  SEANJM
//
//  Created by Randa Al-Sadek on 6/11/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "LoginViewController.h"
#import "HomeViewController.h"
#import "AFNetworking.h"
#import "Macros.h"
#import "LanguageController.h"
#import "Utility.h"
#import "PNTToolbar.h"
#import "ConfigurationManager.h"

//#define loginService @"http://213.236.53.172/snsrs/restserviceimpl.svc/loginv1"
#define loginService @"http://213.236.53.172/snbts/restserviceimpl.svc/loginBTSExtended"
@interface LoginViewController ()<UITextFieldDelegate> {
    
    __weak IBOutlet UILabel *usernameLbl;
    __weak IBOutlet UILabel *passwordLbl;
    __weak IBOutlet UITextField *usernameTxt;
    __weak IBOutlet UITextField *passwordTxt;
    __weak IBOutlet UIActivityIndicatorView *activityIndicator;
    __weak IBOutlet UIScrollView *scrollview;
    __weak IBOutlet UIButton *loginButton;
    NSMutableArray  *inputAccessoryArray;
    ConfigurationManager *config;
    
    NSInteger success;
}

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    usernameLbl.text = LocalizedString(@"username");
    passwordLbl.text = LocalizedString(@"password");
    loginButton.titleLabel.text = LocalizedString(@"signIn");
    
//    usernameTxt.text = @"ziad";
//    passwordTxt.text = @"1234";
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
    success = 0;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [Utility setWhiteShadowToAllLabels:scrollview];
    
    inputAccessoryArray = [NSMutableArray array];
    for (id subview in scrollview.subviews) {
        if([subview isKindOfClass:[UITextField class]] || [subview isKindOfClass:[UITextView class]])
        {
            [inputAccessoryArray addObject:subview];
        }
    }
    
    PNTToolbar *toolbar = [PNTToolbar defaultToolbar];
    toolbar.navigationButtonsTintColor = [UIColor blueColor];
    toolbar.mainScrollView = scrollview;
    toolbar.inputFields = inputAccessoryArray;
}

-(void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loginButtonPressed:(id)sender {
    
    if([AFNetworkReachabilityManager sharedManager].reachable)
    {
        if (![[UserDefaults objectForKey:@"SRSConfigLoaded"] isEqualToString: @"1"] || ![[UserDefaults objectForKey:@"TCSConfigLoaded"] isEqualToString: @"1"]) {
            
            config = [[ConfigurationManager alloc]init];
            [self loadFaultCategories];
            
            [activityIndicator startAnimating];
            
            
            [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        }

        if([[usernameTxt text] isEqualToString:@""] || [[passwordTxt text] isEqualToString:@""] ) {
            [self alertStatus:LocalizedString(@"enterUsername") :LocalizedString(@"signInFailed") :0];
        }
        else {
        
            [activityIndicator setHidden:NO];
            [activityIndicator startAnimating];
        
            NSDictionary *parameters = @{@"username": usernameTxt.text,
                                         @"password": passwordTxt.text};
            
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            manager.requestSerializer = [AFJSONRequestSerializer serializer];
            [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            
            [manager POST:loginService parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
                [activityIndicator stopAnimating];
            
                NSLog(@"JSON: %@", responseObject);
            
                if([responseObject objectForKey:@"Result"] && (![[responseObject objectForKey:@"Result"] isKindOfClass:[NSNull class]]))
                {
                    NSDictionary *resultArray = [NSDictionary dictionaryWithDictionary:[responseObject objectForKey:@"Result"]];
            
                    if ([resultArray count] > 1) {
                
                        [self saveResponseData:resultArray];
                        
                        HomeViewController *view = [[HomeViewController alloc]init];
                        UINavigationController *navigation = [[UINavigationController alloc]initWithRootViewController:view];
                        [self presentViewController:navigation animated:YES completion:nil];
                        
                        NSLog(@"user defaults %@",[UserDefaults objectForKey:kuserIsGM]);
                    }
            
                }
                else {
                    [Utility showAlertViewWithTitle:LocalizedString(@"error")  message:LocalizedString(@"enterValidUsername") cancelButtonTile:LocalizedString(@"ok")];
                }
            
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                NSLog(@"Error: %@", error);
                [activityIndicator stopAnimating];
            }];

        }
    }
    else
        [Utility showAlertViewWithTitle:@"" message:LocalizedString(@"nointernet") cancelButtonTile:@"Ok"];
}


- (void) alertStatus:(NSString *)msg :(NSString *)title :(int) tag
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:LocalizedString(@"ok")
                                              otherButtonTitles:nil, nil];
    alertView.tag = tag;
    [alertView show];
}

- (void) saveResponseData: (NSDictionary *)responseData {
    
    [UserDefaults setObject:[responseData objectForKey:@"officeid"] forKey:kofficeNumber];
    [UserDefaults setObject:usernameTxt.text forKey:kloginUser];
    [UserDefaults setObject:[responseData objectForKey:@"name"] forKey:kuserFullName];
    [UserDefaults setObject:[responseData objectForKey:@"phone"] forKey:kuserPhone];
    [UserDefaults setObject:[responseData objectForKey:@"allUsers"] forKey:kuserIDs];
    [UserDefaults setObject:[responseData objectForKey:@"groupId"] forKey:kuserGroupId];
    [UserDefaults setBool:([[responseData objectForKey:@"isGM"]  isEqual: @1])? YES:NO forKey:kuserIsGM];
    [UserDefaults setObject:[responseData objectForKey:@"roles"] forKey:kuserRoles];
    [UserDefaults setObject:[responseData objectForKey:@"id"] forKey:kloginUserId];
    [UserDefaults setObject:[responseData objectForKey:@"isSRS"] forKey:kisSRS];
    [UserDefaults setObject:[responseData objectForKey:@"isSuperAdmin"] forKey:kIsSuperAdmin];
    [UserDefaults synchronize];

    NSLog(@"%@",[UserDefaults objectForKey:kuserIsGM]);
}

#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


- (void)keyboardWillShow:(NSNotification *)notification
{
    CGPoint keyboardPoint = [[[notification userInfo]objectForKey:UIKeyboardFrameEndUserInfoKey]CGRectValue].origin;
    
    for (UIView *view in scrollview.subviews) {
        if (view.isFirstResponder) {
            CGFloat viewY = CGRectGetMaxY(view.frame);
            
            CGFloat difference = (keyboardPoint.y - 64.0f) - viewY;
            if (difference < 10) {
                
                if (difference >= 0) {
                    [scrollview setContentOffset:CGPointMake(0, difference) animated:YES];
                } else {
                    [scrollview setContentOffset:CGPointMake(0, -difference + 10.0f) animated:YES];
                }
            }
        }
    }
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [scrollview setContentOffset:CGPointZero animated:YES];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

- (void)loadFaultCategories {

    [config loadTCSConfiguration:^{
        [self stopActivityIndicator];
        [UserDefaults setObject:@"1" forKey:@"TCSConfigLoaded"];
    }failure:^{
        [self stopActivityIndicator];
        [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"failLoadConfig") cancelButtonTile:LocalizedString(@"ok")];
        
    }];
    
    [config loadSRSConfiguration:^{
        [self stopActivityIndicator];
        [UserDefaults setObject:@"1" forKey:@"SRSConfigLoaded"];
    }failure:^{
        [self stopActivityIndicator];
        [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"failLoadConfig") cancelButtonTile:LocalizedString(@"ok")];
    }];
    [UserDefaults synchronize];
    
}

- (void)stopActivityIndicator {
    [activityIndicator stopAnimating];
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
}

@end
