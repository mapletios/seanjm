//
//  Utility.m
//  SEANJM
//
//  Created by Randa Al-Sadek on 7/23/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "Utility.h"
#import "Macros.h"
#import "LanguageController.h"
#import "AFNetworkReachabilityManager.h"
@implementation Utility

+ (void)setTextViewBorder: (UITextView *)textView {
    UIColor *grey = customGrey;
    [textView.layer setBorderColor:[grey CGColor]];
    [textView.layer setBorderWidth:1.0];
    [textView.layer setCornerRadius:6];
}

+ (void)showAlertViewWithTitle: (NSString *)title message:(NSString *)message cancelButtonTile:(NSString *)cancelButtonTitle {
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:cancelButtonTitle otherButtonTitles:nil, nil];
    [alert show];
}

+ (void)setFormsWithPointLat: (UILabel *)pointLat pointLon:(UILabel *)pointLon mapButton:(UIButton *)mapButton sendButton:(UIButton *)sendButton username:(UITextField *)username userPhone:(UITextField *)userPhone {
    
    pointLat.text = LocalizedString(@"latitude");
    pointLon.text = LocalizedString(@"longitude");
    mapButton.titleLabel.text = LocalizedString(@"map");
    sendButton.titleLabel.text = LocalizedString(@"send");
    
    if (username != nil && userPhone !=nil) {
        username.text = [UserDefaults objectForKey:@"userFullName"];
        userPhone.text = [UserDefaults objectForKey:@"userPhone"];
    }
}

+ (NSNumber *)setButtonChecked:(NSNumber *)option button:(UIButton *)button{
    UIImage *checked = [UIImage imageNamed:@"checked"];
    [button setBackgroundImage:checked forState:UIControlStateNormal];
    option = @1;
    return option;
}

+ (NSNumber *)setButtonUnChecked:(NSNumber *)option button:(UIButton *)button {
    UIImage *unchecked = [UIImage imageNamed:@"unchecked"];
    option = @0;
    [button setBackgroundImage:unchecked forState:UIControlStateNormal];
    return  option;
}

+(void)applyBlueRoundedButton :(id)button
{
    [[button layer] setCornerRadius:4.0];
    [button setBackgroundColor:[UIColor colorWithRed:100/255.0 green:150/255.0 blue:250/255.0 alpha:1.0]];
    [[button layer] setBorderWidth:1.0];
        [[button layer] setBorderColor:[UIColor darkGrayColor].CGColor];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [[button layer] setMasksToBounds:YES];
    
    [[button titleLabel] setFont:[UIFont fontWithName:HELVETICA_BOLD size:16.0]];
}

+(void)setScrollViewSize :(UIScrollView *)scrollview
{
    CGRect contentRect = CGRectZero;
    for (UIView *view in scrollview.subviews) {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    contentRect.size.height +=20;
    scrollview.contentSize = contentRect.size;
    [scrollview setShowsVerticalScrollIndicator:NO];
    [scrollview setShowsHorizontalScrollIndicator:NO];
}

+(void)setNavigationBarProperties :(UINavigationController *)navigation
{
    
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[ver objectAtIndex:0] intValue] >= 8) {
        [[UINavigationBar appearance] setTranslucent:NO];
        [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
        
        NSDictionary *titleAttributes =@{
                                         NSFontAttributeName :[UIFont fontWithName:@"Helvetica-Bold" size:15.0],
                                         NSForegroundColorAttributeName : [UIColor whiteColor]
                                         };
        
        navigation.navigationBar.titleTextAttributes = titleAttributes;
    }
    
    if ([[ver objectAtIndex:0] intValue] >= 7) {
        // iOS 7.0 or later
        [navigation.navigationBar setTintColor:[UIColor whiteColor]];
        [navigation.navigationBar setBarTintColor:customBlue];
        [navigation.navigationBar setTranslucent: NO];
    }else {
        // iOS 6.1 or earlier
        [navigation.navigationBar setTintColor:[UIColor whiteColor]];
    }
}

+(void)setWhiteShadowToAllLabels :(id)view
{
    for (id object in [view subviews]) {
        if([object isKindOfClass:[UILabel class]])
        {
            [object setShadowColor:[UIColor whiteColor]];
            [object setShadowOffset:CGSizeMake(0.0, 1)];
        }
    }
    
}

+(void)setBlackShadowToAllLabels :(id)view
{
    for (id object in [view subviews]) {
        if([object isKindOfClass:[UILabel class]])
        {
            [object setShadowColor:[UIColor blackColor]];
            [object setShadowOffset:CGSizeMake(0.0, 1)];
        }
    }
    
}
+(void)setBlackShadow :(UILabel *)label
{
    [label setShadowColor:[UIColor blackColor]];
    [label setShadowOffset:CGSizeMake(0.0, 1)];
}

+(void)showInternetConnectionAlert
{
    [Utility showAlertViewWithTitle:@"" message:LocalizedString(@"nointernet") cancelButtonTile:LocalizedString(@"ok")];
}

+(BOOL)checkInternetConnection
{
    if(![AFNetworkReachabilityManager sharedManager].reachable)
        [Utility showAlertViewWithTitle:@"" message:LocalizedString(@"nointernet") cancelButtonTile:LocalizedString(@"ok")];
    
    return [AFNetworkReachabilityManager sharedManager].reachable;
}

+(void)LoadVisualEffectView :(UIView *)view
{
    UIImageView *imgBackground =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height)];
    [imgBackground setImage:[UIImage imageNamed:@"bg_splash"]];
    [view addSubview:imgBackground];
    UIBlurEffect *blurrEffect =[UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    
    UIVisualEffectView *visualEffectView =[[UIVisualEffectView alloc]initWithEffect:blurrEffect];
    visualEffectView.frame = imgBackground.frame;
    [imgBackground addSubview:visualEffectView];
}


+(BOOL)isValueNilNullOrEmpty :(id)value
{
    if([value isKindOfClass:[NSNull class]])
        return YES;
    else if(value == nil)
        return YES;
    else if([value length] ==0)
        return YES;
    else if(value == (id)[NSNull null]) {
        return YES;
    }
    
    return NO;
}

+(BOOL)isCurrentLocationAvailable
{
    if ([UserDefaults objectForKey:@"currentLat"]) {
        if([[UserDefaults objectForKey:@"currentLat"] isEqualToNumber:@0])
            return NO;
        
        if([[UserDefaults objectForKey:@"currentLon"] isEqualToNumber:@0])
            return NO;
    }
    else if (![UserDefaults objectForKey:@"currentLat"])
        return NO;
    
    
    return YES;
}

+(NSString *)getcurrentDateOnly
{
    //month/day/year hour/minutes/seconds
    NSDate *CurrentDate = [NSDate date];
    NSDateFormatter *Formatter=[[NSDateFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [Formatter setLocale:locale];
    [Formatter setDateFormat:@"MM/dd/yyyy h:mm:ss"];
    NSString *FormattedDate=[Formatter stringFromDate:CurrentDate];
    return FormattedDate;
}

//Arabic Numbers
+ (NSString *) replace:(NSString *)c
{
    if ([c isEqualToString:@"٠"])
        return @"0";
    if ([c isEqualToString:@"١"])
        return @"1";
    if ([c isEqualToString:@"٢"])
        return @"2";
    if ([c isEqualToString:@"٣"])
        return @"3";
    if ([c isEqualToString:@"٤"])
        return @"4";
    if ([c isEqualToString:@"٥"])
        return @"5";
    if ([c isEqualToString:@"٦"])
        return @"6";
    if ([c isEqualToString:@"٧"])
        return @"7";
    if ([c isEqualToString:@"٨"])
        return @"8";
    if ([c isEqualToString:@"٩"])
        return @"9";
    return c;
}

+ (NSString *) replaceArabicNumbers:(NSString *) str
{
    NSString * value = @"";
    for (int i = 0; i < [str length]; ++i)
    {
        NSString* part = [str substringWithRange:NSMakeRange(i, 1)];
        part = [self replace:part];
        value = [value stringByAppendingString:part];
    }
    return value;
}
@end
