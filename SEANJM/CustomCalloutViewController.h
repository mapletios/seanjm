//
//  CustomCalloutViewController.h
//  SEANJM
//
//  Created by Randa Al-Sadek on 7/22/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ArcGIS/ArcGIS.h>

@interface CustomCalloutViewController : UIViewController

@property(nonatomic, strong) NSString *calloutType;
@property(nonatomic, strong) NSString *txt1;
@property(nonatomic, strong) NSString *txt2;
@property(nonatomic, strong) NSString *txt3;
@property(nonatomic, strong) NSString *txt4;

@end
