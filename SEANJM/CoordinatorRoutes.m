//
//  CoordinatorRoutes.m
//  SEANJM
//
//  Created by Abdul Kareem on 9/9/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "CoordinatorRoutes.h"

@implementation CoordinatorRoutes
@synthesize  busesAssignedArray,numberOfRounds,routeInfo;


- (void)encodeWithCoder:(NSCoder *)coder;
{
    [coder encodeObject:busesAssignedArray forKey:@"busesAssignedArray"];
    [coder encodeObject:numberOfRounds forKey:@"numberOfRounds"];
    [coder encodeObject:routeInfo forKey:@"routeInfo"];
}

- (id)initWithCoder:(NSCoder *)coder;
{
    self = [super init];
    if (self != nil)
    {
        busesAssignedArray = [coder decodeObjectForKey:@"busesAssignedArray"] ;
        numberOfRounds = [coder decodeObjectForKey:@"numberOfRounds"] ;
        routeInfo = [coder decodeObjectForKey:@"routeInfo"] ;
    }
    return self;
}@end
