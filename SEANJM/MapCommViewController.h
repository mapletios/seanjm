//
//  MapCommViewController.h
//  SEANJM
//
//  Created by Randa Al-Sadek on 7/2/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapCommViewController : UIViewController

@property (nonatomic) int mapType;
@property (nonatomic) BOOL showReport;

@end
