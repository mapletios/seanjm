//
//  BusesListViewController.h
//  SEANJM
//
//  Created by Abdul Kareem on 9/9/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BusesListViewController : UIViewController
-(id)initWithSelectedBusesOfRoute :(NSArray *)busesForRoute;
@end
