//
//  BusesListViewController.m
//  SEANJM
//
//  Created by Abdul Kareem on 9/9/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "BusesListViewController.h"
#import "TwoTitleCell.h"
#import "FooterViewCell.h"
#import "Macros.h"
#import "LanguageController.h"
#import "AFNetworking.h"
#import "Utility.h"
#import "UserInformation.h"
#import "MBProgressHUD.h"
#import "AddUsersViewController.h"
#import "AddBusesViewController.h"
#import "AFNetworking.h"
#import "ConfigurationManager.h"
#import "BTSData.h"

#define BTSAssignUserSerivice @"http://213.236.53.172/snbts/restserviceimpl.svc/assignBusToRouteV1"
#define BTSRemoveUserFromRoute @"http://213.236.53.172/snbts/restserviceimpl.svc/removeBusesFromSpecificRoute"

@interface BusesListViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    ConfigurationManager *config;
    NSOperationQueue *operationqueue;
    
    
}
@property (nonatomic,weak)IBOutlet UITableView *busesTableView;
@property (nonatomic)NSString *busCellIdentifier;
@property (nonatomic)NSString *busFooterIdentifier;
@property (nonatomic)NSMutableArray *busArray;
@property (nonatomic)NSDictionary *userSelectedRole;
@property (nonatomic)NSDictionary *NewUserRoute;
@property (nonatomic)MBProgressHUD *hudview;
@property (nonatomic)NSInteger selectedIndexToEdit;
@property(nonatomic,strong) NSMutableArray *sendBusArray;
@property(nonatomic) NSMutableArray *removingArray;

@end

@implementation BusesListViewController

-(id)initWithSelectedBusesOfRoute :(NSArray *)busesForRoute
{
    self =[super init];
    _busArray = [NSMutableArray arrayWithArray:busesForRoute];
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _sendBusArray = [NSMutableArray array];
    _removingArray = [NSMutableArray array];
    config = [[ConfigurationManager alloc]init];
    operationqueue =[[NSOperationQueue alloc]init];
    
    _busCellIdentifier =   @"TwoTitleCell";
    _busFooterIdentifier = @"FooterViewCell";
    
    [_busesTableView registerNib:[UINib nibWithNibName:_busCellIdentifier bundle:[NSBundle mainBundle]]forCellReuseIdentifier:_busCellIdentifier];
    
    [_busesTableView registerNib:[UINib nibWithNibName:_busFooterIdentifier bundle:[NSBundle mainBundle]]forCellReuseIdentifier:_busFooterIdentifier];
    
    AddObserver(self, @selector(getUpdatedBusesFromServer), @"UpdateBusListLoad", nil);
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.topItem.title = [[UserInformation getUserSelectedRoute] objectForKey:@"name"];
    self.navigationController.navigationBar.backItem.title = LocalizedString(@"back");
    _NewUserRoute = [NSDictionary dictionary];
    UIBarButtonItem *btnNextButton =[[UIBarButtonItem alloc] initWithTitle:LocalizedString(@"send") style:UIBarButtonItemStylePlain target:self action:@selector(sendTapped:)];
    [self.navigationItem setRightBarButtonItem:btnNextButton];
    
    NSIndexPath *tableSelection = [_busesTableView indexPathForSelectedRow];
    [_busesTableView deselectRowAtIndexPath:tableSelection animated:YES];
    [self loadAllTasks];
    
}

-(void)getUpdatedBusesFromServer
{
    _busArray = (NSMutableArray *)[BTSData getAssignedBusesByCoordinatorForRoute];
    [_busesTableView reloadData];
}

-(void)loadAllTasks
{
    /// Load from webservice ///
    
    if([UserDefaults objectForKey:kBTS_C_Assignedbuses] && _busArray.count ==0)
    {
        _busArray = [NSMutableArray arrayWithArray:[BTSData getAssignedBusesByCoordinatorForRoute]];
        [_busesTableView reloadData];
    }
    
    //Update Task Info
    if([[UserDefaults objectForKey:kUpdateBus] count] >0)
    {
        _NewUserRoute = [UserDefaults objectForKey:kUpdateBus];
        [_busArray replaceObjectAtIndex:_selectedIndexToEdit withObject:_NewUserRoute];
        [UserDefaults removeObjectForKey:kUpdateBus];
        [UserDefaults synchronize];
    }
    
    //Add New Task
    else if([[UserDefaults objectForKey:kAddnewBus] count] >0)
    {
        _NewUserRoute = [UserDefaults objectForKey:kAddnewBus];
        [_sendBusArray addObject:_NewUserRoute];
        [_busArray addObject:_NewUserRoute];
        [UserDefaults removeObjectForKey:kAddnewBus];
        [UserDefaults synchronize];
        [_busesTableView reloadData];
        
    }
}

#pragma mark -
#pragma mark IBActions
-(IBAction)sendTapped:(id)sender
{
    if(![Utility checkInternetConnection])
        return;
    
    @autoreleasepool {
        [_sendBusArray removeAllObjects];
        for (NSMutableDictionary *castDic in _busArray) {
            if( [castDic objectForKey:@"addItem"])
            {
                NSMutableDictionary *mutableDictionary = [castDic mutableCopy];
                [mutableDictionary removeObjectForKey:@"addItem"];
                [_sendBusArray addObject:mutableDictionary];
            }
        }
        
    }
    
    if(_sendBusArray.count >0)
    {
        NSString *serviceURL = [NSString stringWithFormat:@"%@",BTSAssignUserSerivice];
        [self sendRemoveRequest:serviceURL :_sendBusArray];
    }
    if(_removingArray.count >0)
    {
        NSString *serviceURL = [NSString stringWithFormat:@"%@",BTSRemoveUserFromRoute];
        [self sendRemoveRequest:serviceURL :_removingArray];
    }
}

-(void)sendRemoveRequest:(NSString *)serviceURL :(NSMutableArray *)requestArray
{
    
    
    NSDictionary *sendingDictionary = @{@"routesbuses": requestArray};
    
    //   NSDictionary *sendingDictionary =[NSDictionary dictionaryWithObject:requestArray forKey:@"routesusers"];
    if(!_hudview)
    {
        _hudview = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        _hudview.mode = MBProgressHUDModeIndeterminate;
        _hudview.labelText = @"Syncing...";
        [_hudview setDimBackground:YES];
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager POST:serviceURL parameters:sendingDictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //NSLog(@"JSON: %@", responseObject);
        
        if(![responseObject  isKindOfClass:[NSNull class]])
        {
            if([[responseObject objectForKey:@"Result"] intValue] == 1)
            {
                [_hudview hide:YES];
                [Utility showAlertViewWithTitle:LocalizedString(@"done")  message:LocalizedString(@"syncedsuccessfully") cancelButtonTile:LocalizedString(@"ok")];
                
                [self changeSavedStatusAfterUploadingSuccessfully];
                if([operation.request.URL.relativeString isEqualToString:BTSAssignUserSerivice])
                {
                    [_sendBusArray removeAllObjects];
                }
                if([operation.request.URL.relativeString isEqualToString:BTSRemoveUserFromRoute])
                    [_removingArray removeAllObjects];
                
                DispatchEvent(@"loadWebServiceForCoordinator", nil);
                
            }
            else
                [Utility showAlertViewWithTitle:LocalizedString(@"error")  message:LocalizedString(@"error") cancelButtonTile:LocalizedString(@"ok")];
            [_hudview hide:YES];
            _hudview =nil;
        }
        else {
            [Utility showAlertViewWithTitle:LocalizedString(@"error")  message:LocalizedString(@"error") cancelButtonTile:LocalizedString(@"ok")];
            [_hudview hide:YES];
            _hudview = nil;
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [Utility showAlertViewWithTitle:LocalizedString(@"error")  message:LocalizedString(@"error") cancelButtonTile:LocalizedString(@"ok")];
        //[activityIndicator stopAnimating];
        [_hudview hide:YES];
        _hudview =nil;
    }];
}

-(void)changeSavedStatusAfterUploadingSuccessfully
{
    
    NSMutableArray *finalArray =[NSMutableArray array];
    NSMutableDictionary *mutableDictionary;
    
    for (NSMutableDictionary *castDic in _busArray) {
        mutableDictionary = [castDic mutableCopy];
        if( [castDic objectForKey:@"addItem"])
        {
            [mutableDictionary removeObjectForKey:@"addItem"];
        }
        if( [castDic objectForKey:@"deleteItem"])
        {
            [mutableDictionary removeObjectForKey:@"deleteItem"];
        }
        if(mutableDictionary.count >0)
            [finalArray addObject:mutableDictionary];
    }
    _busArray =  [NSMutableArray arrayWithArray:finalArray];
    
    [_busesTableView reloadData];
}

#pragma mark -
#pragma mark UITableView DataSource


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _busArray.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section;
{
    
    FooterViewCell *footerView = [tableView dequeueReusableCellWithIdentifier:_busFooterIdentifier];
    footerView.lblTitle.text = LocalizedString(@"addNewBus");
    [footerView.btnImage setBackgroundImage:[UIImage imageNamed:@"ic_addround"] forState:UIControlStateNormal];
    [footerView.btnImage setFrame:CGRectMake(5, 5, 32, 32)];
    [footerView.btnImage setTag:section];
    [footerView.lblTitle setFont:[UIFont fontWithName:HELVETICA_BOLD size:14.0]];
    [footerView.contentView setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    [footerView.btnImage addTarget:self action:@selector(leftFooterButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    return footerView.contentView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TwoTitleCell *taskCell =[tableView dequeueReusableCellWithIdentifier:_busCellIdentifier forIndexPath:indexPath];
    
    taskCell.lblRightTitle.text = [NSString stringWithFormat:@"%@: ",[BTSData getRouteNameWithRouteId:[[_busArray objectAtIndex:indexPath.row] objectForKey:@"routeId"]]];
    
    NSString *stringForLeftLabel =[NSString stringWithFormat:@"%@ : %@",
                                   [[_busArray objectAtIndex:indexPath.row] objectForKey:@"companyName"],
                                   [[_busArray objectAtIndex:indexPath.row] objectForKey:@"plateNumber"]];
    
    taskCell.lblLeftTitle.text = stringForLeftLabel;
    [taskCell.btnImage setContentMode:UIViewContentModeScaleAspectFit];
    [taskCell.btnImage setBackgroundImage:[UIImage imageNamed:@"ic_info"] forState:UIControlStateNormal];
    [taskCell.btnImage.superview setTag:indexPath.row];
    [taskCell.btnImage addTarget:self action:@selector(leftButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    //[taskCell.btnImage setHidden:YES];
    
    [taskCell.btnImage setUserInteractionEnabled:YES];
    [taskCell setBackgroundColor:[UIColor whiteColor]];
    
    if(_removingArray.count >0)
    {
        if([[_busArray objectAtIndex:indexPath.row] objectForKey:@"deleteItem"])
            [taskCell setBackgroundColor:LightRedColor];
    }
    if(_NewUserRoute.count >0){
        if([[_busArray objectAtIndex:indexPath.row] objectForKey:@"addItem"])
        {
            [taskCell setBackgroundColor:LightYellowColor];
        }
    }
    return taskCell;
}

#pragma mark - TableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section;
{
    return 44.0;
}

#pragma mark TABELVIEW EDITING

-(NSString *)tableView:(UITableView *)tableView titleForSwipeAccessoryButtonForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return @"Undo";
}

-(void)tableView:(UITableView *)tableView swipeAccessoryButtonPushedForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    [_removingArray removeObject:[_busArray objectAtIndex:indexPath.row]];
    [[tableView cellForRowAtIndexPath:indexPath] setBackgroundColor:[UIColor whiteColor]];
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
        
        [tableView beginUpdates];
        if(![_removingArray containsObject:[_busArray objectAtIndex:indexPath.row]])
            [_removingArray addObject:[_busArray objectAtIndex:indexPath.row]];
        
        [[tableView cellForRowAtIndexPath:indexPath] setBackgroundColor:LightRedColor];
        NSMutableDictionary *deleteDic = [NSMutableDictionary dictionaryWithDictionary:[_busArray objectAtIndex:indexPath.row]];
        [deleteDic setObject:@"delete" forKey:@"deleteItem"];
        [_busArray replaceObjectAtIndex:indexPath.row withObject:deleteDic];
        
        [tableView endUpdates];
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

#pragma mark -
#pragma mark - IBActions
-(IBAction)leftButtonPressed:(UIButton *)sender
{
    _selectedIndexToEdit = sender.superview.tag;
    AddBusesViewController *objAddbus = [[AddBusesViewController alloc]initWithinfo:[_busArray objectAtIndex:_selectedIndexToEdit]];
    [self.navigationController pushViewController:objAddbus animated:YES];
}

//Add new task
-(IBAction)leftFooterButtonTapped:(UIButton *)sender
{
    AddBusesViewController *objAddbus = [[AddBusesViewController alloc]initWithBusArray:_busArray];
    [self.navigationController pushViewController:objAddbus animated:YES];
}

@end
