//
//  AddGuidersViewController.h
//  SEANJM
//
//  Created by Abdul Kareem on 9/8/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddGuidersViewController : UIViewController
-(id)initWithSelectedTask :(NSDictionary *)taskdictionary;;
@end
