//
//  SenderReceiverActionCell.h
//  SEANJM
//
//  Created by Abdul Kareem on 9/16/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SenderReceiverActionCell : UITableViewCell
@property (nonatomic,weak)IBOutlet UILabel *lblTitle;
@property (nonatomic,weak)IBOutlet UILabel *lblsubTitle;
@property (nonatomic,weak)IBOutlet UIButton *btnSend;
@end
