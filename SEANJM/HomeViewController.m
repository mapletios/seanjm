//
//  HomeViewController.m
//  SEANJM
//
//  Created by Randa Al-Sadek on 6/11/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "HomeViewController.h"
#import "OptionsViewController.h"
#import "LoginViewController.h"
#import "Macros.h"
#import "HomeCommViewController.h"
#import "LanguageController.h"
#import "Utility.h"
#import <CoreLocation/CoreLocation.h>
#import "InfoViewController.h"
#import "RolesViewController.h"
#import "BTSData.h"
#import "AdminsOptionsViewController.h"


@interface HomeViewController ()<CLLocationManagerDelegate, UIAlertViewDelegate> {
    
    CLLocationManager *locationManager;
    
    IBOutlet UIImageView *imgBG;
    IBOutlet UIButton *btnLogout;
    IBOutlet UILabel *lblUserName;
    InfoViewController *objInfo;
    int count ;
}

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithTitle:LocalizedString(@"back")
                                                                 style:UIBarButtonItemStylePlain
                                                                target:nil
                                                                action:nil];
    
    [self.navigationItem setBackBarButtonItem:backItem];
    
    //Get Current Location
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    
    if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [locationManager requestAlwaysAuthorization];
    }
    
    UIImageView *imgback =[[UIImageView alloc]initWithFrame:self.view.frame];
    [imgback setImage:[UIImage imageNamed:@"background"]];
    //[self.view addSubview:imgback];
    //[Utility applyBlueRoundedButton:btnLogout];
    //[btnLogout.titleLabel setFont:[UIFont fontWithName:HELVETICA_BOLD size:15.0]];
    [self loadUserDetails];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userInfoClosed) name:@"Info_close"object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startUserLocation) name:kStartLocation object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopUserLocation) name:kStopLocation object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleDefault];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewDidDisappear:(BOOL)animated
{
    if(![self.navigationController.viewControllers containsObject:self])
        DispatchEvent(kStopLocation, nil);
}

-(void)loadUserDetails
{
    [lblUserName setText:[[UserDefaults objectForKey:@"loginUser"] uppercaseString]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)minaButtonPressed:(id)sender {
    int officeNumber = [[UserDefaults objectForKey:kofficeNumber]intValue];
//    NSString *userID = [UserDefaults objectForKey:kloginUserId];
//    NSString *adminID m= kAdminID;
    
    if (officeNumber > 0 || officeNumber == -1) {
        OptionsViewController *view = [[OptionsViewController alloc]init];
        [self.navigationController pushViewController:view animated:YES];
    }
    else {
        [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"notAuthorized") cancelButtonTile:LocalizedString(@"ok")];
    }
}
- (IBAction)communicationsButtonPressed:(id)sender {
    
    if ([UserDefaults objectForKey:kisSRS]) {
        HomeCommViewController *view = [[HomeCommViewController alloc]init];
        [self.navigationController pushViewController:view animated:YES];
    }
    else {
        [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"notAuthorized") cancelButtonTile:LocalizedString(@"ok")];
    }
}

-(IBAction)btsButtonTapped:(id)sender{
    NSLog(@"groupe id %@",[UserDefaults objectForKey:kuserGroupId]);
    if([[UserDefaults objectForKey:kIsSuperAdmin] integerValue] == 1)
    {
        AdminsOptionsViewController *objAdminOptions =[[AdminsOptionsViewController alloc]initWithAuthorityIsSuperAdmin:YES];
        [self.navigationController pushViewController:objAdminOptions animated:YES];
        return;
        
    }
    else if([[UserDefaults objectForKey:kuserGroupId] integerValue] >0)
    {
        RolesViewController *objRoles =[[RolesViewController alloc]init];
        [self.navigationController pushViewController:objRoles animated:YES];
        return;
    }
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:LocalizedString(@"info") message:LocalizedString(@"comingSoon") delegate:self cancelButtonTitle:LocalizedString(@"ok") otherButtonTitles:nil, nil];
    [alert show];
    
}
- (IBAction)logoutButtonPressed:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:LocalizedString(@"logout") message:LocalizedString(@"logoutMessage") delegate:self cancelButtonTitle:LocalizedString(@"no") otherButtonTitles:LocalizedString(@"yes"), nil];
    [alert show];
}

-(IBAction)infoButtonTapped:(id)sender
{
    objInfo =[[InfoViewController alloc]init];
    //[self.navigationController pushViewController:objInfo animated:YES];
    [objInfo.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [objInfo.view setCenter:self.view.center];
    objInfo.view.alpha=0.0;
    
    [self addChildViewController:objInfo];
    [self.view addSubview:objInfo.view];
    [objInfo didMoveToParentViewController:self];
    [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^(void) {
        
        objInfo.view.alpha=1.0;
        
        
    } completion:^(BOOL finished) {
        
    }];
    
    [objInfo.view setCenter:self.view.center];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    [UserDefaults setValue:@0 forKey:@"currentLat"];
    [UserDefaults setValue:@0 forKey:@"currentLon"];
    if(count <=0)
    {
    [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"failedGetLocation") cancelButtonTile:LocalizedString(@"ok")];
        count++;
    }
    
    [UserDefaults synchronize];
}

-(void)locationManagerDidPauseLocationUpdates:(CLLocationManager *)manager
{
    
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *currentLocation = [locations lastObject];
    if (currentLocation != nil) {
        //        NSLog(@"%f",currentLocation.coordinate.latitude);
        [UserDefaults setValue:[NSNumber numberWithDouble:currentLocation.coordinate.latitude] forKey:@"currentLat"];
        [UserDefaults setValue:[NSNumber numberWithDouble:currentLocation.coordinate.longitude] forKey:@"currentLon"];
        [UserDefaults synchronize];
    }
}

#pragma UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        LoginViewController *view = [[LoginViewController alloc]init];
        [self presentViewController:view animated:YES completion:^{
            [UserDefaults removeObjectForKey:@"loginUser"];
            [UserDefaults removeObjectForKey:@"officeNumber"];
            [UserDefaults removeObjectForKey:@"userFullName"];
            [UserDefaults removeObjectForKey:@"userPhone"];
            [UserDefaults removeObjectForKey:@"userIDs"];
            [UserDefaults removeObjectForKey:kBTSConfigLoaded];
            [UserDefaults removeObjectForKey:kCoordinatorRoutesWithBusesAndRounds];
            [UserDefaults removeObjectForKey:kBTSConfigLoaded];
            [UserDefaults removeObjectForKey:kBTSCONFIGURATION];
            [UserDefaults removeObjectForKey:kBTS_M_AssignedUsersAndGuides];
            [UserDefaults removeObjectForKey:kBTS_M_AssignedUsersAndGuides];
            [UserDefaults removeObjectForKey:kBTS_C_Assignedbuses];
            [UserDefaults removeObjectForKey:kBTS_SR_Assignedbuses];
            [UserDefaults removeObjectForKey:kSendBus];
            [UserDefaults removeObjectForKey:kAddnewBus];
            [UserDefaults removeObjectForKey:kUpdateBus];
            [UserDefaults removeObjectForKey:kAddnewUser];
            [UserDefaults removeObjectForKey:kUpdateUser];
            [UserDefaults removeObjectForKey:kAddnewGuider];
            [UserDefaults removeObjectForKey:kUpdateGuider];
            [UserDefaults removeObjectForKey:kAllGroups];
            [UserDefaults removeObjectForKey:kuserRoles];
            [UserDefaults removeObjectForKey:kSARAllRoutesReports];
            [UserDefaults removeObjectForKey:kSARAllRoutesReports];
            
            
            [UserDefaults synchronize];
        }];
    }
}

-(void)userInfoClosed
{
    [UIView animateWithDuration:0.8 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^(void) {
        
        objInfo.view.alpha=0.0;
    } completion:^(BOOL finished) {
 
 
        [objInfo.view removeFromSuperview];
        [objInfo removeFromParentViewController];
        [objInfo didMoveToParentViewController:nil];
        
    }];
}

-(void)startUserLocation
{
    [locationManager startUpdatingLocation];
}

-(void)stopUserLocation
{
    [locationManager stopUpdatingLocation];
}


@end
