//
//  ReportCell.m
//  SEANJM
//
//  Created by Abdul Kareem on 9/21/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "ReportCell.h"

@implementation ReportCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
