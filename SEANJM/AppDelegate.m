    //
//  AppDelegate.m
//  SEANJM
//
//  Created by Randa Al-Sadek on 5/12/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginViewController.h"
#import "HomeViewController.h"
#import "Macros.h"
#import "LanguageController.h"
#import "Utility.h"
#import <ArcGIS/ArcGIS.h>
#import "AFNetworkReachabilityManager.h"
#import "ConfigurationManager.h"

@interface AppDelegate () {
    
    ConfigurationManager *config;
    UIActivityIndicatorView *activityIndicator;
}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.

    [self internetChangedStatus];
    //**********************************ArcGIS Initialization************************///
    NSError  *error;
    NSString *clientID = @"hUkKP9NRi5QCLPJj";
    [AGSRuntimeEnvironment setClientID:clientID error:&error];
    //**********************************ArcGIS Initialization************************///
    
    /**********Testing**********/
//    [UserDefaults setObject:@"ziad" forKey:@"loginUser"];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    UINavigationController *navigation;
    if ([UserDefaults objectForKey:@"loginUser"]) {
        HomeViewController *homeViewController = [[HomeViewController alloc]init];
        navigation = [[UINavigationController alloc]initWithRootViewController:homeViewController];
        [navigation setNavigationBarHidden:YES];
        self.window.rootViewController = navigation;
    }
    else {
        LoginViewController *loginViewController = [[LoginViewController alloc] init];
        self.window.rootViewController = loginViewController;
    }

    if (!([[UserDefaults objectForKey:@"SRSConfigLoaded"] isEqualToString:@"1"]) || !([[UserDefaults objectForKey:@"TCSConfigLoaded"] isEqualToString:@"1"])) {
        
        config = [[ConfigurationManager alloc]init];
        [self loadFaultCategories];

        activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [activityIndicator setCenter: self.window.rootViewController.view.center];
        [activityIndicator hidesWhenStopped];
        [activityIndicator startAnimating];
        [self.window.rootViewController.view addSubview:activityIndicator];
    

        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    }

    
    [Utility setNavigationBarProperties:navigation];
    
    navigation.navigationController.interactivePopGestureRecognizer.enabled = NO;
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}

- (void)internetChangedStatus {
    __block BOOL reachable;
    // and now activate monitoring
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    // Setting block doesn't means you area running it right now
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
        switch (status) {
            case AFNetworkReachabilityStatusNotReachable:
                NSLog(@"No Internet Connection");
                
                reachable = NO;
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
                NSLog(@"WIFI");
                
                reachable = YES;
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN:
                NSLog(@"3G");
                
                reachable = YES;
                break;
            default:
                NSLog(@"Unkown network status");
                
                reachable = NO;
                break;
        }
        NSLog(@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status));
    }];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    DispatchEvent(kStopLocation, nil);
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    DispatchEvent(kStartLocation, nil);
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)loadFaultCategories {
    
    [config loadTCSConfiguration:^{
        [self stopActivityIndicator];
        [UserDefaults setObject:@"1" forKey:@"TCSConfigLoaded"];
        [UserDefaults synchronize];
    }failure:^{
        [self stopActivityIndicator];
        [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"failLoadConfig") cancelButtonTile:LocalizedString(@"ok")];

    }];
    
    [config loadSRSConfiguration:^{
        [self stopActivityIndicator];
        [UserDefaults setObject:@"1" forKey:@"SRSConfigLoaded"];
        [UserDefaults synchronize];
    }failure:^{
        [self stopActivityIndicator];
        [Utility showAlertViewWithTitle:LocalizedString(@"error") message:LocalizedString(@"failLoadConfig") cancelButtonTile:LocalizedString(@"ok")];
    }];

}

- (void)stopActivityIndicator {
    [activityIndicator stopAnimating];
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
}

@end
