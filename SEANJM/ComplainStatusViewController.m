//
//  ComplainStatusViewController.m
//  SEANJM
//
//  Created by Randa Al-Sadek on 9/10/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "ComplainStatusViewController.h"
#import "ConfigurationManager.h"
#import "LanguageController.h"
#import "ActionSheetStringPicker.h"
#import "Macros.h"
#import "Utility.h"

@interface ComplainStatusViewController ()<UITextFieldDelegate> {
    
    __weak IBOutlet UILabel     *complainNumLbl;
    __weak IBOutlet UITextField *complainNumTxt;
    __weak IBOutlet UILabel     *complainTypeLbl;
    __weak IBOutlet UITextField *complainTypeTxt;
    __weak IBOutlet UILabel     *complainDateLbl;
    __weak IBOutlet UITextField *complainDateTxt;
    __weak IBOutlet UILabel     *statusLbl;
    __weak IBOutlet UITextField *statusTxt;
    __weak IBOutlet UIButton    *btnSend;
    
    
    __weak IBOutlet UIActivityIndicatorView *activityIndicator;
    
    
    NSMutableDictionary *statusList;
    ConfigurationManager *config;
}

@end

@implementation ComplainStatusViewController

@synthesize complain;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    statusList =[NSMutableDictionary dictionary];
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithTitle:LocalizedString(@"back")
                                                                 style:UIBarButtonItemStylePlain
                                                                target:nil
                                                                action:nil];
    
    [self.navigationItem setBackBarButtonItem:backItem];
    
    config = [[ConfigurationManager alloc]init];
    
    complainNumLbl.text = LocalizedString(@"complainNumber");
    complainTypeLbl.text = LocalizedString(@"complainType");
    complainDateLbl.text = LocalizedString(@"complainDate");
    statusLbl.text = LocalizedString(@"complainStatus");
    
    complainNumTxt.text = [NSString stringWithFormat:@"%@",[complain objectForKey:@"id"]];
    complainTypeTxt.text = [complain objectForKey:@"mainType"];
    complainDateTxt.text = [complain objectForKey:@"date"];
    statusTxt.text = [complain objectForKey:@"status"];

}

-(void)viewWillAppear:(BOOL)animated
{
    [Utility applyBlueRoundedButton:btnSend];
}

#pragma mark - UITextFieldDelegata
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    statusList = [[UserDefaults objectForKey:@"MaintenanceStatus"] mutableCopy];
    [statusList removeObjectForKey:@"-1"];
    NSArray *statusValues = [statusList allValues];
    
    [ActionSheetStringPicker showPickerWithTitle:@"" rows:statusValues initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        textField.text = selectedValue;
        [complain setValue:selectedValue forKey:@"status"];
        
    } cancelBlock:nil origin:textField];
    return NO;
}

- (IBAction)doneButtonPressed:(id)sender {
    
    
    NSArray *keys = [statusList allKeysForObject:[complain objectForKey:@"status"]];
    if(keys.count >0)
    {
        [activityIndicator startAnimating];
        int statusId = [[keys objectAtIndex:0]intValue];
    
        [config updateComplaintStatus:[[complain objectForKey:@"id"]intValue] complaintType:[[complain objectForKey:@"complaintType"]intValue] status:statusId completion:^ {
            
            [activityIndicator stopAnimating];
            [self.navigationController popViewControllerAnimated:YES];
            //        [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:3] animated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshMapFeatures" object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshMap" object:nil];
            
        }failure:^ {
            [activityIndicator stopAnimating];
        }];
    }
}

@end
