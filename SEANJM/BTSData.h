//
//  BTSData.h
//  SEANJM
//
//  Created by Abdul Kareem on 9/3/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTSData : NSObject


+(NSArray *)getRoutes;

+(NSArray *)getRoles;

+(NSArray *)getBusCompanies;

+(NSString *)getBusCompanyNameWithBusId :(NSString *)bus_id
;
+(NSArray *)getAllBuses;

+(NSDictionary *)getBusFromPlateNumber :(NSString *)plateNo;

+(NSString *)getBusPlateNumberFromBusId :(NSString *)busId;

+(NSDictionary *)getBusInfoFromBusId :(NSString *)busId;

+(NSArray *)getLocations;

+(NSArray *)getSenderLocationsForCoordinator;

+(NSArray *)getReceiverLocationsForCoordinator;


+(NSArray *)getGuidersForCoordinator;

+(NSArray *)getUsers;

+(NSArray *)getAllGuidersForCoordinator;

+(NSArray *)getAllUsersForCoordinator;

+(NSArray *)getAssignedUsersByManagerForRoute;

+(NSArray *)getAssignedUsersByManagerForSpecificRoute :(NSString *)routeID;

+(NSArray *)getAssignedGuidersByManagerForRoute;

+(NSArray *)getAssignedBusesByCoordinatorForRoute;

+(NSString *)getLocationNameWithLocationId :(NSString *)location_id;

+(NSString *)getRoleNameWithRoleId :(NSString *)role_id;

+(NSString *)getRouteNameWithRouteId :(NSString *)route_id;

+(NSArray *)getCoordinatorRoutesWithNumberOfRounds;

+(void)saveCoordinatorRoutesWithNumberOfRounds :(NSArray *)routes_array;

+(NSNumber *)getDefaultNumberOfRoundsFromSelectedRoute;

+(void)removeCoordinatorRoutesWithNumberOfRounds;

+(NSArray *)getSuperAdminAllReportsForRoutes;

+(NSArray *)getAllGroups;

+(NSDictionary *)getSuperAdminReportForRoute :(int)route_id;

@end
