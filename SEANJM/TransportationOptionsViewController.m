//
//  TransportationOptionsViewController.m
//  SEANJM
//
//  Created by Randa Al-Sadek on 7/23/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "TransportationOptionsViewController.h"
#import "TransportationFirstFormViewController.h"
#import "TransportationThirdFormViewController.h"
#import "LanguageController.h"
#import "Utility.h"
#import "MapCommViewController.h"

@interface TransportationOptionsViewController ()

@end

@implementation TransportationOptionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.navigationItem setTitle:LocalizedString(@"transportComplain")];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithTitle:LocalizedString(@"back")
                                                                 style:UIBarButtonItemStylePlain
                                                                target:nil
                                                                action:nil];
    
    [self.navigationItem setBackBarButtonItem:backItem];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)firstButtonPressed:(id)sender {
    TransportationFirstFormViewController *view = [[TransportationFirstFormViewController alloc]init];
    view.formType = 1;
    [self.navigationController pushViewController:view animated:YES];
}

- (IBAction)secondButtonPressed:(id)sender {
    TransportationFirstFormViewController *view = [[TransportationFirstFormViewController alloc]init];
    view.formType = 2;
    [self.navigationController pushViewController:view animated:YES];

}

- (IBAction)thirdButtonPressed:(id)sender {
    TransportationThirdFormViewController *view = [[TransportationThirdFormViewController alloc]init];
    [self.navigationController pushViewController:view animated:YES];

}

- (IBAction)reportsButtonPressed:(id)sender {
    if(![Utility checkInternetConnection])
        return;
    
    MapCommViewController *mapView = [[MapCommViewController alloc]init];
    mapView.mapType = 3;
    mapView.showReport = YES;
    [self.navigationController pushViewController:mapView animated:YES];

}

@end
