//
//  CoordinatorRoutes.h
//  SEANJM
//
//  Created by Abdul Kareem on 9/9/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoordinatorRoutes : NSObject
@property (nonatomic)NSMutableArray *busesAssignedArray;
@property (nonatomic)NSNumber       *numberOfRounds;
@property (nonatomic)NSDictionary   *routeInfo;

- (void) encodeWithCoder : (NSCoder *)encode ;
- (id) initWithCoder : (NSCoder *)decode;
@end
