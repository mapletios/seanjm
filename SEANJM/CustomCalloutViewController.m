//
//  CustomCalloutViewController.m
//  SEANJM
//
//  Created by Randa Al-Sadek on 7/22/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "CustomCalloutViewController.h"
#import "LanguageController.h"

@interface CustomCalloutViewController () {
    
    __weak IBOutlet UIView *view;
    __weak IBOutlet UILabel *lbl1;
    __weak IBOutlet UILabel *lbl2;
    __weak IBOutlet UILabel *lbl3;
    __weak IBOutlet UILabel *lbl4;
}

@end

@implementation CustomCalloutViewController

@synthesize calloutType, txt1, txt2, txt3, txt4;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGRect newFrame = self.view.frame;
    newFrame.size.width = self.view.frame.size.width;
    
    if ([calloutType isEqualToString:@"TCS"]) {
        [lbl1 setText:[NSString stringWithFormat:@"رقم الخيمة: %@", txt1]];
        [lbl2 setText:[NSString stringWithFormat:@"اسم المنطقة: %@", txt2]];
        [lbl3 setText:[NSString stringWithFormat:@"نوع الخيمة: %@", txt3]];
        newFrame.size.height = 71;
    }
    else if ([calloutType isEqualToString:@"SRS"]) {
        [lbl1 setText:[NSString stringWithFormat:@"%@: %@", LocalizedString(@"complainNumber"), txt1]];
        [lbl2 setText:[NSString stringWithFormat:@"%@: %@", LocalizedString(@"complainType"), txt2]];
        [lbl3 setText:[NSString stringWithFormat:@"%@: %@", LocalizedString(@"complainDate"), txt3]];
        [lbl4 setText:[NSString stringWithFormat:@"%@: %@", LocalizedString(@"complainStatus"), txt4]];
    }
    
    [self.view setFrame:newFrame];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

@end
