//
//  UserInformation.h
//  SEANJM
//
//  Created by Abdul Kareem on 9/3/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface UserInformation : NSObject

+(NSString *)getGroupID;


+(NSString *)getOfficeID;


+(BOOL)getIsUserGM;


+(NSArray *)getUserRoles;

+(NSString *)getUserSelectedRoleId;

+(NSDictionary *)getUserSelectedRoute;

+(NSString *)getUserId;

+(NSDictionary *)getUserSelectedRole;
@end
