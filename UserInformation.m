//
//  UserInformation.m
//  SEANJM
//
//  Created by Abdul Kareem on 9/3/15.
//  Copyright (c) 2015 Randa Al-Sadek. All rights reserved.
//

#import "UserInformation.h"
#import "Macros.h"

@implementation UserInformation

+(NSString *)getGroupID
{
    NSLog(@"group id %@",[UserDefaults objectForKey:kuserGroupId]);
    return [UserDefaults objectForKey:kuserGroupId];
}

+(NSString *)getOfficeID
{
    NSLog(@"office id %@",[UserDefaults objectForKey:kofficeNumber]);
    return [UserDefaults objectForKey:kofficeNumber];
}

+(BOOL)getIsUserGM
{
    NSLog(@"is GM %d",[UserDefaults boolForKey:kuserIsGM]);
    return [UserDefaults boolForKey:kuserIsGM];
}

+(NSArray *)getUserRoles
{
    return [UserDefaults objectForKey:kuserRoles];
}

+(NSString *)getUserSelectedRoleId
{
    if([[UserDefaults objectForKey:kUserSelectedRole] objectForKey:@"id"])
        return [[[UserDefaults objectForKey:kUserSelectedRole] objectForKey:@"id"] stringValue];
    else
        return @"";
}

+(NSDictionary *)getUserSelectedRole
{
    if([UserDefaults objectForKey:kUserSelectedRole])
        return [UserDefaults objectForKey:kUserSelectedRole];
    else
        return @{};
}

+(NSDictionary *)getUserSelectedRoute
{
    if([UserDefaults objectForKey:kUserSelectedRoute])
        return [UserDefaults objectForKey:kUserSelectedRoute];
    else
        return nil;
}

+(NSString *)getUserId
{
    NSLog(@"user id %@",[UserDefaults objectForKey:kloginUserId]);
    if([UserDefaults objectForKey:kloginUserId])
        return [UserDefaults objectForKey:kloginUserId];
    else
        return @"";
}

@end
